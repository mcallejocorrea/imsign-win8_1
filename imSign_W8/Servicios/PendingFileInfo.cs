﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace imSign_W8.Servicios
{
    public class PendingFileInfo
    {

        private String fileName;
        private String content_type;
        private String idDoc;


        public String FileName
        {
            get
            {
                return this.fileName;
            }
            set
            {
                this.fileName = value;
            }
        }

        public String Content_Type
        {
            get
            {
                return this.content_type;
            }
            set
            {
                this.content_type = value;

            }
        }

        public String IdDoc
        {
            get
            {
                return this.idDoc;
            }
            set
            {
                this.idDoc = value;
            }
        }


    }
}
