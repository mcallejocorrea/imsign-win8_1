﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace imSign_W8.Servicios
{
    public class GetPetitionsResponse
    {

        private List<PendingFileInfo> petitions;

        public List<PendingFileInfo> Petitions
        {
            get
            {
                return this.petitions;
            }
            set
            {
                this.petitions = value;
            }
        }
    }
}
