﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using imSign_W8.BiometricsData;

namespace imSign_W8.Servicios
{
    public class ImSignResponse
    {
        private String message;
        private int status;
        private UserInfo userInfo;
        private String validation = "";
        private ImSignACK ack = new ImSignACK();
        private List<ImSignShow> info = new List<ImSignShow>();

        public String Message
        {
            get
            {
                return this.message;
            }
            set
            {
                this.message = value;
            }
        }

        public int Status
        {
            get
            {
                return this.status;
            }
            set
            {
                this.status = value;
            }
        }

        public UserInfo UserInfo
        {
            get
            {
                return this.userInfo;
            }
            set
            {
                this.userInfo = value;
            }
        }

        public String Validation
        {
            get
            {
                return this.validation;
            }
            set
            {
                this.validation = value;
            }
        }

        public ImSignACK Ack
        {
            get
            {
                return this.ack;
            }
            set
            {
                this.ack = value;
            }
        }

        public List<ImSignShow> Info
        {
            get
            {
                return this.info;
            }
            set
            {
                this.info = value;
            }
        }


    }
}
