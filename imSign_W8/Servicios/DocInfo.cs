﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace imSign_W8.Servicios
{
    public class DocInfo
    {

        private String Uid;
        private String FileName;
        private String Content_type;
        private int Length;
        private String Md5;
        private String Date;
        private String User;
        private String Content;
        private String Hash;
        private String DocumentUID;



        public String uid
        {
            get
            {
                return this.Uid;
            }
            set
            {
                this.Uid = value;
            }
        }

        public String fileName
        {
            get
            {
                return this.FileName;
            }
            set
            {
                this.FileName = value;
            }
        }

        public String content_type
        {
            get
            {
                return this.Content_type;
            }
            set
            {
                this.Content_type = value;
            }
        }

        public int length
        {
            get
            {
                return this.Length;
            }
            set
            {
                this.Length = value;
            }
        }
        public String md5
        {
            get
            {
                return this.Md5;
            }
            set
            {
                this.Md5 = value;
            }
        }

        public String date
        {
            get
            {
                return this.Date;
            }
            set
            {
                this.Date = value;
            }
        }

        public String user
        {
            get
            {
                return this.User;
            }
            set
            { 
                this.User = value;
            }
        }

        public String content
        {
            get
            {
                return this.Content;
            }
            set
            {
                this.Content = value;
            }
        }

        public String hash
        {
            get
            {
                return this.Hash;
            }
            set
            {
                this.Hash = value;
            }
        }

        public String documentUID
        {
            get
            {
                return this.DocumentUID;
            }
            set
            {
                this.DocumentUID = value;
            }
        }

    }
    
}
