﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace imSign_W8.Servicios
{
    public class SendFileResponse
    {
        private String Message;
        private String Code;

        public String message
        {
            get
            {
                return this.Message;
            }
            set
            {
                this.Message = value;
            }
        }

        public String code
        {
            get
            {
                return this.Code;
            }
            set
            {
                this.Code = value;
            }
        }
    }
}
