﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace imSign_W8.Servicios
{
    public class ImSignACK
    {

        private String publicKey = "";
        private String idKey = "";



        public String PublicKey
        {
            get
            {
                return this.publicKey;
            }
            set
            {
                this.publicKey = value;
            }
        }


        public String IdKey
        {
            get
            {
                return this.idKey;
            }
            set
            {
                this.idKey = value;
            }
        }
    }
}
