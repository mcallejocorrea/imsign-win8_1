﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using imSign_W8.Entidades;

namespace imSign_W8.Servicios
{
    public class UserDataServices
    {

        private String Name;
        private String SurName;
        private String IdCode;
        private String Email;
        private String BusinessName;
        private int NKey;
        private Boolean Premium;
        private int IdUser;
        private String Tlf;
        private String Username;
        private String Password;


        public String name
        {
            get
            {
                return this.Name;
            }
            set
            {
                this.Name = value;
            }
        }

        public String surName
        {
            get
            {
                return this.SurName;
            }
            set
            {
                this.SurName = value;
            }
        }

        public String idCode
        {
            get
            {
                return this.IdCode;
            }
            set
            {
                this.IdCode = value;
            }
        }

        public String email
        {
            get
            {
                return this.Email;
            }
            set
            {
                this.Email = value;
            }
        }

        public String businessName
        {
            get
            {
                return this.BusinessName;
            }
            set
            {
                this.BusinessName = value;
            }
        }

        public int nKey
        {
            get
            {
                return this.NKey;
            }
            set
            {
                this.NKey = value;
            }
        }

        public Boolean premium
        {
            get
            {
                return this.Premium;
            }
            set
            {
                this.Premium = value;
            }
        }

        public int idUser
        {
            get
            {
                return this.IdUser;
            }
            set
            {
                this.IdUser = value;
            }
        }

        public String tlf
        {
            get
            {
                return this.Tlf;
            }
            set
            {
                this.Tlf = value;
            }
        }

        public String username
        {
            get
            {
                return this.Username;
            }
            set
            {
                this.Username = value;
            }
        }

        public String password
        {
            get
            {
                return this.Password;
            }
            set
            {
                this.Password = value;
            }
        }




        public UserDataServices(UserData datosUsuario)
        {
            name = datosUsuario.name;
            surName = datosUsuario.surName;
            idCode = datosUsuario.idCode;
            email = datosUsuario.email;
            businessName = datosUsuario.businessName;
            nKey = datosUsuario.nKey;
            premium = datosUsuario.premium;
            idUser = datosUsuario.idUser;
            tlf = datosUsuario.tlf;
            username = datosUsuario.username;
            password = datosUsuario.password;

        }
    }
}
