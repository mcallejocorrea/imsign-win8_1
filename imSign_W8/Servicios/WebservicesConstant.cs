﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace imSign_W8.Servicios
{
   public class WebservicesConstant
    {
        public static int ERROR_CONECTION_CODE = 404;
        public static int ERROR_VALIDATION_CODE = 405;
        public static int ERROR_JSON_CODE = 406;
        public static int ERROR_BD_CODE = 500;
        public static int ERROR_ACTIVATION_CODE = 501;
        public static int ERROR_EXPIRED_CODE = 502;
        public static int ERROR_EMAIL_CODE = 503;
        public static int ERROR_SIGN_CODE = 504;
        public static int ERROR_ACTIVATION_BEFORE_CODE = 505;

        public static int SUCCESS_CODE = 200;


        public static int ERROR_USUARIO_EXISTENTE = 511;
    }
}
