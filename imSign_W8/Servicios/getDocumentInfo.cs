﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace imSign_W8.Servicios
{
    public class GetDocumentInfo
    {

       
        private String FileName;
        private String Content_type;
      
        private String Md5;
        private String Date;
      
        private String Content;
       
        private String IdDoc;



      

        public String fileName
        {
            get
            {
                return this.FileName;
            }
            set
            {
                this.FileName = value;
            }
        }

        public String content_type
        {
            get
            {
                return this.Content_type;
            }
            set
            {
                this.Content_type = value;
            }
        }

       
        public String md5
        {
            get
            {
                return this.Md5;
            }
            set
            {
                this.Md5 = value;
            }
        }

        public String date
        {
            get
            {
                return this.Date;
            }
            set
            {
                this.Date = value;
            }
        }

       

        public String content
        {
            get
            {
                return this.Content;
            }
            set
            {
                this.Content = value;
            }
        }

       

        public String idDoc
        {
            get
            {
                return this.IdDoc;
            }
            set
            {
                this.IdDoc = value;
            }
        }
    }
}
