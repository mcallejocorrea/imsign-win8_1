﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Security.Cryptography;
using Windows.Security.Cryptography.Core;
using Windows.Storage.Streams;
using Newtonsoft.Json;
using imSign_W8.BiometricsData;
using imSign_W8.Utils;
using imSign_W8.Entidades;

namespace imSign_W8.Servicios
{
    public class GeneratorUrl
    {

       // public String urlBase = " https://radiant-forest-7496.herokuapp.com/rest/imsignService/";

        public String urlBase = "http://imsign.cloudapp.net/WS_INDITEX-0.0.1-SNAPSHOT/rest/imsignService/";

        
        private String activation = "activation";
        private String validationW8 = "validationW8";
        private String validation = "validation";
        //private String registration = "register";
        private String registrationW8 = "registerW8";
        private String registration = "register";
        private String login = "login";
        private String recovery = "recovery";
        private String getPetitions = "getPetitions";
        private String getDocuments = "getDocuments";
        private String sendFile = "sendFile";



        private String push = "push";
        private long max = long.MaxValue;
        private String IMSIgn = "ImSign";
        private String separator = ":";
        private String sharp = "#";


        private UserInfo mUserInfo;
        private String mTicket1;
        private byte[] mKey;
        private PreferenceUtils mPreferenceUtils;




        private string generateTicket1ValidationService()
        {
            mPreferenceUtils = new PreferenceUtils();

            Random random = new Random();
            long randomNumber = random.Next();

            String randomString = randomNumber.ToString() + IMSIgn;


            //mTicket1 = EncryptionUtils.encodeSHA((max *randomNumber).ToString() + IMSIgn);
            mTicket1 = EncryptionUtils.encodeSHA(randomString);


            return mTicket1;
            
        }


        public String generateValidationTicket2(String ticket1, String username, String password)
        {

            mPreferenceUtils = new PreferenceUtils();
            //para construir el ticket2, primero obtenemos el id del dispositivo
            string idDevice = mPreferenceUtils.GetHardwareId();
            String idVendor = EncryptionUtils.encodeMD5(idDevice).ToUpper();


            //concatenamos el resultado del MD5 del id del dispositivo con el nombre de usuario, la contraseña y el timeStamp 
            //siguiendo este formato:
            // idVendor#username:password#timeStamp

            String DataFromDate = idVendor + sharp + username + ":" + password + sharp + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss");

            

            //obtenemos 3 tokens a partir del ticket1
            String token1 = ticket1.Substring(0, 11);
            String token2 = new string((ticket1.Substring(6, 12)).ToCharArray().Reverse().ToArray());
            String token3 = ticket1.Substring(28, 8);
            String passwordString = token1 + IMSIgn + token2 + token3;
            String keyMD5 = EncryptionUtils.encodeMD5(passwordString).ToUpper();


            var ticket2 = EncryptionUtils.encryptAES_PKCS7(DataFromDate, keyMD5);



            return ticket2;



        }

        private byte[] generateKeyValidationService()
        {
            String token1 = mTicket1.Substring(0, 10);
            String token2 = new string((mTicket1.Substring(6, 12)).ToCharArray().Reverse().ToArray());
            String token3 = mTicket1.Substring(28, 8);
            String passwordString = token1 + IMSIgn + token2 + token3;
            String keyMD5 = EncryptionUtils.encodeMD5(passwordString);

            mKey = Encoding.UTF8.GetBytes(keyMD5);

            return mKey;

        }
        
        public String validationUrlTickets(String userName, String password, UserDataServices datosUsuario)
        {
                  
            String urlValidation = urlBase + validation;

            //String ticket1 = generateTicket1ValidationService();
            //String ticket2 = generateTicket2ValidationService(ticket1, datosUsuario);

            String ticket1 = generateTicket1();
            String ticket2 = generateValidationTicket2(ticket1, userName, password);

            urlValidation +=   "?username=" + userName + "&password=" + password +"&ticket1=" + ticket1 + "&ticket2=" + ticket2;
            //urlValidation += "?password=" + password + "&ticket1=" + ticket1 + "&ticket2=" + ticket2 + "&username=" + userName;
            return urlValidation;

        }

        public String validationUrl(String userName, String password)
        {
            
            String urlValidation = urlBase + validationW8 + "?username=" + userName + "&password=" + password;
            return urlValidation;
        }


        public string generateTicket1()
        {
            String ticket1 = null;          

            //generamos un long aleatorio

            Random random = new Random();
            long randomNumber = random.Next();

            //le concatenamos la cadena "ImSign"

            ticket1 = randomNumber.ToString() + IMSIgn;

            //hacemos el SHA1 
            ticket1 = EncryptionUtils.encodeSHA(ticket1);

            ticket1 = ticket1.ToUpper();

            return ticket1;


           
        }

        

        public String generateRegisterTicket2(String ticket1, UserDataServices datosUsuario)
        {
           

           //para construir el ticket2, primero hacemos un MD5 del Json con los datos del usuario

            //generamos el json
            String json = JsonConvert.SerializeObject(datosUsuario);
            json = EncryptionUtils.encodeMD5(json);

            //concatenamos el resultado del MD5 del JSON con "#" y la fecha

            String DataFromDate = json + sharp + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss");
            //String DataFromDate = EncryptionUtils.encodeMD5(json + sharp + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"));

            //obtenemos 3 tokens a partir del ticket1
            String token1 = ticket1.Substring(0, 11);
            String token2 = new string((ticket1.Substring(6, 12)).ToCharArray().Reverse().ToArray());
            String token3 = ticket1.Substring(28, 8);
            String passwordString = token1 + IMSIgn + token2 + token3;
            String keyMD5 = EncryptionUtils.encodeMD5(passwordString).ToUpper();

                        
            var ticket2 = EncryptionUtils.encryptAES_PKCS7(DataFromDate, keyMD5);

            

            return ticket2;



        }



        public String registerUrl(UserDataServices datosUsuario)
        {

            //URL base
            String urlRegister = urlBase + registrationW8;

            
            return urlRegister;

        }
        public String registerUrlTickets(UserDataServices datosUsuario)
        {

            //URL base
            String urlRegister = urlBase + registration;

            String ticket1 = generateTicket1();
            String ticket2 = generateRegisterTicket2(ticket1, datosUsuario);

            urlRegister += "?ticket1=" + ticket1 + "&ticket2=" + ticket2;


            return urlRegister;

        }


        public String loginUrl(String userName, String password)
        {

            String urlLogin = urlBase + login + "?username=" + userName + "&password=" + password;
            return urlLogin;
        }

        public String getPetitionsUrl(String userName, String password)
        {

            String url = urlBase + getPetitions + "?username=" + userName + "&password=" + password;
            return url;
        }

        public String getDocumentsUrl(String userName, String password)
        {

            String url = urlBase + getDocuments + "?username=" + userName + "&password=" + password;
            return url;
        }

        public String sendFileUrl(String userName, String password)
        {

            String url = urlBase + sendFile + "?username=" + userName + "&password=" + password;
            return url;
        }
        
    }
}
