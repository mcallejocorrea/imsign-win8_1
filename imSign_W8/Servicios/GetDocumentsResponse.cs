﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace imSign_W8.Servicios
{
    public class GetDocumentsResponse
    {
        private List<GetDocumentInfo> listDocuments;

        public List<GetDocumentInfo> ListDocuments
        {
            get
            {
                return this.listDocuments;
            }
            set
            {
                this.listDocuments = value;
            }
        }

    }
}
