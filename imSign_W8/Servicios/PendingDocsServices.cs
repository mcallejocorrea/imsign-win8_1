﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace imSign_W8.Servicios
{
    public class PendingDocsServices
    {
        private int IdUser;
        private List<String> IdDocuments;



        public int idUser

        {
            get
            {
                return this.IdUser;
            }
            set
            {
                this.IdUser = value;
            }
        }

        public List<String> idDocuments
        {
            get
            {
                return this.IdDocuments;
            }
            set
            {
                this.IdDocuments = value;
            }
        }
    }
}
