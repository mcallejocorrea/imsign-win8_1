﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using Windows.Data.Json;
using imSign_W8.Utils;
using imSign_W8.Entidades;
using Windows.Security.Cryptography.Certificates;
using System.IO;

namespace imSign_W8.Servicios
{
    public class WebServices
    {


        public OnResponseListener mListener;
        private PreferenceUtils mPreferenceUtils;


        //public void validationWebService(String activationKey)
        //{

        //    GeneratorUrl generatorUrl = new GeneratorUrl();



        //    String url = generatorUrl.validationUrl(activationKey);


        //    var client = new HttpClient();


        //    var response = client.PostAsync(url, null);
        //    response.Wait();

        //    if (response.Result.IsSuccessStatusCode)
        //    {
        //        var content = response.Result.Content.ReadAsStringAsync();
        //        content.Wait();

        //        var result = content.Result;
                                

        //        ImSignResponse res = JsonConvert.DeserializeObject<ImSignResponse>(result);
        //        //mListener.onResponse(res.Status, res);


        //        //guardamos la clave pública que nos ha devuelto el servicio en PreferenceUtils
        //        String publicKey = res.Ack.PublicKey;
        //        String id = res.Ack.IdKey;
                
        //        mPreferenceUtils.setPublicKey(publicKey);
        //        mPreferenceUtils.setActivationKey(activationKey);
        //        mPreferenceUtils.setID(id);


        //    }
        //    else
        //    {
        //        mListener.onResponse(WebservicesConstant.ERROR_CONECTION_CODE, null);
        //    }
        //}



        public async Task<Boolean> validationWebService(String userName, String password, UserDataServices datosUsuario)
        {

            Boolean retorno = false;

            ImSignResponse res = null;
            GeneratorUrl generatorUrl = new GeneratorUrl();



            //String url = generatorUrl.validationUrl(userName, password);
            String url = generatorUrl.validationUrlTickets(userName, password, datosUsuario);


            var client = new HttpClient();


            var response = client.PostAsync(url, null);
            response.Wait();

            if (response.Result.IsSuccessStatusCode)
            {
                var content = response.Result.Content.ReadAsStringAsync();
                content.Wait();

                var result = content.Result;

                res = JsonConvert.DeserializeObject<ImSignResponse>(result);

                string publicKeyStr = res.Ack.PublicKey;

                EncryptionUtils utils = new EncryptionUtils();

                retorno = await utils.ImportCertificate(publicKeyStr);

            


            }
            else
            {
                retorno = false;
            }


            return retorno;
        }





        public async Task<ImSignResponse> registerWebService(UserDataServices datosUsuario)
        {
            ImSignResponse res = null;

            string certificateString = string.Empty;

            GeneratorUrl generatorUrl = new GeneratorUrl();



            //String url = generatorUrl.registerUrl(datosUsuario);
            String url = generatorUrl.registerUrlTickets(datosUsuario);

                       

            var client = new HttpClient();


            String json = JsonConvert.SerializeObject(datosUsuario);

          
            //enviamos el json con la información del usuario como cuerpo del mensaje
            HttpContent httpContent = new StringContent(json, Encoding.UTF8, "application/json");

            

            var response = client.PostAsync(url, httpContent);
            response.Wait();

            if (response.Result.IsSuccessStatusCode)
            {
                var content = response.Result.Content.ReadAsStringAsync();
                content.Wait();

                var result = content.Result;


                res = JsonConvert.DeserializeObject<ImSignResponse>(result);

            }
            else
            {
                res = null;
            }

            return res;
        }


        public async Task<ImSignResponse> loginWebService(String userName, String password)
        {

          

            ImSignResponse res = null;
            GeneratorUrl generatorUrl = new GeneratorUrl();



            String url = generatorUrl.loginUrl(userName, password);

            var client = new HttpClient();


            var response = client.PostAsync(url, null);
            response.Wait();

            if (response.Result.IsSuccessStatusCode)
            {
                var content = await response.Result.Content.ReadAsStringAsync();               

                var result = content;

                res = JsonConvert.DeserializeObject<ImSignResponse>(result);

                if (res.Status != WebservicesConstant.SUCCESS_CODE)
                {
                    res = null;
                }
                
                    
            }
            else
            {
                res = null;
            }
            
            return res;
        }

        public async Task<GetPetitionsResponse> getPetitionsWebService(String userName, String password)
        {


            GetPetitionsResponse res = null;
            GeneratorUrl generatorUrl = new GeneratorUrl();


            


            String url = generatorUrl.getPetitionsUrl(userName, password);

            var client = new HttpClient();


            var response = client.PostAsync(url, null);
            response.Wait();

            if (response.Result.IsSuccessStatusCode)
            {
                var content = await response.Result.Content.ReadAsStringAsync();

                var result = content;

                res = JsonConvert.DeserializeObject<GetPetitionsResponse>(result);

                
            }
            else
            {
                res = null;
            }

            return res;
        }



        public async Task<GetDocumentsResponse> getDocumentWebService(PendingDocsServices selectedDoc, String userName, String password)
        {


            GetDocumentsResponse res = null;
            GeneratorUrl generatorUrl = new GeneratorUrl();





            String url = generatorUrl.getDocumentsUrl(userName, password);

            var client = new HttpClient();


            String json = JsonConvert.SerializeObject(selectedDoc);


            //enviamos el json con la información del usuario como cuerpo del mensaje
            HttpContent httpContent = new StringContent(json, Encoding.UTF8, "application/json");



            var response = client.PostAsync(url, httpContent);
            response.Wait();
                       

            if (response.Result.IsSuccessStatusCode)
            {
                var content = await response.Result.Content.ReadAsStringAsync();

                var result = content;

                res = JsonConvert.DeserializeObject<GetDocumentsResponse>(result);


            }
            else
            {
                res = null;
            }

            return res;
        }




        public async Task<SendFileResponse> sendFileWebService(String userName, String password, DocInfo docInfo)
        {


            SendFileResponse res = null;
            GeneratorUrl generatorUrl = new GeneratorUrl();


            System.Diagnostics.Debug.WriteLine("------------------------------------" );
            System.Diagnostics.Debug.WriteLine("-----sendFileWebService------");
            System.Diagnostics.Debug.WriteLine("------------------------------------");


            String url = generatorUrl.sendFileUrl(userName, password);
            System.Diagnostics.Debug.WriteLine("url: " + url);
            var client = new HttpClient();


            
            //generamos un json con los datos del documento
            String json = JsonConvert.SerializeObject(docInfo);
            System.Diagnostics.Debug.WriteLine("json: " + json);


            //enviamos el json con la información del documento como cuerpo del mensaje
            HttpContent httpContent = new StringContent(json, Encoding.UTF8, "application/json");



            var response = await client.PostAsync(url, httpContent);
            //response.Wait();
            

            if (response.IsSuccessStatusCode)
            {
                var content = response.Content.ReadAsStringAsync();
                content.Wait();

                var result = content.Result;

                System.Diagnostics.Debug.WriteLine("result: " + result);


                res = JsonConvert.DeserializeObject<SendFileResponse>(result);

            }
            else
            {
                res = null;
            }

            return res;
            
           
        }

        
        
    }


        public interface OnResponseListener {
            void onResponse(int code, ImSignResponse response);
        }
}
