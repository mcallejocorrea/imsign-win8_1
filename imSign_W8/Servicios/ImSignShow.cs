﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace imSign_W8.Servicios
{
    public class ImSignShow
    {
        private String activationKey;

        private Boolean status;

        private DateTime generateKey;

        private int stadistic;


        public String ActivationKey
        {
            get
            {
                return this.activationKey;
            }
            set
            {
                this.activationKey = value;
            }
        }

        public Boolean Status
        {
            get
            {
                return this.status;
            }
            set
            {
                this.status = value;
            }
        }

        public DateTime GenerateKey
        {
            get
            {
                return this.generateKey;
            }
            set
            {
                this.generateKey = value;
            }
        }

        public int Stadistic
        {
            get
            {
                return this.stadistic;
            }
            set
            {
                this.stadistic = value;
            }
        }





    }
}
