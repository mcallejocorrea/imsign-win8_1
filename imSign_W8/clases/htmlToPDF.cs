﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//For converting HTML TO PDF- START
using iTextSharp.text;
using iTextSharp.text.html;
using iTextSharp.text.pdf;
using iTextSharp.text.xml;
using iTextSharp.text.html.simpleparser;
using iTextSharp.tool;
using iTextSharp.tool.xml;
using System.IO;
using System.util;
using System.Text.RegularExpressions;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.Storage.Streams;
//For converting HTML TO PDF- END

using iTextSharp.tool.xml.html;
using iTextSharp.tool.xml.html.table;
using iTextSharp.tool.xml.pipeline;
using iTextSharp.tool.xml.pipeline.css;
using iTextSharp.tool.xml.pipeline.end;
using iTextSharp.tool.xml.pipeline.html;
using iTextSharp.tool.xml.css;
using iTextSharp.tool.xml.css.parser;
using iTextSharp.tool.xml.css.apply;
using iTextSharp.tool.xml.parser;
using iTextSharp.tool.xml.parser.io;
using iTextSharp.tool.xml.parser.state;


namespace imSign_W8.clases
{
    public class htmlToPDF
    {
        public StorageFolder directorio_origen;
        public StorageFolder directorio_destino;
        public StorageFile pdfOutputFile;
        public string cadenaHTML;
        public string cadenaCSS;
        public static String base64;
        public static Dictionary<string, string> dicBase64;

        public htmlToPDF()
        {
            
        }

        public async Task<Boolean> convertImageToBase64(StorageFolder location, string fileName)
        {
            bool exito = true;

            try
            {
                StorageFile imageFile = await location.GetFileAsync(fileName);

                using (var stream = await imageFile.OpenAsync(FileAccessMode.Read))
                {
                    var reader = new DataReader(stream.GetInputStreamAt(0));
                    var bytes = new byte[stream.Size];
                    await reader.LoadAsync((uint)stream.Size);

                    reader.ReadBytes(bytes);
                    base64 = Convert.ToBase64String(bytes);
                }
            }
            catch(Exception)
            {
                exito = false;
            }

            return exito;
        }
        public async Task<Boolean> insertarImagenesEnHTML(StorageFolder location, StorageFile docHtml, StorageFolder carpetaFirma, string nombreDeFirma)
        {
            bool exito = true;

            try
            {
                IReadOnlyList<StorageFile> listaImgs = await location.GetFilesAsync();
                dicBase64 = new Dictionary<string, string>();

                foreach (StorageFile i in listaImgs)
                {
                    Boolean exitoImagen = await convertImageToBase64(location, i.Name);

                    if (exitoImagen)
                    {
                        dicBase64.Add(i.Name, base64);
                    }
                }

                //obtenemos el documento html como cadena
                string cadHTML = await FileIO.ReadTextAsync(docHtml, Windows.Storage.Streams.UnicodeEncoding.Utf8);
                cadHTML = cadHTML.Replace("\n", " ").Replace("\t", " ").Replace("\r", " ");

                //reemplazamos las etiquetas img originaesl por las nuevas con las imagenes en base64
                foreach (KeyValuePair<string, string> kv in dicBase64)
                {
                    cadHTML = cadHTML.Replace("src=\"" + location.Name + "/" + kv.Key, "src=\"data:image/png;base64," + kv.Value);
                }
                if (carpetaFirma != null && nombreDeFirma != null)
                {
                    cadHTML = await insertarFirmaEnHTML(carpetaFirma, cadHTML, nombreDeFirma);
                }

                //reemplazamos el antiguo html con uno nuevo que contenga las imágenes en base64
                StorageFile nuevoDoc = await directorio_origen.CreateFileAsync(docHtml.Name, CreationCollisionOption.ReplaceExisting);
                await Windows.Storage.FileIO.WriteTextAsync(nuevoDoc, cadHTML);
            }
            catch (Exception)
            {
                exito = false;
            }

            return exito;
        }
        private async Task<string> insertarFirmaEnHTML(StorageFolder ruta, string textoHTML, string nomImg)
        {
            string resultado = String.Empty;
            int anchoIMG = Convert.ToInt32(512 * 0.54);
            int altoIMG = Convert.ToInt32(115 * 0.54);

            try
            {
                dicBase64 = new Dictionary<string, string>();
                Boolean exitoImagen = await convertImageToBase64(ruta, nomImg);
                dicBase64.Add(nomImg, base64);

                foreach (KeyValuePair<string, string> kv in dicBase64)
                {
                    //Reemplazamos el literal que identifica el lugar donde va la firma por un tag <img> con la firma en base64
                    resultado = textoHTML.Replace("<!--IMAGEN_FIRMA-->", "<img id=\"etiquetaFirma\" style=\"width:" + anchoIMG + "; height:" + altoIMG + "; border:1px solid black;\" src=\"data:image/png;base64," + kv.Value + "\" />");
                }
            }
            catch (Exception ex)
            {
                resultado = ex.Message;
            }

            return resultado;
        }

        public Byte[] obtenerDocumentoComoArrayDeBytes(string html, string css)
        {
            byte[] resultado; //contendrá el documento PDF final.
            byte[] htmlCad = System.Text.Encoding.UTF8.GetBytes(html);
            byte[] cssCad = System.Text.Encoding.UTF8.GetBytes(css);

            using (MemoryStream ms = new MemoryStream())
            {
                using (Document doc = new Document())
                {
                    using (PdfWriter writer = PdfWriter.GetInstance(doc, ms))
                    {
                        doc.Open();

                        using (MemoryStream msCss = new MemoryStream(cssCad))
                        {
                            using (MemoryStream msHtml = new MemoryStream(htmlCad))
                            {                                
                                iTextSharp.tool.xml.XMLWorkerHelper.GetInstance().ParseXHtml(writer, doc, msHtml, msCss);
                            }
                        }

                        doc.Close();
                    }
                }

                resultado = ms.ToArray();
            }

            return resultado;
        }

        public String getImageRootPath()
        {
            return "";
        }


        public async Task<Byte[]> obtenerDocumentoComoArrayDeBytes(string cadenaHTML)
        {
            byte[] resultado = null; //contendrá el documento PDF final.
            byte[] htmlCad = System.Text.Encoding.UTF8.GetBytes(cadenaHTML);
            //byte[] cssCad = System.Text.Encoding.UTF8.GetBytes(cadenaCSS);

            //StorageFolder carpeta = await StorageFolder.GetFolderFromPathAsync(rutaCarpeta);
            //StorageFile docHtml = await carpeta.GetFileAsync(nombreDocHTML);

            //using (MemoryStream ms = new MemoryStream())
            //{

            StorageFile pdf = await ApplicationData.Current.LocalFolder.CreateFileAsync("PDFmaria.pdf", CreationCollisionOption.ReplaceExisting);
            Stream pdfStream = await pdf.OpenStreamForWriteAsync();

            using (Document doc = new Document())
            {


                using (PdfWriter writer = PdfWriter.GetInstance(doc, pdfStream))
                {
                    doc.Open();

                    HtmlPipelineContext htmlContext = new HtmlPipelineContext(null);
                    htmlContext.SetTagFactory(Tags.GetHtmlTagProcessorFactory());

                    ImSignImageProvider imageProvider = new ImSignImageProvider();
                    htmlContext.SetImageProvider(imageProvider);

                    ICSSResolver cssResolver =   XMLWorkerHelper.GetInstance().GetDefaultCssResolver(true);

                    IPipeline pipeline =
                    new CssResolverPipeline(cssResolver,
                            new HtmlPipeline(htmlContext,
                                new PdfWriterPipeline(doc, writer)));

                    XMLWorker worker = new XMLWorker(pipeline, true);
                    XMLParser p = new XMLParser(worker);

                    // Get the input stream for the SessionState file
                    StorageFile file = await ApplicationData.Current.LocalFolder.GetFileAsync("pagina_prueba.html");

                    try
                    {
                        using (var stream = await file.OpenAsync(FileAccessMode.Read))
                        {
                            var reader = new DataReader(stream.GetInputStreamAt(0));
                            var bytes = new byte[stream.Size];
                            await reader.LoadAsync((uint)stream.Size);
                            reader.ReadBytes(bytes);
                                
                            p.Parse(stream.AsStream());
                        }
                    }
                    catch (Exception e)
                    {
                        string error = e.ToString();
                    }
                        
                    doc.Close();                              
                }
            }

            //    resultado = ms.ToArray();
            //}

            return resultado;
        }

        public async Task<Byte[]> obtenerDocumentoComoArrayDeBytes2(string cadenaHTML)
        {
            byte[] resultado = null; //contendrá el documento PDF final.
            byte[] htmlCad = System.Text.Encoding.UTF8.GetBytes(cadenaHTML);

            StorageFile pdf = await ApplicationData.Current.LocalFolder.CreateFileAsync("PDFmaria.pdf", CreationCollisionOption.ReplaceExisting);
            Stream pdfStream = await pdf.OpenStreamForWriteAsync();

            using (Document doc = new Document())
            {
                using (PdfWriter writer = PdfWriter.GetInstance(doc, pdfStream))
                {
                    doc.Open();

                    HtmlPipelineContext htmlContext = new HtmlPipelineContext(null);
                    htmlContext.SetTagFactory(Tags.GetHtmlTagProcessorFactory());

                    ImSignImageProvider imageProvider = new ImSignImageProvider();
                    htmlContext.SetImageProvider(imageProvider);                   

                    ICSSResolver cssResolver = XMLWorkerHelper.GetInstance().GetDefaultCssResolver(true);

                    IPipeline pipeline =
                    new CssResolverPipeline(cssResolver,
                            new HtmlPipeline(htmlContext,
                                new PdfWriterPipeline(doc, writer)));

                    XMLWorker worker = new XMLWorker(pipeline, true);
                    XMLParser p = new XMLParser(worker);

                    StorageFile file = await ApplicationData.Current.LocalFolder.GetFileAsync("pruebaHTMLMaria.html");

                    try
                    {
                        using (var stream = await file.OpenAsync(FileAccessMode.Read))
                        {
                            var reader = new DataReader(stream.GetInputStreamAt(0));
                            var bytes = new byte[stream.Size];
                            await reader.LoadAsync((uint)stream.Size);
                            reader.ReadBytes(bytes);

                            p.Parse(stream.AsStream());
                        }
                    }
                    catch (Exception e)
                    {
                        string error = e.ToString();
                    }

                    doc.Close();
                }
            }

            return resultado;
        }
    } 
}
