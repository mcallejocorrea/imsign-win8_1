﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Networking;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Storage;
using Windows.Storage;
using Cubisoft.Winrt.Ftp;
using Cubisoft.Winrt.Ftp.Messages;


namespace imSign_W8.clases
{
    public class ftpManager
    {
        public FtpClient CreateFtpClient(String ftpServer, String userName, String password)
        {

            System.Net.NetworkCredential credential = new System.Net.NetworkCredential(userName.Trim(), password.Trim());
            var ftpClient = new FtpClient();


            ftpClient.Credentials = credential;
            ftpClient.HostName = new HostName(ftpServer);
            ftpClient.ServiceName = "21";

            return ftpClient;
        }

        public async Task<Boolean> ConnectAsync(FtpClient ftpClient)
        {
            System.Diagnostics.Debug.WriteLine("-----------------------------------");
            System.Diagnostics.Debug.WriteLine("TestConnectAsync");
            System.Diagnostics.Debug.WriteLine("-----------------------------------");

            

            await ftpClient.ConnectAsync();

            if (ftpClient.IsConnected)
                return true;
            else
                return false;
            
        }

        public async Task CreateFileAsync(FtpClient ftpClient, String Path, StorageFile fileToTransfer, String fileName)
        {
            System.Diagnostics.Debug.WriteLine("-----------------------------------");
            System.Diagnostics.Debug.WriteLine("TestCreateFileAsync");
            System.Diagnostics.Debug.WriteLine("-----------------------------------");

            

           // await ftpClient.ConnectAsync();

          

            if (!(await ftpClient.DirectoryExistsAsync(Path )))
                await ftpClient.CreateDirectoryAsync(Path );

            string testFilePath = Path + "/"  + fileName;

            if (await ftpClient.FileExistsAsync(testFilePath))
                await ftpClient.DeleteFileAsync(testFilePath);

            using (var stream = await ftpClient.OpenWriteAsync(testFilePath))
            {
                byte[] pdfBytes = await BiometricsData.MetadataBuilder.ReadFile(fileToTransfer);

                
                await stream.WriteAsync(pdfBytes.AsBuffer());
                await stream.FlushAsync();
                stream.Dispose();
            }

           // var fileExists = await ftpClient.FileExistsAsync(testFilePath);


            //if (await ftpClient.DirectoryExistsAsync(Path + "pruebaWindows"))
            //{
            //    await ftpClient.DeleteDirectoryAsync(Path + "pruebaWindows", true);
            //}

            await ftpClient.DisconnectAsync();


            System.Diagnostics.Debug.WriteLine("-----------------------------------");
        }

       

    }
}
