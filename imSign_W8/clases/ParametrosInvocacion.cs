﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using imSign_W8.Entidades;

namespace imSign_W8.clases
{
    public class ParametrosInvocacion
    {

        

        private const string uriMemoraBase = "memoracart:///user={0}&IDContrato={1}&numDocumentos={2}&Ruta={3}";
        
        private string nombreUsuario;
        private string idContrato;
        private string rutaDirectorio;
        private int numDocumentos;
        private Dictionary<string, string[]> documentos;
        private string nombreImagen;
        private List<List<Punto>> _allPoints;

        private string nombreFirmante;

        private string uriRecibida;


        public string NombreUsuario
        {
            get
            {
                return this.nombreUsuario;
            }
            set
            {
                this.nombreUsuario = value;
            }
        }

        public string IdContrato
        {
            get
            {
                return this.idContrato;
            }
            set
            {
                this.idContrato = value;
            }
        
        }

        public string RutaDirectorio
        {
            get
            {
                return this.rutaDirectorio;
            }
            set
            {
                this.rutaDirectorio = value;
            }

        }

        public int NumDocumentos
        {
            get
            {
                return this.numDocumentos;
            }
            set
            { 
                this.numDocumentos = value;
            }

        }

        public Dictionary<string, string[]> Documentos
        {
            get
            {
                return this.documentos;
            }
            set
            {
                this.documentos = value;
            }
        }

        public string NombreImagen
        {
            get
            {
                return this.nombreImagen;
            }
            set
            {
                this.nombreImagen = value;
            }
        }

        public string UriMemoraBase
        {
            get
            {
                return uriMemoraBase;
            }
        }

        public List<List<Punto>> AllPoints
        {
            get
            {
                return this._allPoints;
            }
            set
            {
                this._allPoints = value;
            }
        }      

        public string NombreFirmante
        {
            get
            {
                return this.nombreFirmante;
            }
            set
            {
                this.nombreFirmante = value;
            }
        }

        public string UriRecibida
        {
            get
            {
                return this.uriRecibida;
            }
            set
            {
                this.uriRecibida = value;
            }
        }

    }
}
