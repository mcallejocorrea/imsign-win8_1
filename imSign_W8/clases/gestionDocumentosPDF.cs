﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.UI;
using SDKTemplate;
using System.Threading.Tasks;
using Windows.Graphics.Imaging;
using System.Text.RegularExpressions;
using System.Collections;
using System.Xml.Serialization;
using imSign_W8.clases;
using Windows.UI.Input;
using Windows.UI.Xaml.Navigation;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.Storage.Streams;

using iTextSharp.text;
using iTextSharp.text.html;
using iTextSharp.text.pdf;
using iTextSharp.text.xml;
using iTextSharp.tool.xml.exceptions;
using iTextSharp.tool.xml.css.apply;
using iTextSharp.tool.xml.html;
using iTextSharp.text.html.simpleparser;
using iTextSharp.tool;
using iTextSharp.tool.xml;
using iTextSharp.tool.xml.parser;
using iTextSharp.tool.xml.pipeline;
using iTextSharp.tool.xml.pipeline.css;
using iTextSharp.tool.xml.pipeline.end;
using iTextSharp.tool.xml.pipeline.html;
using System.IO;
using Windows.UI.Xaml.Media.Imaging;
using iTextSharp.text.xml.xmp;
using Windows.Networking.BackgroundTransfer;
using System.Threading;
using Windows.Security.Cryptography;
using Windows.Security.Cryptography.Core;




namespace imSign_W8.clases
{
    class gestionDocumentosPDF
    {
        public StorageFolder directorio_origen_html;
        public StorageFolder directorio_origen_css;
        public StorageFolder directorio_origen_imagenes;
        public StorageFolder directorio_origen_firma;
        public StorageFolder directorio_destino;
        public string nombreDocumentoPDF;
        public string nombreDocHtml;
        public string nombreDocFirma;
        public List<string> nombreDocCss;

        private List<DownloadOperation> activeDownloads;
        private CancellationTokenSource cts;

        #region Constructores
        public gestionDocumentosPDF()
        {
            directorio_origen_html = ApplicationData.Current.LocalFolder;
            directorio_origen_css = ApplicationData.Current.LocalFolder;
            directorio_origen_imagenes = ApplicationData.Current.LocalFolder;
            directorio_destino = ApplicationData.Current.LocalFolder;
        }
        public gestionDocumentosPDF(StorageFolder dir_destino)
        {
            directorio_origen_html = ApplicationData.Current.LocalFolder;
            directorio_destino = dir_destino;
        }
        public gestionDocumentosPDF(StorageFolder dir_html, StorageFolder dir_destino)
        {
            directorio_origen_html = dir_html;
            directorio_origen_css = ApplicationData.Current.LocalFolder;
            directorio_origen_imagenes = ApplicationData.Current.LocalFolder;
            directorio_destino = dir_destino;
        }
        public gestionDocumentosPDF(StorageFolder dir_html, StorageFolder dir_css, StorageFolder dir_destino)
        {
            directorio_origen_html = dir_html;
            directorio_origen_css = dir_css;
            directorio_origen_imagenes = ApplicationData.Current.LocalFolder;
            directorio_destino = dir_destino;
        }
        public gestionDocumentosPDF(StorageFolder dir_html, StorageFolder dir_css, StorageFolder dir_imagenes, StorageFolder dir_destino)
        {
            directorio_origen_html = dir_html;
            directorio_origen_css = dir_css;
            directorio_origen_imagenes = dir_imagenes;
            directorio_destino = dir_destino;
        }
        #endregion

        #region Parámetros
        public StorageFolder Directorio_Origen_HTML
        {
            get
            {
                return this.directorio_origen_html;
            }
            set
            {
                this.directorio_origen_html = value;
            }
        }
        public StorageFolder Directorio_Origen_CSS
        {
            get
            {
                return this.directorio_origen_css;
            }
            set
            {
                this.directorio_origen_css = value;
            }
        }
        public StorageFolder Directorio_Origen_IMAGENES
        {
            get
            {
                return this.directorio_origen_imagenes;
            }
            set
            {
                this.directorio_origen_imagenes = value;
            }
        }
        public StorageFolder Directorio_Origen_FIRMA
        {
            get
            {
                return this.directorio_origen_firma;
            }
            set
            {
                this.directorio_origen_firma = value;
            }
        }
        public StorageFolder Directorio_Destino
        {
            get
            {
                return this.directorio_destino;
            }
            set
            {
                this.directorio_destino = value;
            }
        }
        public string NombreDocumentoPDF
        {
            get
            {
                return this.nombreDocumentoPDF;
            }
            set
            {
                this.nombreDocumentoPDF = value;
            }
        }
        public string NombreDocHtml
        {
            get
            {
                return this.nombreDocHtml;
            }
            set
            {
                this.nombreDocHtml = value;
            }
        }
        public string NommbreDocFirma
        {
            get
            {
                return this.nombreDocFirma;
            }
            set
            {
                this.nombreDocFirma = value;
            }
        }
        public List<string> NombreDocCss
        {
            get
            {
                return this.nombreDocCss;
            }
            set
            {
                this.nombreDocCss = value;
            }
        }
        #endregion

        #region Métodos


        Windows.ApplicationModel.Resources.ResourceLoader loader = new Windows.ApplicationModel.Resources.ResourceLoader();

        public async Task<string> transformar_html()
        {
            string resultado = String.Empty;
            htmlToPDF insertadorImagenes = new htmlToPDF();

            try
            {
                //obtener documento html
                StorageFile docHtml = await directorio_origen_html.GetFileAsync(nombreDocHtml);

                //obtener documentos css
                IReadOnlyList<StorageFile> listaImgs = await directorio_origen_css.GetFilesAsync();

                //insertar imágenes en el html
                insertadorImagenes.directorio_origen = directorio_origen_html;
                bool exito = await insertadorImagenes.insertarImagenesEnHTML(directorio_origen_imagenes, docHtml, directorio_origen_firma, nombreDocFirma);

                //crear el documento pdf de salida.              
                StorageFile pdfOutputFile = await directorio_destino.CreateFileAsync(String.Concat(DateTimeOffset.Now.ToString("MM_dd_yy_hh_mm_ss"), "_", nombreDocumentoPDF), CreationCollisionOption.ReplaceExisting);
                resultado = pdfOutputFile.Name;
                Stream pdfStream = await pdfOutputFile.OpenStreamForWriteAsync();

                using (Document doc = new Document())
                {
                    using (PdfWriter writer = PdfWriter.GetInstance(doc, pdfStream))
                    {
                        doc.Open();

                        HtmlPipelineContext htmlContext = new HtmlPipelineContext(null);
                        htmlContext.SetTagFactory(Tags.GetHtmlTagProcessorFactory());

                        ImSignImageProvider imageProvider = new ImSignImageProvider();
                        htmlContext.SetImageProvider(imageProvider);

                        //añadimos la referencia a los ficheros de estilos 
                        ICSSResolver cssResolver = XMLWorkerHelper.GetInstance().GetDefaultCssResolver(false);

                        foreach (StorageFile s in listaImgs)
                        {
                            IRandomAccessStream streamCss = await s.OpenAsync(FileAccessMode.Read);
                            cssResolver.AddCss(XMLWorkerHelper.GetCSS(streamCss.AsStream()));
                            streamCss.Dispose();
                        }

                        //creación del parser
                        IPipeline pipeline =
                        new CssResolverPipeline(cssResolver,
                                new HtmlPipeline(htmlContext,
                                    new PdfWriterPipeline(doc, writer)));

                        XMLWorker worker = new XMLWorker(pipeline, true);
                        XMLParser p = new XMLParser(worker);

                        //conversión a pdf
                        try
                        {
                            using (var stream = await docHtml.OpenAsync(FileAccessMode.Read))
                            {
                                var reader = new DataReader(stream.GetInputStreamAt(0));
                                var bytes = new byte[stream.Size];
                                await reader.LoadAsync((uint)stream.Size);
                                reader.ReadBytes(bytes);

                                p.Parse(stream.AsStream());
                            }
                        }
                        catch (Exception e)
                        {
                            string error = e.ToString();
                        }




                        //cerrar los streams de datos
                        doc.Close();
                        pdfStream.Dispose();
                    }
                }




                
            }
            catch (Exception ex)
            {
                resultado = ex.Message;
            }

            return resultado;
        }

        public async Task<string> transformar_html(StorageFile imagenFirma, ParametrosInvocacion parametros)
        {
            string resultado = String.Empty;
            htmlToPDF insertadorImagenes = new htmlToPDF();

            try
            {
                //obtener documento html
                StorageFile docHtml = await directorio_origen_html.GetFileAsync(nombreDocHtml);

                //obtener documentos css
                IReadOnlyList<StorageFile> listaImgs = await directorio_origen_css.GetFilesAsync();

                //insertar imágenes en el html
                insertadorImagenes.directorio_origen = directorio_origen_html;
                bool exito = await insertadorImagenes.insertarImagenesEnHTML(directorio_origen_imagenes, docHtml, directorio_origen_firma, nombreDocFirma);

                //crear el documento pdf de salida.              
                StorageFile pdfOutputFile = await directorio_destino.CreateFileAsync(String.Concat(DateTimeOffset.Now.ToString("MM_dd_yy_hh_mm_ss"), "_", nombreDocumentoPDF), CreationCollisionOption.ReplaceExisting);
                resultado = pdfOutputFile.Name;
                Stream pdfStream = await pdfOutputFile.OpenStreamForWriteAsync();


                Stream auxPdfStream = new MemoryStream();
                pdfStream.CopyTo(auxPdfStream);

                using (Document doc = new Document())
                {
                    using (PdfWriter writer = PdfWriter.GetInstance(doc, pdfStream))
                    {
                        doc.Open();

                        HtmlPipelineContext htmlContext = new HtmlPipelineContext(null);
                        htmlContext.SetTagFactory(Tags.GetHtmlTagProcessorFactory());

                        ImSignImageProvider imageProvider = new ImSignImageProvider();
                        htmlContext.SetImageProvider(imageProvider);

                        //añadimos la referencia a los ficheros de estilos 
                        ICSSResolver cssResolver = XMLWorkerHelper.GetInstance().GetDefaultCssResolver(false);

                        foreach (StorageFile s in listaImgs)
                        {
                            IRandomAccessStream streamCss = await s.OpenAsync(FileAccessMode.Read);
                            cssResolver.AddCss(XMLWorkerHelper.GetCSS(streamCss.AsStream()));
                            streamCss.Dispose();
                        }

                        //creación del parser
                        IPipeline pipeline =
                        new CssResolverPipeline(cssResolver,
                                new HtmlPipeline(htmlContext,
                                    new PdfWriterPipeline(doc, writer)));

                        XMLWorker worker = new XMLWorker(pipeline, true);
                        XMLParser p = new XMLParser(worker);

                        //conversión a pdf
                        try
                        {
                            using (var stream = await docHtml.OpenAsync(FileAccessMode.Read))
                            {
                                var reader = new DataReader(stream.GetInputStreamAt(0));
                                var bytes = new byte[stream.Size];
                                await reader.LoadAsync((uint)stream.Size);
                                reader.ReadBytes(bytes);

                                p.Parse(stream.AsStream());
                            }
                        }
                        catch (Exception e)
                        {
                            string error = e.ToString();
                        }



                        //mcc
                        //creamos la estructura con los datos biométricos
                        imSign_W8.BiometricsData.DeviceRequest request = new imSign_W8.BiometricsData.DeviceRequest();
                        string structura = await request.crearEstructura(imagenFirma, parametros, auxPdfStream, resultado);

                        doc.AddKeywords(structura);

                        //fin MCC




                        //cerrar los streams de datos
                        doc.Close();
                        pdfStream.Dispose();
                    }
                }





            }
            catch (Exception ex)
            {
                resultado = ex.Message;
            }

            return resultado;
        }



        public async Task<String> generarPDFDatosBiometricos(StorageFile imagenFirma, ParametrosInvocacion parametros, Stream inputPdfStream, String pdfName, StorageFolder pdfOUTFolder)
        {
            String structura = string.Empty;

            try
            {
                
                //creamos la estructura con los datos biométricos
                imSign_W8.BiometricsData.DeviceRequest request = new imSign_W8.BiometricsData.DeviceRequest();
                structura = await request.crearEstructura(imagenFirma, parametros, inputPdfStream, pdfName);

                PdfReader reader = new PdfReader(inputPdfStream);
                StorageFile pdfOutputFile;
                pdfOutputFile = await pdfOUTFolder.CreateFileAsync(string.Format(pdfName.Split('.')[0] + "_{0}" + "2.pdf", DateTimeOffset.Now.ToString("MM_dd_yy_HH_mm_ss")), CreationCollisionOption.ReplaceExisting);

                IRandomAccessStream outputPdfStream = null;

                if (pdfOutputFile != null)
                {
                    outputPdfStream = await pdfOutputFile.OpenAsync(FileAccessMode.ReadWrite);
                }

                PdfStamper stamper = new PdfStamper(reader, outputPdfStream.AsStream(), '1', true);
                IDictionary<string, string> info = new Dictionary<string, string>();
                info.Add("Keywords", structura);

                stamper.MoreInfo = info;

                MemoryStream os = new System.IO.MemoryStream();
                XmpWriter xmp = new XmpWriter(os, info);
                xmp.Close();

                //fin creamos la estructura con los datos biométricos
                stamper.Close();
                outputPdfStream.Dispose();
                inputPdfStream.Dispose();
                await Task.Delay(1000);


               
            }
            catch (Exception ex)
            {
               
            }
            return structura;
           
        }



        public async Task<String> cargarPdfWeb (String downloadUrl)
        {
            // Validating the URI is required since it was received from an untrusted source (user input).
            // The URI is validated by calling Uri.TryCreate() that will return 'false' for strings that are not valid URIs.
            // Note that when enabling the text box users may provide URIs to machines on the intrAnet that require
            // the "Home or Work Networking" capability.
            Uri source;
            int indexArchivo = -1;
            String nombreArchivo = String.Empty;
            String mensajeError = String.Empty;

            cts = new CancellationTokenSource();
            activeDownloads = new List<DownloadOperation>();


            //comprobamos que se trate de un archivo con extensión .pdf
            if(!String.Equals((downloadUrl.Substring((downloadUrl.Length - 4), 4)),".pdf"))
            {

                mensajeError = loader.GetString("errorURIDescargaWeb");
                return mensajeError;
            }


            //comprobamos si se trata de una uri válida
            if (!Uri.TryCreate(downloadUrl, UriKind.Absolute, out source))
            {
                mensajeError = loader.GetString("errorURIDescargaWeb");
                return mensajeError;
            }

            
            //obtenemos el nombre del archivo 
            indexArchivo = downloadUrl.LastIndexOf('/');

            if(indexArchivo == -1)
            {
                mensajeError = loader.GetString("errorURIDescargaWeb");
                return mensajeError;
            }
            else
            {
                nombreArchivo = downloadUrl.Substring(indexArchivo + 1);
            }

          

            

            StorageFile destinationFile;
            try
            {
                destinationFile = await ApplicationData.Current.LocalFolder.CreateFileAsync(
                    nombreArchivo, CreationCollisionOption.GenerateUniqueName);
            }
            catch (FileNotFoundException ex)
            {
                mensajeError = loader.GetString("errorCreacionArchivoLocal");
                return mensajeError;
            }

            BackgroundDownloader downloader = new BackgroundDownloader();
            DownloadOperation download = downloader.CreateDownload(source, destinationFile);

        

            download.Priority = BackgroundTransferPriority.Default;

            

            List<DownloadOperation> requestOperations = new List<DownloadOperation>();
            requestOperations.Add(download);

            // If the app isn't actively being used, at some point the system may slow down or pause long running
            // downloads. The purpose of this behavior is to increase the device's battery life.
            // By requesting unconstrained downloads, the app can request the system to not suspend any of the
            // downloads in the list for power saving reasons.
            // Use this API with caution since it not only may reduce battery life, but it may show a prompt to
            // the user.
            UnconstrainedTransferRequestResult result;
            try
            {
                result = await BackgroundDownloader.RequestUnconstrainedDownloadsAsync(requestOperations);
            }
            catch (System.NotImplementedException)
            {
                mensajeError = loader.GetString("errorDescargaArchivo");
                return mensajeError;
            }

           

            await HandleDownloadAsync(download, true);

            mensajeError = "OK";

            return mensajeError;
        }


        private async Task HandleDownloadAsync(DownloadOperation download, bool start)
        {
            try
            {
              

                // Store the download so we can pause/resume.
                activeDownloads.Add(download);

                //Progress<DownloadOperation> progressCallback = new Progress<DownloadOperation>(DownloadProgress);
                if (start)
                {
                    // Start the download and attach a progress handler.
                    await download.StartAsync().AsTask(cts.Token);
                }
                else
                {
                    // The download was already running when the application started, re-attach the progress handler.
                    await download.AttachAsync().AsTask(cts.Token);
                }

                ResponseInformation response = download.GetResponseInformation();

               
            }
            catch (TaskCanceledException)
            {
                //LogStatus("Canceled: " + download.Guid, NotifyType.StatusMessage);
            }
            catch (Exception ex)
            {
                
                    throw;
                
            }
            finally
            {
                activeDownloads.Remove(download);
            }
        }
        
        #endregion




        public async Task<StorageFile> Base64ToFile(string base64String, string fileName)
        {

            IBuffer data = null;
            data = CryptographicBuffer.DecodeFromBase64String(base64String);


          

            

            StorageFolder tempFolder = ApplicationData.Current.TemporaryFolder;
            StorageFile pdfOutputFile = null;

            try
            {
                pdfOutputFile = await tempFolder.CreateFileAsync(fileName, CreationCollisionOption.ReplaceExisting);
            }
            catch (Exception e)
            {
                String mensaje = e.InnerException.ToString();
                return null;
            }

            IRandomAccessStream outputPdfStream = null;

            if (pdfOutputFile != null)
            {
                outputPdfStream = await pdfOutputFile.OpenAsync(FileAccessMode.ReadWrite);

              

                var res = await outputPdfStream.WriteAsync(data);

            }

            
            outputPdfStream.Dispose();

            return pdfOutputFile;

          
        }
    }
}
