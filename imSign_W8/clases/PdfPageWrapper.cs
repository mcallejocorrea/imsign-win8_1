﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.ComponentModel;
using Windows.Foundation;
using Windows.Data.Pdf;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI.Xaml.Media.Imaging;


namespace imSign_W8.clases
{
    public class  PdfPageWrapper : INotifyPropertyChanged
    {


        public PdfDocument pdfDoc;
        public PdfPage pdfPage;
        public BitmapImage bitmap;
        public uint pageIndex;
        private CancellationTokenSource token;

        public PdfPageWrapper(PdfDocument doc, uint index)
        {

            pdfDoc = doc;
            pageIndex = index;
            pdfPage = pdfDoc.GetPage(pageIndex);

            token = new CancellationTokenSource();

            TriggerPageRender();

            pageIndex++;

        }

        public double Width
        {
            get
            {
                return pdfPage.Size.Width;
            }
        }

        public double Height
        {
            get
            {
                return pdfPage.Size.Height;
            }

            
        }

        public BitmapImage Source
        {
            get
            {
                return bitmap;
            }
        }



        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }


        //método para renderizar la página en el bitmap a varios zooms
        public async void TriggerPageRender(float zoomFactor = 1)
        {

            Windows.Data.Pdf.PdfPageRenderOptions renderOptions = new Windows.Data.Pdf.PdfPageRenderOptions();
            renderOptions.DestinationHeight = (uint)(pdfPage.Size.Height * zoomFactor);

            InMemoryRandomAccessStream stream = new InMemoryRandomAccessStream();

            await pdfPage.RenderToStreamAsync(stream, renderOptions);
            bitmap = new BitmapImage();
            bitmap.SetSource(stream);

            NotifyPropertyChanged("Source");

        }


        public void Release()
        {

        }

        
    }
}
