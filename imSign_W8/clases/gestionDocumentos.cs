﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Core;
using Windows.Storage;

namespace imSign_W8.clases
{
    public class gestionDocumentos
    {
        public async Task<Boolean> cerrarApp()
        {

            Boolean res = false;

            try
            {
                //limpiamos los ficheros locales temporales
                res = await deleteLocalFiles();

                //cerramos la aplicación


                CoreApplication.Exit();
            }
            catch (Exception e)
            {
                res = false;
            }


            return res;


        }

        public async Task<Boolean> deleteLocalFiles()
        {
            Boolean res = false;

            //si se han generado ficheros, los borramos antes de salir de la app
            //limpiamos la carpeta de documentos locales donde se generan los temporales

            try
            {
                StorageFolder carpetaLocal = ApplicationData.Current.LocalFolder;
                var files = (await carpetaLocal.GetFilesAsync());


                foreach (var file in files)
                {
                    await file.DeleteAsync(StorageDeleteOption.Default);
                }

                res = true;
            }

            catch (Exception e)
            {
                res = false;
            }

            return res;
        }
    }
}
