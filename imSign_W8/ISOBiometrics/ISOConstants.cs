﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace imSign_W8.ISOBiometrics
{
    class ISOConstants
    {

        public const float SCALE_INCHES_MILLIMETERS = 25.4f;

        public const double SCALE_INCHES_METERS = 0.000264583;
        public const int X_COORD = 0;
        public const int Y_COORD = 1;
        public const int MAX_SIGNATURE_SIZE = 500;   // Millimeters
        public const int MAX_SIGNATURE_SPEED = 10; //m/s
        public const int MAX_SHORT_VALUE = 65536;
        public const double MAX_SCALING_VALUE = 65520;
        public static double MIN_SCALING_VALUE = Math.Pow(2, -16);
        public static double MAX_FRACTION_FIELD_VALUE = Math.Pow(2, 11);
        public static double MAX_SIGNED_POSITIVE_VALUE = Math.Pow(2, 15) - 1;
        public const double MAX_EXPONENT_FIELD_VALUE = 31;

        public const int HEADER_SDI = 0x53444900;
        public const int HEADER_VERSION = 0x20313000;
        public const int ISO_CHANNELS = 16;
        public const int TIME_SCALE = 1000;

        public const int TYPE_BYTE = 0;
        public const int TYPE_SHORT = 1;
        public const int TYPE_SAMPLE_POINTS = 3;
        public const int TYPE_INTEGER = 4;

        public const int DEFAULT_EXPONENTIAL_VALUE = 0;
        public const int RESERVED_BYTE_HEADER_CLOSING = 0;


        // Channel Item Constants
        public const int X = 15;         // Channel position X "X"
        public const int Y = 14;         // Channel position Y "Y"
        public const int Z = 13;         // Channel position Z "Z"
        public const int VX = 12;        // Channel velocity X "VX"
        public const int VY = 11;        // Channel velocity Y "VY"
        public const int AX = 10;        // Channel acceleration X "AX"
        public const int AY = 9;         // Channel acceleration Y "AY"
        public const int T = 8;          // Channel time "T"
        public const int DT = 7;         // Channel time different "DT"
        public const int F = 6;          // Channel pressure "F"
        public const int S = 5;          // Channel tip swicth state "S"
        public const int TX = 4;         // Channel tilt X "TX"
        public const int TY = 3;         // Channel tilt Y "TY"
        public const int AZ = 2;         // Channel azimuth angle "Az"
        public const int EI = 1;         // Channel elevation angle "EI"
        public const int R = 0;          // Channel rotation "R"

        // Channel Descriptors
        public const int RFU = 0;
        public const int LINEAR_COMPONENT_REMOVED_DESCRIPTOR = 1;
        public const int CONSTANT_VALUE_DESCRIPTOR = 2;
        public const int STANDARD_DEVIATION_VALUE_DESCRIPTOR = 3;
        public const int MEAN_VALUE_DESCRIPTOR = 4;
        public const int MAXIMUM_VALUE_DESCRIPTOR = 5;
        public const int MINIMUM_VALUE_DESCRIPTOR = 6;
        public const int SCALING_VALUE_DESCRIPTOR = 7;


    }
}
