﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace imSign_W8.ISOBiometrics
{
    public class ScalingValueItem
    {

        private int exponentField;     // F
        private int fractionField;     // E


        public ScalingValueItem(int exponentField, int fractionField)
        {
            this.exponentField = exponentField;
            this.fractionField = fractionField;
        }

        public int getExponentField()
        {
            return exponentField;
        }

        public void setExponentField(int exponentField)
        {
            this.exponentField = exponentField;
        }

        public int getFractionField()
        {
            return fractionField;
        }

        public void setFractionField(int fractionField)
        {
            this.fractionField = fractionField;
        }
    }
}
