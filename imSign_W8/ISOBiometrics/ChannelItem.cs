﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace imSign_W8.ISOBiometrics
{
    public class ChannelItem
    {
        public static int FIELD_NOT_INCLUDED = int.MinValue;


        private int scalingValue;
        private int minValue;
        private int maxValue;
        private int meanValue;
        private int standardDeviation;
        private int constantValue;
        private int linearComponentRemove;
        private int[] channelValues;


        public ChannelItem(int scalingValue, int minValue, int maxValue, int meanValue, int standardDeviation, int constantValue,
                       int linearComponentRemove)
        {
            this.scalingValue = scalingValue;
            this.minValue = minValue;
            this.maxValue = maxValue;
            this.meanValue = meanValue;
            this.standardDeviation = standardDeviation;
            this.constantValue = constantValue;
            this.linearComponentRemove = linearComponentRemove;
            initChannelValues();
        }

        private void initChannelValues()
        {
            channelValues = new int[8];
            channelValues[0] = FIELD_NOT_INCLUDED;
            channelValues[1] = linearComponentRemove;
            channelValues[2] = constantValue;
            channelValues[3] = standardDeviation;
            channelValues[4] = minValue;
            channelValues[5] = maxValue;
            channelValues[6] = meanValue;
            channelValues[7] = scalingValue;

        }



        public void setScalingValue(int scalingValue)
        {
            this.scalingValue = scalingValue;
        }

        public void setMinValue(int minValue)
        {
            this.minValue = minValue;
        }

        public void setMaxValue(int maxValue)
        {
            this.maxValue = maxValue;
        }

        public void setMeanValue(int meanValue)
        {
            this.meanValue = meanValue;
        }

        public void setStandardDeviation(int standardDeviation)
        {
            this.standardDeviation = standardDeviation;
        }

        public void setConstantValue(int constantValue)
        {
            this.constantValue = constantValue;
        }

        public void setLinearComponentRemove(int linearComponentRemove)
        {
            this.linearComponentRemove = linearComponentRemove;
        }

        public void setChannelValues(int[] channelValues)
        {
            this.channelValues = channelValues;
        }


        public int getScalingValue()
        {
            return scalingValue;
        }

        public int getMinValue()
        {
            return minValue;
        }

        public int getMaxValue()
        {
            return maxValue;
        }

        public int getMeanValue()
        {
            return meanValue;
        }

        public int getStandardDeviation()
        {
            return standardDeviation;
        }

        public int getConstantValue()
        {
            return constantValue;
        }

        public int getLinearComponentRemove()
        {
            return linearComponentRemove;
        }

        public int[] getChannelValues()
        {
            return channelValues;
        }


        public byte getPreamble()
        {
            short result = 0;
            for (int i = 0; i < channelValues.Length; i++)
                if ((channelValues[i] != FIELD_NOT_INCLUDED))
                {
                    result = (short)(result | (short)(Math.Pow(2, i)));
                }
            return (byte)(result & 0xFF);
        }
    }
}
