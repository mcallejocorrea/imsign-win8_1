﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Graphics.Display;
using imSign_W8.Entidades;

namespace imSign_W8.ISOBiometrics
{
    public class ISOUtils
    {
        //private Context context;
        private List<Byte> mBiometricsByteList;
        private ChannelItem[] mChannelItems;

        public ISOUtils()
        {

            setupISOConfiguration();
        }

        public byte[] getmBiometricsByteList()
        {
            byte[] result = new byte[mBiometricsByteList.Count()];
            for (int i = 0; i < mBiometricsByteList.Count(); i++)
                result[i] = mBiometricsByteList.ElementAt(i);
            return result;
        }


       

        // Returns 16 bits where every position will be 0 or 1 depending on if the channel exist.
        // Needs a boolean array where true means 1 and false means 0.
        public short getChannelInclusion(ChannelItem[] channelItems)
        {

            short result = 0;

            for (int i = 0; i < channelItems.Length; i++)
            {
                if (channelItems[i] != null)
                {
                    result = (short)(result | (short)(Math.Pow(2, i)));
                }
            }
            return result;
        }


        // Setup default configuration for ISO standard
        public void setupISOConfiguration()
        {
            mChannelItems = new ChannelItem[ISOConstants.ISO_CHANNELS];
            mChannelItems[ISOConstants.R] = null;
            mChannelItems[ISOConstants.EI] = null;
            mChannelItems[ISOConstants.AZ] = null;
            mChannelItems[ISOConstants.TY] = null;
            mChannelItems[ISOConstants.TX] = null;
            mChannelItems[ISOConstants.S] = null;
            mChannelItems[ISOConstants.F] = null;
            mChannelItems[ISOConstants.DT] = new ChannelItem(ISOConstants.TIME_SCALE, ChannelItem.FIELD_NOT_INCLUDED,
                    ChannelItem.FIELD_NOT_INCLUDED, ChannelItem.FIELD_NOT_INCLUDED,
                    ChannelItem.FIELD_NOT_INCLUDED, ChannelItem.FIELD_NOT_INCLUDED,
                    ChannelItem.FIELD_NOT_INCLUDED);
            mChannelItems[ISOConstants.T] = null;
            mChannelItems[ISOConstants.AY] = null;
            mChannelItems[ISOConstants.AX] = null;
            mChannelItems[ISOConstants.VY] =
                    new ChannelItem(getVScale(), ChannelItem.FIELD_NOT_INCLUDED,
                    ChannelItem.FIELD_NOT_INCLUDED, ChannelItem.FIELD_NOT_INCLUDED,
                            ChannelItem.FIELD_NOT_INCLUDED, ChannelItem.FIELD_NOT_INCLUDED,
                    ChannelItem.FIELD_NOT_INCLUDED);
            mChannelItems[ISOConstants.VX] =
                    new ChannelItem(getVScale(), ChannelItem.FIELD_NOT_INCLUDED,
                    ChannelItem.FIELD_NOT_INCLUDED, ChannelItem.FIELD_NOT_INCLUDED,
                    ChannelItem.FIELD_NOT_INCLUDED, ChannelItem.FIELD_NOT_INCLUDED,
                    ChannelItem.FIELD_NOT_INCLUDED);
            mChannelItems[ISOConstants.Z] = null;
            mChannelItems[ISOConstants.Y] = new ChannelItem(getXYScale(), ChannelItem.FIELD_NOT_INCLUDED,
                    ChannelItem.FIELD_NOT_INCLUDED, ChannelItem.FIELD_NOT_INCLUDED,
                    ChannelItem.FIELD_NOT_INCLUDED, ChannelItem.FIELD_NOT_INCLUDED,
                    ChannelItem.FIELD_NOT_INCLUDED);
            mChannelItems[ISOConstants.X] = new ChannelItem(getXYScale(), ChannelItem.FIELD_NOT_INCLUDED,
                    ChannelItem.FIELD_NOT_INCLUDED, ChannelItem.FIELD_NOT_INCLUDED,
                    ChannelItem.FIELD_NOT_INCLUDED, ChannelItem.FIELD_NOT_INCLUDED,
                    ChannelItem.FIELD_NOT_INCLUDED);

            mBiometricsByteList = new List<Byte>();

            //Add header construction
            addHeaderToBiometricData();
        }


        private void addHeaderToBiometricData()
        {
            //add HEADER_SDI
            addValueToList(ISOConstants.HEADER_SDI, mBiometricsByteList, ISOConstants.TYPE_INTEGER);
            //add HEADER_VERSION
            addValueToList(ISOConstants.HEADER_VERSION, mBiometricsByteList, ISOConstants.TYPE_INTEGER);
            //add Channel inclusion
            addValueToList(getChannelInclusion(mChannelItems), mBiometricsByteList, ISOConstants.TYPE_SHORT);

            //Loop for all the channel items
            for (int j = mChannelItems.Length - 1; j >= 0; j--)
            {
                //add Preamble and scaling value of X
                if (mChannelItems[j] != null)
                {
                    addValueToList(mChannelItems[j].getPreamble(), mBiometricsByteList, ISOConstants.TYPE_BYTE);
                    for (int i = mChannelItems[j].getChannelValues().Length - 1; i >= 0; i--)
                    {
                        int channelValue = mChannelItems[j].getChannelValues()[i];
                        if (channelValue != ChannelItem.FIELD_NOT_INCLUDED)
                        {
                            switch (i)
                            {
                                case ISOConstants.LINEAR_COMPONENT_REMOVED_DESCRIPTOR:
                                case ISOConstants.CONSTANT_VALUE_DESCRIPTOR:
                                case ISOConstants.STANDARD_DEVIATION_VALUE_DESCRIPTOR:
                                case ISOConstants.MEAN_VALUE_DESCRIPTOR:
                                case ISOConstants.MAXIMUM_VALUE_DESCRIPTOR:
                                case ISOConstants.MINIMUM_VALUE_DESCRIPTOR:
                                    // Works with signed values
                                    if (j == ISOConstants.X || j == ISOConstants.Y || j == ISOConstants.VX || j == ISOConstants.VY
                                            || j == ISOConstants.AX || j == ISOConstants.AY ||
                                            j == ISOConstants.TX || j == ISOConstants.TY)
                                    {
                                        addValueToList(channelValue, mBiometricsByteList, ISOConstants.TYPE_SHORT);
                                        // Works with unsigned values
                                    }
                                    else
                                    {
                                        if (channelValue > ISOConstants.MAX_SIGNED_POSITIVE_VALUE)
                                        {
                                            addValueToList(signedToUnsigned(channelValue),
                                                    mBiometricsByteList,
                                                    ISOConstants.TYPE_SHORT);
                                        }
                                        addValueToList(channelValue, mBiometricsByteList, ISOConstants.TYPE_SHORT);
                                    }
                                    break;
                                case ISOConstants.SCALING_VALUE_DESCRIPTOR:
                                    addValueToList(getScalingBytes(getScalingValues(channelValue,
                                            ISOConstants.DEFAULT_EXPONENTIAL_VALUE)), mBiometricsByteList, ISOConstants.TYPE_SHORT);
                                    break;
                            }
                        }
                    }
                }
            }
            addValueToList(ISOConstants.RESERVED_BYTE_HEADER_CLOSING, mBiometricsByteList, ISOConstants.TYPE_BYTE);

        }


        public void addBodyToBiometricData(List<List<Punto>> listPoints)
        {
            // If bit seven equals to 0 no extended data
            addValueToList(0, mBiometricsByteList, ISOConstants.TYPE_BYTE);
            int numberPoints = 0;

            //obtenemos el número total de puntos de nuestro canvas
            foreach (List<Punto> individualList in listPoints)
            {
                numberPoints += individualList.Count();
            }
            addValueToList(numberPoints, mBiometricsByteList, ISOConstants.TYPE_SAMPLE_POINTS);


            ResolutionScale resolutionScale = DisplayProperties.ResolutionScale;
            

            
            float isoXYScale = getXYScale();
            float isoVScale = getVScale();
            float timeDT = listPoints[0][0].time.ToBinary();

            foreach (List<Punto> individualList in listPoints)
            {
                foreach (Punto individualPoint in individualList)
                {
                   
                    addValueToList((int)(individualPoint.xPos *  isoXYScale), mBiometricsByteList, ISOConstants.TYPE_SHORT);
                    addValueToList((int)(individualPoint.yPos *  isoXYScale), mBiometricsByteList, ISOConstants.TYPE_SHORT);
                    addValueToList((int)(individualPoint.xVel *  isoVScale), mBiometricsByteList, ISOConstants.TYPE_SHORT);
                    addValueToList((int)(individualPoint.yVel *  isoVScale), mBiometricsByteList, ISOConstants.TYPE_SHORT);
                    //Save the time difference between the point and the previous one in ms
                    addValueToList((int)((individualPoint.time.ToBinary() - timeDT)), mBiometricsByteList, ISOConstants.TYPE_SHORT);
                    timeDT = individualPoint.time.ToBinary();

                }
            }




        }

        public int getVScale()
        {
            int result;

            result = (int)Math.Floor((double)(ISOConstants.MAX_SHORT_VALUE / ISOConstants.MAX_SIGNATURE_SPEED));
            // Scale value range
            if (result > ISOConstants.MAX_SCALING_VALUE || result < ISOConstants.MIN_SCALING_VALUE)
            {
                result = -1;
            }
            //Log.d("ISOspeedScale", String.valueOf(result));

            return result;
        }

        public int getXYScale()
        {
            int result;

            result = (int)Math.Floor(((double)ISOConstants.MAX_SHORT_VALUE / ISOConstants.MAX_SIGNATURE_SIZE));
            // Scale value range
            if (result > ISOConstants.MAX_SCALING_VALUE || result < ISOConstants.MIN_SCALING_VALUE)
            {
                result = -1;
            }
            //Log.d("ISOScale", String.valueOf(result));

            return result;
        }

        private void addValueToList(int value, List<Byte> list, int typeValue)
        {
            if (typeValue > ISOConstants.TYPE_SAMPLE_POINTS)
            {
                list.Add((byte)((value & 0xFF000000) >> 24));
            }
            if (typeValue > ISOConstants.TYPE_SHORT)
            {
                list.Add((byte)((value & 0x00FF0000) >> 16));
            }
            if (typeValue > ISOConstants.TYPE_BYTE)
            {
                list.Add((byte)((value & 0x0000FF00) >> 8));
            }
            list.Add((byte)(value & 0x000000FF));
        }

        // This method has two operation: subtract 1 bit and flip all the bits
        private int signedToUnsigned(int originalValue)
        {
            return ~(originalValue - 1);
        }

        // This method has two operation: subtract 1 bit and flip all the bits
        private int unsignedToSigned(int originalValue)
        {
            return (~originalValue) + 1;
        }

        // If present, scaling values shall consist of 2 octets. The 5 most significant bits of the first octet shall constitute
        // the exponent field E, and the remaining 11 bits shall constitute the fraction field F.
        // Return 16 bits as [e4 e3 e2 e1 e0 f10 f9 ..... f0]
        public short getScalingBytes(ScalingValueItem item)
        {
            short result;
            int aux = item.getExponentField() << 11;
            //Log.d("aux", String.valueOf(aux));
            result = (short)(aux | item.getFractionField());
            // Log.d("result", String.valueOf(result));
            return result;
        }

        // Escaling field "e" , has to be setting up to 0, the maximum value of e need to be 31 and the fraction field "f"
        // has to be minor than 2048.
        public ScalingValueItem getScalingValues(int isoScale, int e)
        {
            int f;
            f = (int)(((isoScale / (Math.Pow(2, (e - 16)))) - 1) * (Math.Pow(2, 11)));
            //Log.d("fraction , exponent", String.valueOf(f) + " , " + String.valueOf(e));
            if (f < ISOConstants.MAX_FRACTION_FIELD_VALUE && f >= 0)
            {
                return new ScalingValueItem(e, f);
            }
            else
            {
                if (e < ISOConstants.MAX_EXPONENT_FIELD_VALUE)
                {
                    return getScalingValues(isoScale, ++e);
                }
                else
                {
                    return null;
                }
            }
        }




    }





}
