﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using imSign_W8.Entidades;
using imSign_W8.Servicios;

namespace imSign_W8.Utils
{
   
        public class NumeroNif
        {
            /// <summary>
            /// Tipos de Códigos.
            /// </summary>
            /// <remarks>Aunque actualmente no se utilice el término CIF, se usa en la enumeración
            /// por comodidad</remarks>
            private enum TiposCodigosEnum { NIF, NIE }

            // Número tal cual lo introduce el usuario
            private string numero;
            private TiposCodigosEnum tipo;

            /// <summary>
            /// Parte de Nif: En caso de ser un Nif intracomunitario, permite obtener el cógido del país
            /// </summary>
            public string CodigoIntracomunitario { get; internal set; }
            internal bool EsIntraComunitario { get; set; }

            /// <summary>
            /// Parte de Nif: Letra inicial del Nif, en caso de tenerla
            /// </summary>
            public string LetraInicial { get; internal set; }

            /// <summary>
            /// Parte de Nif: Bloque numérico del NIF. En el caso de un NIF de persona física,
            /// corresponderá al DNI
            /// </summary>
            public int Numero { get; internal set; }

            /// <summary>
            /// Parte de Nif: Dígito de control. Puede ser número o letra
            /// </summary>
            public string DigitoControl { get; internal set; }

            /// <summary>
            /// Valor que representa si el Nif introducido es correcto
            /// </summary>
            public bool EsCorrecto { get; internal set; }

            /// <summary>
            /// Cadena que representa el tipo de Nif comprobado:
            ///     - NIF : Número de identificación fiscal de persona física
            ///     - NIE : Número de identificación fiscal extranjería
            /// </summary>
            public string TipoNif { get { return tipo.ToString(); } }

            /// <summary>
            /// Constructor. Al instanciar la clase se realizan todos los cálculos
            /// </summary>
            /// <param name="numero">Cadena de 9 u 11 caracteres que contiene el DNI/NIF
            /// tal cual lo ha introducido el usuario para su verificación</param>
            private NumeroNif(string numero)
            {
                // Se eliminan los carácteres sobrantes
                numero = EliminaCaracteres(numero);

                // Todo en maýusculas
                numero = numero.ToUpper();

                // Comprobación básica de la cadena introducida por el usuario
                if (numero.Length != 9 && numero.Length != 11)
                {
                    this.EsCorrecto = false;
                    return;
                }

                this.numero = numero;
                Desglosa();

                switch (tipo)
                {
                    case TiposCodigosEnum.NIF:
                    case TiposCodigosEnum.NIE:
                        this.EsCorrecto = CompruebaNif();
                        break;
                    
                }
            }

            #region Preparación del número (desglose)

            /// <summary>
            /// Realiza un desglose del número introducido por el usuario en las propiedades
            /// de la clase
            /// </summary>
            private void Desglosa()
            {
                Int32 n;
                if (numero.Length == 11)
                {
                    // Nif Intracomunitario
                    EsIntraComunitario = true;
                    CodigoIntracomunitario = numero.Substring(0, 2);
                    LetraInicial = numero.Substring(2, 1);
                    Int32.TryParse(numero.Substring(3, 7), out n);
                    DigitoControl = numero.Substring(10, 1);
                    tipo = GetTipoDocumento(LetraInicial[0]);
                }
                else
                {
                    // Nif español
                    tipo = GetTipoDocumento(numero[0]);
                    EsIntraComunitario = false;
                    if (tipo == TiposCodigosEnum.NIF)
                    {
                        LetraInicial = string.Empty;
                        Int32.TryParse(numero.Substring(0, 8), out n);
                    }
                    else
                    {
                        LetraInicial = numero.Substring(0, 1);
                        Int32.TryParse(numero.Substring(1, 7), out  n);
                    }
                    DigitoControl = numero.Substring(8, 1);
                }
                Numero = n;
            }

            /// <summary>
            /// En base al primer carácter del código, se obtiene el tipo de documento que se intenta
            /// comprobar
            /// </summary>
            /// <param name="letra">Primer carácter del número pasado</param>
            /// <returns>Tipo de documento</returns>
            private TiposCodigosEnum GetTipoDocumento(char letra)
            {
                Regex regexNumeros = new Regex("[0-9]");
                if (regexNumeros.IsMatch(letra.ToString()))
                    return TiposCodigosEnum.NIF;

                Regex regexLetrasNIE = new Regex("[XYZ]");
                if (regexLetrasNIE.IsMatch(letra.ToString()))
                    return TiposCodigosEnum.NIE;

                throw new Exception("El código no es reconocible");
               
            }



            /// <summary>
            /// Eliminación de todos los carácteres no numéricos o de texto de la cadena
            /// </summary>
            /// <param name="numero">Número tal cual lo escribe el usuario</param>
            /// <returns>Cadena de 9 u 11 carácteres sin signos</returns>
            private string EliminaCaracteres(string numero)
            {
        
                // All characters that are not numbers or letters.
                string chars = @"[^\w]";
                Regex regex = new Regex(chars);
                return regex.Replace(numero, string.Empty).ToUpper();
       
            }

            #endregion

            #region Cálculos

            private bool CompruebaNif()
            {

                if (numero.Substring(0, 1).ToString() == "Y")
                {
                    numero = 1 + numero.Substring(1, 8);
                    Numero = Int32.Parse(numero.Substring(0,8));
                }
                if (numero.Substring(0, 1).ToString() == "Z")
                {
                    numero = 2 + numero.Substring(1, 8);
                    Numero = Int32.Parse(numero.Substring(0, 8));
                }

                return DigitoControl == GetLetraNif();
            }

            
            /// <summary>
            /// Obtiene la suma de todos los dígitos
            /// </summary>
            /// <returns>de 23, devuelve la suma de 2 + 3</returns>
            private Int32 SumaDigitos(Int32 digitos)
            {
                string sNumero = digitos.ToString();
                Int32 suma = 0;

                for (Int32 i = 0; i < sNumero.Length; i++)
                {
                    Int32 aux;
                    Int32.TryParse(sNumero[i].ToString(), out aux);
                    suma += aux;
                }
                return suma;
            }

            /// <summary>
            /// Obtiene la letra correspondiente al Dni
            /// </summary>
            private string GetLetraNif()
            {
               
                int indice = Numero % 23;
                
                return "TRWAGMYFPDXBNJZSQVHLCKE "[indice].ToString();
            }

            /// <summary>
            /// Obtiene una cadena con el número de identificación completo
            /// </summary>
            public override string ToString()
            {
                string nif;
                string formato = "{0:0000000}";

                if (tipo == TiposCodigosEnum.NIF)
                    formato = "{0:00000000}";

                nif = EsIntraComunitario ? CodigoIntracomunitario :
                    string.Empty + LetraInicial + string.Format(formato, Numero) + DigitoControl;
                return nif;
            }

            #endregion

            /// <summary>
            /// Comprobación de un número de identificación fiscal español
            /// </summary>
            /// <param name="numero">Numero a analizar</param>
            /// <returns>Instancia de <see cref="NumeroNif"/> con los datos del número.
            /// Destacable la propiedad <seealso cref="NumeroNif.EsCorrecto"/>, que contiene la verificación
            /// </returns>
            public static NumeroNif CompruebaNif(string numero)
            {
                return new NumeroNif(numero);
            }

        }

        //public class Imagen
        //{

        //    private static int anchoCuadroFirma = 279;
           
        //    private static int dpiPdf = 72;


        //    private static float anchoCuadroFirmaInches = 2.91f;


        //    public float CalculaEscala(iTextSharp.text.Image imagen)
        //    {
        //        float ret = 0f;

        //        var CurrentDpi = Windows.Graphics.Display.DisplayInformation.GetForCurrentView().RawDpiX;

        //        //obtenemos el ancho en píxeles del cuadro de firma teniendo en cuenta el valor de dpi del dispositivo actual
        //        float currentAnchoCuadroFirma = (CurrentDpi * anchoCuadroFirma) / dpiPdf;

        //        //calculamos el % de reducción necesario para la imagen de firma en caso de una escala del 100%

        //        float porcentaje100 = (anchoCuadroFirma * 100) / currentAnchoCuadroFirma;

        //        //si la escala del dispositivo no es del 100%, aplicamos el factor correspondiente
        //        var currentScale = Windows.Graphics.Display.DisplayInformation.GetForCurrentView().ResolutionScale;

        //        switch (currentScale)
        //        {
        //            case Windows.Graphics.Display.ResolutionScale.Scale100Percent:
        //                ret = porcentaje100;
        //                break;

        //            case Windows.Graphics.Display.ResolutionScale.Scale120Percent:
        //                ret = porcentaje100 * 1.2f;
        //                break;
        //            case Windows.Graphics.Display.ResolutionScale.Scale140Percent:
        //                ret = porcentaje100 * 1.4f;
        //                break;
        //            case Windows.Graphics.Display.ResolutionScale.Scale150Percent:
        //                ret = porcentaje100 * 1.5f;
        //                break;
        //            case Windows.Graphics.Display.ResolutionScale.Scale160Percent:
        //                ret = porcentaje100 * 1.6f;
        //                break;
        //            case Windows.Graphics.Display.ResolutionScale.Scale180Percent:
        //                ret = porcentaje100 * 1.8f;
        //                break;
        //            case Windows.Graphics.Display.ResolutionScale.Scale225Percent:
        //                ret = porcentaje100 * 2.25f;
        //                break;
        //        }


        //        return ret;

        //    }


        //    public float CalculaEscalaImagen(iTextSharp.text.Image imagen)
        //    {
        //        float ret = 0f;

        //        var CurrentDpi = Windows.Graphics.Display.DisplayInformation.GetForCurrentView().LogicalDpi;

        //        //obtenemos el % de reducción respecto al valor de dpi del dispositivo
        //        float porcentajeReduccion = (dpiPdf / CurrentDpi) * 100;

        //        float anchoCuadroFirmaPixels = anchoCuadroFirmaInches * CurrentDpi;

        //        float porcentajeImagen = (anchoCuadroFirmaPixels * 100) / imagen.Width;

        //        ret = (porcentajeReduccion * porcentajeImagen) / 100;
                

        //        return ret;

        //    }
        //}

    public class Validaciones
    {
        public static Boolean datosValidos(string userName, string password, string dni, string email, string telef, out string mensajeError)
        {
            Boolean resultado = true;
            String Messagetext = string.Empty;
            var loader = new Windows.ApplicationModel.Resources.ResourceLoader();
              UserData datosUsuario = new UserData();

              mensajeError = String.Empty;

            //validación de datos obligatorios

            // if (!((tbUser.Text.ToString() != string.Empty) && (this.passwordBox.Password != null) && (this.tbDNI.Text.ToString() != string.Empty) && (this.tbEmail.Text.ToString() != string.Empty)))
            if (!((userName != string.Empty) && (password != null) && (dni != string.Empty) && (email != string.Empty)))
            {
                Messagetext = loader.GetString("datosObligatorios");
                resultado = false;
            }
            else
            {
                if (datosUsuario.IsValidEmail(email) == false)
                {

                    Messagetext = loader.GetString("validacionEmail");
                    resultado = false;

                }

                if (datosUsuario.IsValidPhoneNumber(telef) == false)
                {

                    Messagetext += "\n" + loader.GetString("validacionMovil");
                    resultado = false;

                }


                NumeroNif nif = NumeroNif.CompruebaNif(dni);

                if (nif.EsCorrecto == false)
                {
                    Messagetext += "\n" + loader.GetString("validacionDNI");
                    resultado = false;
                }
            }




            if (Messagetext != string.Empty)
            {
                //var msgDlg = new Windows.UI.Popups.MessageDialog(Messagetext);
                //msgDlg.DefaultCommandIndex = 1;
                //msgDlg.ShowAsync();
                mensajeError = Messagetext;


                

            }

            return resultado;


        }

    }

    public class ConfiguracionUsuario
    {
        public static async Task<String> RegistrarUsuario(UserDataServices datosUsuario)
        {

            Boolean resultadoValidacion = false;
            String resultado = String.Empty;

            Windows.Storage.ApplicationDataContainer localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
            var loader = new Windows.ApplicationModel.Resources.ResourceLoader();
           

            // llamada a servicio de registro para registrar la información del usuario y recibir su clave pública
            WebServices servicios = new WebServices();
            ImSignResponse resRegistro = null;

            try
            {
                resRegistro = await servicios.registerWebService(datosUsuario);
            }
            catch
            {
                resRegistro = null;
                resultadoValidacion = false;

            }

            if (resRegistro != null)
            {
                if (resRegistro.Status != WebservicesConstant.ERROR_USUARIO_EXISTENTE)
                {
                    //llamamos al servicio de validación para obtener la clave pública del usuario
                    resultadoValidacion = await servicios.validationWebService(resRegistro.UserInfo.username, resRegistro.UserInfo.password, datosUsuario);
                }
                else if (resRegistro.Status == WebservicesConstant.ERROR_USUARIO_EXISTENTE)
                {
                    //llamamos al servicio de validación para obtener la clave pública del usuario
                    resultadoValidacion = await servicios.validationWebService(datosUsuario.username, datosUsuario.password, datosUsuario);
                }

                if (!resultadoValidacion)
                {
                    resultado = loader.GetString("errorImportacionCertificado");
                }
                else
                {
                    resultado = loader.GetString("guardadoDatosOK");
                }
            }
            else
            {

                //limpiamos las preferencias de usuario
                localSettings.Values["name"] = string.Empty;
                localSettings.Values["surname"] = string.Empty;
                localSettings.Values["email"] = string.Empty;
                localSettings.Values["tlf"] = string.Empty;
                localSettings.Values["businessName"] = string.Empty;
                localSettings.Values["idCode"] = string.Empty;
                localSettings.Values["username"] = string.Empty;
                localSettings.Values["password"] = string.Empty;

                resultado = loader.GetString("errorServicioRegistro");
            }

            return resultado;
        }
    }



}
