﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Security.Cryptography;
using Windows.Security.Cryptography.Core;
using Windows.Storage.Streams;
using Windows.Security.Cryptography.Certificates;
using Windows.Storage;
using Windows.Storage.Pickers;

namespace imSign_W8.Utils
{
    public class EncryptionUtils
    {


        private static string certificateIssuer = "imSign";
        public static IBuffer keyMaterial = null;
        public enum tipoUsuario { pago, gratis };
       




        public static string encodeSHA(string encode)
        {

            var bufferString = CryptographicBuffer.ConvertStringToBinary(encode, BinaryStringEncoding.Utf8);

            var hashAlgorithmProvider = HashAlgorithmProvider.OpenAlgorithm(HashAlgorithmNames.Sha1);
            
            var bufferHash = hashAlgorithmProvider.HashData(bufferString);

            return CryptographicBuffer.EncodeToHexString(bufferHash);

        }

        public static string encodeSHA(byte[] encode)
        {

            IBuffer bufferString = CryptographicBuffer.CreateFromByteArray(encode);

            var hashAlgorithmProvider = HashAlgorithmProvider.OpenAlgorithm(HashAlgorithmNames.Sha1);

            var bufferHash = hashAlgorithmProvider.HashData(bufferString);

            return CryptographicBuffer.EncodeToHexString(bufferHash);

        }

       public static String encodeMD5(String encode)
        {

            var bufferString = CryptographicBuffer.ConvertStringToBinary(encode, BinaryStringEncoding.Utf8);

            var hashAlgorithmProvider = HashAlgorithmProvider.OpenAlgorithm(HashAlgorithmNames.Md5);

            var bufferHash = hashAlgorithmProvider.HashData(bufferString);
 
            return CryptographicBuffer.EncodeToHexString(bufferHash);

            
        }

       public static String encodeMD5(IBuffer encode)
       {

           var hashAlgorithmProvider = HashAlgorithmProvider.OpenAlgorithm(HashAlgorithmNames.Md5);

           var bufferHash = hashAlgorithmProvider.HashData(encode);

           return CryptographicBuffer.EncodeToHexString(bufferHash);


       }

       public static String encodeMD5(byte[] encode)
       {

           IBuffer bufferString = CryptographicBuffer.CreateFromByteArray(encode);
           var hashAlgorithmProvider = HashAlgorithmProvider.OpenAlgorithm(HashAlgorithmNames.Md5);

           var bufferHash = hashAlgorithmProvider.HashData(bufferString);

           return CryptographicBuffer.EncodeToHexString(bufferHash);


       }
        


        public static String encryptAES(String strToEncrypt, string key) 
        {
            String encryptedString = null;

            String algName = SymmetricAlgorithmNames.AesCbcPkcs7;
            
            IBuffer encrypted;
            IBuffer iv = null;
            IBuffer data = null;           
            try
            {
               
                SymmetricKeyAlgorithmProvider Algorithm = SymmetricKeyAlgorithmProvider.OpenAlgorithm(algName);
               
                
                data = CryptographicBuffer.ConvertStringToBinary(strToEncrypt, BinaryStringEncoding.Utf8);

                // CBC mode needs Initialization vector, here just random data.
               
                iv = CryptographicBuffer.CreateFromByteArray(new byte[] { 0000000000000000 });
               

                //convertimos a IBuffer la key
                // Convert the string to UTF8 binary data.
                IBuffer buffKey = CryptographicBuffer.ConvertStringToBinary(key, BinaryStringEncoding.Utf8);
                CryptographicKey Cryptkey = Algorithm.CreateSymmetricKey(buffKey);

                encrypted = CryptographicEngine.Encrypt(Cryptkey, data, iv);

                encryptedString = CryptographicBuffer.EncodeToHexString(encrypted);

            }
            catch (Exception e)
            {

            }
            return encryptedString;

        }



        public static String decryptAES(IBuffer encryptedData, String key) {
        

            String decryptedString = null;

            String algName = SymmetricAlgorithmNames.AesCbc;
            IBuffer decrypted;
            IBuffer iv = null;
           

            try
            {

                SymmetricKeyAlgorithmProvider Algorithm = SymmetricKeyAlgorithmProvider.OpenAlgorithm(algName);
                
                // CBC mode needs Initialization vector, here just random data.
                // IV property will be set on "Encrypted".

                iv = CryptographicBuffer.GenerateRandom(Algorithm.BlockLength);


                //convertimos a IBuffer la key
                // Convert the string to UTF8 binary data.
                IBuffer buffKey = CryptographicBuffer.ConvertStringToBinary(key, BinaryStringEncoding.Utf8);
                CryptographicKey Cryptkey = Algorithm.CreateSymmetricKey(buffKey);

                decrypted = CryptographicEngine.Decrypt(Cryptkey, encryptedData, iv);

                decryptedString = decrypted.ToString();

            }
            catch (Exception e)
            {

            }
            return decryptedString;


        }


        public static String decryptAES(String encryptedData, String key)
        {
           
            String decryptedString = null;

            String algName = SymmetricAlgorithmNames.AesCbc;
            IBuffer decrypted;
            IBuffer iv = null;


            try
            {

                SymmetricKeyAlgorithmProvider Algorithm = SymmetricKeyAlgorithmProvider.OpenAlgorithm(algName);

                // CBC mode needs Initialization vector, here just random data.
                // IV property will be set on "Encrypted".

                iv = CryptographicBuffer.GenerateRandom(Algorithm.BlockLength);


                //convertimos a IBuffer la key
                // Convert the string to UTF8 binary data.
                IBuffer buffKey = CryptographicBuffer.ConvertStringToBinary(key, BinaryStringEncoding.Utf8);
                CryptographicKey Cryptkey = Algorithm.CreateSymmetricKey(buffKey);


                IBuffer buffEncryptedData = CryptographicBuffer.ConvertStringToBinary(encryptedData, BinaryStringEncoding.Utf8);

                decrypted = CryptographicEngine.Decrypt(Cryptkey, buffEncryptedData, iv);

                decryptedString = decrypted.ToString();

            }
            catch (Exception e)
            {

            }
            return decryptedString;


        }



        public static String encryptAES_PKCS7(String strToEncrypt, string key)
        {
            String encryptedString = null;

            String decryptedString = null;

            String algName = SymmetricAlgorithmNames.AesCbcPkcs7;
            

            IBuffer encrypted, decrypted;
            IBuffer iv = null;
            IBuffer data = null;
            try
            {

                SymmetricKeyAlgorithmProvider Algorithm = SymmetricKeyAlgorithmProvider.OpenAlgorithm(algName);


                data = CryptographicBuffer.ConvertStringToBinary(strToEncrypt, BinaryStringEncoding.Utf8);

                iv = CryptographicBuffer.CreateFromByteArray(new byte[] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 });


                //convertimos a IBuffer la key
                // Convert the string to UTF8 binary data.
               // IBuffer buffKey = CryptographicBuffer.ConvertStringToBinary(key, BinaryStringEncoding.Utf8);

               
                IBuffer buffKey = CryptographicBuffer.DecodeFromHexString(key);

                //IBuffer buffKey = CryptographicBuffer.ConvertStringToBinary(valBinario, BinaryStringEncoding.Utf8);
                CryptographicKey Cryptkey = Algorithm.CreateSymmetricKey(buffKey);

                encrypted = CryptographicEngine.Encrypt(Cryptkey, data, iv);

                encryptedString = CryptographicBuffer.EncodeToHexString(encrypted).ToUpper();


                decrypted = CryptographicEngine.Decrypt(Cryptkey, encrypted, iv);
                decryptedString = CryptographicBuffer.EncodeToHexString(decrypted);

                

            }
            catch (Exception e)
            {

            }
            return encryptedString;

        }



        public static String encryptAES_CBC(String strToEncrypt, byte[] key)
        {
            String encryptedString = null;

            //String algName = SymmetricAlgorithmNames.AesCbcPkcs7;
            String algName = SymmetricAlgorithmNames.AesCbc;

            IBuffer encrypted, decrypted;
            IBuffer iv = null;
            IBuffer data = null;
            try
            {

                SymmetricKeyAlgorithmProvider Algorithm = SymmetricKeyAlgorithmProvider.OpenAlgorithm(algName);


                data = CryptographicBuffer.ConvertStringToBinary(strToEncrypt, BinaryStringEncoding.Utf8);
                IBuffer buffKey = CryptographicBuffer.CreateFromByteArray(key);
                CryptographicKey Cryptkey = Algorithm.CreateSymmetricKey(buffKey);

                


                encrypted = CryptographicEngine.Encrypt(Cryptkey, data, null);

                encryptedString = CryptographicBuffer.EncodeToHexString(encrypted).ToUpper();

                
               

            }
            catch (Exception e)
            {

                string error = e.ToString();

            }
            return encryptedString;

        }

         public static String encryptAES_PKCS7(byte[] dataToEncrypt, CryptographicKey Cryptkey)
         {
             String encryptedString = null;

             //String algName = SymmetricAlgorithmNames.AesCbcPkcs7;
             String algName = SymmetricAlgorithmNames.AesCbc;

             IBuffer encrypted, decrypted;
             IBuffer iv = null;
             IBuffer data = null;
             try
             {

                 SymmetricKeyAlgorithmProvider Algorithm = SymmetricKeyAlgorithmProvider.OpenAlgorithm(algName);


                 data = CryptographicBuffer.CreateFromByteArray(dataToEncrypt);


                 iv = CryptographicBuffer.CreateFromByteArray(new byte[] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 });


                 encrypted = CryptographicEngine.Encrypt(Cryptkey, data, null);

                 //encryptedString = CryptographicBuffer.EncodeToHexString(encrypted).ToUpper();
                 encryptedString = CryptographicBuffer.EncodeToBase64String(encrypted);





                 //probamos a desencriptar
                 // Decrypt the data.
                 decrypted = CryptographicEngine.Decrypt(Cryptkey, encrypted, iv);

             }
             catch (Exception e)
             {

                 string error = e.ToString();

             }
             return encryptedString;

         }


         public static String decryptAES_PKCS7(String encryptedData, String key)
         {
            

             String decryptedString = null;

             String algName = SymmetricAlgorithmNames.AesCbcPkcs7;
             IBuffer decrypted;
             IBuffer iv = null;
             IBuffer data = null;


             try
             {

                 SymmetricKeyAlgorithmProvider Algorithm = SymmetricKeyAlgorithmProvider.OpenAlgorithm(algName);


                 data = CryptographicBuffer.ConvertStringToBinary(encryptedData, BinaryStringEncoding.Utf8);
                 data = CryptographicBuffer.DecodeFromBase64String(encryptedData);

                 // CBC mode needs Initialization vector, here just random data.

                 iv = CryptographicBuffer.CreateFromByteArray(new byte[] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 });
                 // iv = CryptographicBuffer.GenerateRandom(Algorithm.BlockLength);


                 //convertimos a IBuffer la key
                 // Convert the string to UTF8 binary data.
                 IBuffer buffKey = CryptographicBuffer.ConvertStringToBinary(key, BinaryStringEncoding.Utf8);
                 CryptographicKey Cryptkey = Algorithm.CreateSymmetricKey(buffKey);

                 decrypted = CryptographicEngine.Decrypt(Cryptkey, data, iv);

                 decryptedString = CryptographicBuffer.EncodeToHexString(decrypted).ToUpper();


             }
             catch (Exception e)
             {

             }
             return decryptedString;


         }

         public static string GetHash(string inputString)
         {
             HashAlgorithmProvider Algorithm = HashAlgorithmProvider.OpenAlgorithm(HashAlgorithmNames.Sha1);
             IBuffer vector = CryptographicBuffer.ConvertStringToBinary(inputString, BinaryStringEncoding.Utf8);
             IBuffer digest = Algorithm.HashData(vector);

             if (digest.Length != Algorithm.HashLength)
             {
                 throw new System.InvalidOperationException("HashAlgorithmProvider ha fallado al generar el hash con el tamaño adecuado");
             }
             string dataHash = CryptographicBuffer.EncodeToHexString(digest);

             return dataHash;

         }



         public static String generateTicket1()
         {
             String ticket1 = null;

             //generamos un long aleatorio

             Random random = new Random();
             long randomNumber = random.Next();

             //le concatenamos la cadena "ImSign"

             ticket1 = randomNumber.ToString() + "ImSign";

             //hacemos el SHA1 
             ticket1 = EncryptionUtils.encodeSHA(ticket1);

             ticket1 = ticket1.ToUpper();

             return ticket1;
             

         }

         public static String generateTicket2(String ticket1, String datosUsuario)
         {


             //para construir el ticket2, primero hacemos un MD5 del Json con los datos del usuario

             //generamos el json
               String json = EncryptionUtils.encodeMD5(datosUsuario);

             //concatenamos el resultado del MD5 del JSON con "#" y la fecha

             String DataFromDate = json + "#" + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss");


             //obtenemos 3 tokens a partir del ticket1
             String token1 = ticket1.Substring(0, 10);
             String token2 = new string((ticket1.Substring(6, 12)).ToCharArray().Reverse().ToArray());
             String token3 = ticket1.Substring(28, 8);
             String passwordString = token1 + "ImSign" + token2 + token3;
             String keyMD5 = EncryptionUtils.encodeMD5(passwordString).Substring(0,16);

             byte[] key = Encoding.UTF8.GetBytes(keyMD5);

             String ticket2 = Encoding.UTF8.GetString(key, 0, key.Length);
             
             return ticket2.ToUpper();
             
         }


        public static async Task<CryptographicKey> getPublicKey()
         {

             CryptographicKey key = null;

            //obtenemos el archivo .cer

             StorageFolder carpetaCertificados = Windows.ApplicationModel.Package.Current.InstalledLocation;
             carpetaCertificados = await carpetaCertificados.GetFolderAsync("Certificados");


            
            StorageFile certificateFile = null;

             try
             {
                 certificateFile = await carpetaCertificados.GetFileAsync("cert1.pfx");
             }
            catch (Exception e)
             {
                 string error = e.ToString();
             }

                       
            
             // ImportPfxDataAsync accepts the certificate as a Base64 string, so we need to encode it
             IBuffer certificateBuffer = await FileIO.ReadBufferAsync(certificateFile);
             string encodedCertificate = Windows.Security.Cryptography.CryptographicBuffer.EncodeToBase64String(certificateBuffer);
             try
             {

                 await CertificateEnrollmentManager.ImportPfxDataAsync(encodedCertificate, "password", ExportOption.NotExportable, KeyProtectionLevel.NoConsent, InstallOptions.None, "ImSign");

                
                 var task = CertificateStores.FindAllAsync();

                task.AsTask().Wait(); 
                var certlist = task.GetResults();

                foreach (Certificate cert in certlist)
                {
                    if (cert.Issuer.Equals("imSign") && cert.FriendlyName.Equals("ImSign"))
                    {
                        key = PersistedKeyProvider.OpenPublicKeyFromCertificate(cert, HashAlgorithmNames.Sha1, CryptographicPadding.RsaPkcs1V15);
                        break;
                    }
                }

             }
             catch
             {
                 
             }

             return key;
         }


        public async Task<Boolean> ImportCertificate(String encodedCertificate)
        {
            Boolean res = false;

            try
            {


                await CertificateEnrollmentManager.ImportPfxDataAsync(encodedCertificate, "password", ExportOption.NotExportable, KeyProtectionLevel.NoConsent, InstallOptions.None, "ImSign");

               
                res = true;

            }
            catch (Exception e)
            {
                res = false;

            }

            return res;

        }

        public static CryptographicKey GetCryptographicPublicKeyFromCert(string strCert)
        {
            int length;
            CryptographicKey CryptKey = null;
            byte[] bCert = Convert.FromBase64String(strCert);

            // Assume Cert contains RSA public key
            // Find matching OID in the certificate and return public key
            byte[] rsaOID = EncodeOID("1.2.840.113549.1.1.1");
            int index = FindX509PubKeyIndex(bCert, rsaOID, out length);

            // Found X509PublicKey in certificate so copy it.
            if (index > -1)
            {
                byte[] X509PublicKey = new byte[length];
                Array.Copy(bCert, index, X509PublicKey, 0, length);

                AsymmetricKeyAlgorithmProvider AlgProvider = AsymmetricKeyAlgorithmProvider.OpenAlgorithm(AsymmetricAlgorithmNames.RsaPkcs1);
                CryptKey = AlgProvider.ImportPublicKey(CryptographicBuffer.CreateFromByteArray(X509PublicKey));
            }

            return CryptKey;
        }

        static int FindX509PubKeyIndex(byte[] Reference, byte[] value, out int length)
        {
            int index = -1;
            bool found;
            length = 0;

            for (int n = 0; n < Reference.Length; n++)
            {
                if ((Reference[n] == value[0]) && (n + value.Length < Reference.Length))
                {
                    index = n;
                    found = true;

                    for (int m = 1; m < value.Length; m++)
                    {

                        if (Reference[n + m] != value[m])
                        {
                            found = false;
                            break;
                        }
                    }

                    if (found) break;
                    else index = -1;
                }
            }

            if (index > -1)
            {
                // Find outer Sequence
                while (index > 0 && Reference[index] != 0x30) index--;
                index--;
                while (index > 0 && Reference[index] != 0x30) index--;
            }

            if (index > -1)
            {
                // Find the length of encoded Public Key
                if ((Reference[index + 1] & 0x80) == 0x80)
                {
                    int numBytes = Reference[index + 1] & 0x7F;
                    for (int m = 0; m < numBytes; m++)
                    {
                        length += (Reference[index + 2 + m] << ((numBytes - 1 - m) * 8));
                    }

                    length += 4;
                }
                else
                {
                    length = Reference[index + 1] + 2;
                }
            }

            return index;
        }

        static public byte[] EncodeOID(string szOID)
        {
            int[] OIDNums;
            byte[] pbEncodedTemp = new byte[64];
            byte[] pbEncoded = null;
            int n, index, num, count;

            OIDNums = ParseOID(szOID);
            pbEncodedTemp[0] = 6;
            pbEncodedTemp[2] = Convert.ToByte(OIDNums[0] * 40 + OIDNums[1]);

            count = 1;

            for (n = 2, index = 3; n < OIDNums.Length; n++)
            {
                num = OIDNums[n];

                if (num >= 16384)
                {
                    pbEncodedTemp[index++] = Convert.ToByte(num / 16384 | 0x80);
                    num = num % 16384;
                    count++;
                }

                if (num >= 128)
                {
                    pbEncodedTemp[index++] = Convert.ToByte(num / 128 | 0x80);
                    num = num % 128;
                    count++;
                }

                pbEncodedTemp[index++] = Convert.ToByte(num);
                count++;
            }

            pbEncodedTemp[1] = Convert.ToByte(count);
            pbEncoded = new byte[count + 2];
            Array.Copy(pbEncodedTemp, 0, pbEncoded, 0, count + 2);

            return pbEncoded;
        }

        static public int[] ParseOID(string szOID)
        {
            int nlast, n = 0;
            bool fFinished = false;
            int count = 0;
            int[] dwNums = null;

            do
            {
                nlast = n;
                n = szOID.IndexOf(".", nlast);
                if (n == -1) fFinished = true;
                count++;
                n++;
            } while (fFinished == false);

            dwNums = new int[count];
            count = 0;
            fFinished = false;

            do
            {
                nlast = n;
                n = szOID.IndexOf(".", nlast);
                if (n != -1)
                {
                    dwNums[count] = Convert.ToInt32(szOID.Substring(nlast, n - nlast), 10);
                }
                else
                {
                    fFinished = true;
                    dwNums[count] = Convert.ToInt32(szOID.Substring(nlast, szOID.Length - nlast), 10);
                }

                n++;
                count++;
            } while (fFinished == false);

            return dwNums;
        }


        public static CryptographicKey GenerateSymmetricKey()
        {
            String algName = SymmetricAlgorithmNames.AesCbcPkcs7;
         

            CryptographicKey key;
            // Create an SymmetricKeyAlgorithmProvider object for the algorithm specified on input.
            SymmetricKeyAlgorithmProvider Algorithm = SymmetricKeyAlgorithmProvider.OpenAlgorithm(algName);


            // Generate a symmetric key.
            keyMaterial = CryptographicBuffer.GenerateRandom(16);


            try
            {
                key = Algorithm.CreateSymmetricKey(keyMaterial);
            }
            catch (ArgumentException ex)
            {
               
                return null;
            }

            
            
            return key;
        }


       

        public static async Task<CryptographicKey> getCertificateKeys()
        {

            CryptographicKey publickey = null;

            try
            {

                
                var task = CertificateStores.FindAllAsync();

                task.AsTask().Wait();
                var certlist = task.GetResults();

                foreach (Certificate cert in certlist)
                {
                    if(cert.Issuer.Equals(certificateIssuer))
                    {
                       
                        //obtenemos la clave publica del certificado
                        publickey = PersistedKeyProvider.OpenPublicKeyFromCertificate(cert, HashAlgorithmNames.Sha1, CryptographicPadding.RsaPkcs1V15);
                        break;
                    }
                }                

            }
            catch (Exception e)
            {
                string error;
                error = e.ToString();
            }

            return publickey;
        }

        public static async Task<String> cifradoClave2(CryptographicKey publicKey, IBuffer dataToEncrypt)
        {
            String resultado = string.Empty;
            AsymmetricKeyAlgorithmProvider Algorithm = AsymmetricKeyAlgorithmProvider.OpenAlgorithm(AsymmetricAlgorithmNames.RsaPkcs1);
            IBuffer iv = null;
           
            try
            {
                var encryptedData = CryptographicEngine.Encrypt(publicKey, dataToEncrypt, iv);
               
                resultado = CryptographicBuffer.EncodeToBase64String(encryptedData);              
                
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
            }

            return resultado;
        }

        public static string encodeSHA512(string encode)
        {

            var bufferString = CryptographicBuffer.ConvertStringToBinary(encode, BinaryStringEncoding.Utf8);

            var hashAlgorithmProvider = HashAlgorithmProvider.OpenAlgorithm(HashAlgorithmNames.Sha512);

            var bufferHash = hashAlgorithmProvider.HashData(bufferString);

            return CryptographicBuffer.EncodeToHexString(bufferHash).ToUpper();

        }

    }
}
