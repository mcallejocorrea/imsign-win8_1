﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.System.Profile;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.Devices.Geolocation;


namespace imSign_W8.Utils
{
    public class PreferenceUtils
    {
    
    

        //Configuration Preferences

        private String USER = "user";
        private String USER_FTP = "userFtp";
        private String URL_FTP = "urlFtp";
        private String PASSWORD_FTP = "passwordFtp";
        private String PUBLIC_KEY = "publicKey";
        private String ACTIVATION_KEY = "activationKey";
        private String ID_KEY = "keyID";
        private String CHAIN = "iMSign";
        
        private byte[] key;
        private String publickey = "MIIBvTCCASYCCQDVOnLbEReNwjANBgkqhkiG9w0BAQUFADAjMSEwHwYDVQQDExhSYWZhZWwgQm9u\nYXJkZWxsIExlbnphbm8wHhcNMTIxMjIwMTIxMjU5WhcNMjIxMjE4MTIxMjU5WjAjMSEwHwYDVQQD\nExhSYWZhZWwgQm9uYXJkZWxsIExlbnphbm8wgZ8wDQYJKoZIhvcNAQEBBQADgY0AMIGJAoGBAJ+h\n6Pea1y/kIfFTL8OkRfYbGkcNJ1c4i/7vbr3H2sOwoCfW4KbsAwApIgZyUn6A5IJC1gLmJ6kQlb0y\nJLUHB1d2K4yHrnsNwkX8U0TV8hvI0ZWltYpiiq6aRZUDHLHMPa+geJOP9yI7r1ZLFWEM+lHslw2+\nhzRMDg8FAtUe7CArAgMBAAEwDQYJKoZIhvcNAQEFBQADgYEAj++atA7+zG7RUeDFUwzva/andJ9g\nzN3NYDPoZGNkFrk65LlopHzFGJ54f1gd3RvjHVFqjKDo3pJ9KQNWosMu2Co9Eq6TtUVLBZqs+m/Y\nJcJ4B60jh7bvYsinDL6uwJM16rDnPlZKT6nDLmSJM1yhamce2HMd7sBSHCELXYS6AuI=";
 

       

        private void setKey()
        {
           
            //obtenemos el id del dispositivo
            HardwareToken token = HardwareIdentification.GetPackageSpecificToken(null);
            IBuffer hardwareId = token.Id;
            String deviceId = hardwareId.ToString();


            String random = EncryptionUtils.encodeSHA(deviceId + CHAIN);
            String token1 = random.Substring(0, 11);
            String token2 = random.Substring(6, 18).Reverse().ToString();
            String token3 = random.Substring(28, 36);
            String passwordString = token1 + token2 + CHAIN + token3;
            String keyMD5 = EncryptionUtils.encodeMD5(passwordString);

            key = Encoding.UTF8.GetBytes(keyMD5);

           
        }

        private void saveSecureString(String key, String value)
        {

            String secureKey = EncryptionUtils.encryptAES(key, Encoding.UTF8.GetString(this.key,0,this.key.Length));
            String secureValue = EncryptionUtils.encryptAES(value, Encoding.UTF8.GetString(this.key, 0, this.key.Length));

            ApplicationData.Current.LocalSettings.Values.Add(secureKey, secureValue);

        }


        private void removeSecureString(String key)
        {
            ApplicationData.Current.LocalSettings.Values.Remove(key);
           
        }

        private String getSecureString(String key)
        {
            object result = null;
            String preferencesStr = null;

            String secureKey = EncryptionUtils.encryptAES_PKCS7(key, this.publickey);
            if (ApplicationData.Current.LocalSettings.Values.ContainsKey(key))
            {
                ApplicationData.Current.LocalSettings.Values.TryGetValue(key, out result);
               preferencesStr = result.ToString();
            }

           if (preferencesStr == null)
            {
                return null;
            }
            else
            {
                return EncryptionUtils.decryptAES_PKCS7(preferencesStr, this.publickey);
            }
        }



        public void setID(String id)
        {

            if (id != null)
            {
                saveSecureString(ID_KEY, id);
            }
            else
            {
                removeSecureString(ID_KEY);
            }
        }

        public String getActivationKey()
        {
            return getSecureString(ACTIVATION_KEY);
        }

        //Get  public key
        public String getPublicKey()
        {
            //return getSecureString(PUBLIC_KEY);

            return this.publickey;
        }

        // Set public key
        public void setPublicKey(String name)
        {

            if (name != null)
            {
                saveSecureString(PUBLIC_KEY, name);
            }
            else
            {
                removeSecureString(PUBLIC_KEY);
            }
        }


        public void setActivationKey(String name)
        {

            if (name != null)
            {
                saveSecureString(ACTIVATION_KEY, name);
            }
            else
            {
                removeSecureString(ACTIVATION_KEY);
            }

        }

        public string GetHardwareId()
        {
            var token = HardwareIdentification.GetPackageSpecificToken(null);
            var hardwareId = token.Id;
            var dataReader = Windows.Storage.Streams.DataReader.FromBuffer(hardwareId);

            byte[] bytes = new byte[hardwareId.Length];
            dataReader.ReadBytes(bytes);

            return BitConverter.ToString(bytes);
        }  


       

    }
}
