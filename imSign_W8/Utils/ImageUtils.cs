﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace imSign_W8.Utils
{
    class ImageUtils
    {

       
            private static int anchoCuadroFirma = 279;

            private static int dpiPdf = 72;


            private static float anchoCuadroFirmaInches = 2.91f;


            public float CalculaEscala(iTextSharp.text.Image imagen)
            {
                float ret = 0f;

                var CurrentDpi = Windows.Graphics.Display.DisplayInformation.GetForCurrentView().RawDpiX;

                //obtenemos el ancho en píxeles del cuadro de firma teniendo en cuenta el valor de dpi del dispositivo actual
                float currentAnchoCuadroFirma = (CurrentDpi * anchoCuadroFirma) / dpiPdf;

                //calculamos el % de reducción necesario para la imagen de firma en caso de una escala del 100%

                float porcentaje100 = (anchoCuadroFirma * 100) / currentAnchoCuadroFirma;

                //si la escala del dispositivo no es del 100%, aplicamos el factor correspondiente
                var currentScale = Windows.Graphics.Display.DisplayInformation.GetForCurrentView().ResolutionScale;

                switch (currentScale)
                {
                    case Windows.Graphics.Display.ResolutionScale.Scale100Percent:
                        ret = porcentaje100;
                        break;

                    case Windows.Graphics.Display.ResolutionScale.Scale120Percent:
                        ret = porcentaje100 * 1.2f;
                        break;
                    case Windows.Graphics.Display.ResolutionScale.Scale140Percent:
                        ret = porcentaje100 * 1.4f;
                        break;
                    case Windows.Graphics.Display.ResolutionScale.Scale150Percent:
                        ret = porcentaje100 * 1.5f;
                        break;
                    case Windows.Graphics.Display.ResolutionScale.Scale160Percent:
                        ret = porcentaje100 * 1.6f;
                        break;
                    case Windows.Graphics.Display.ResolutionScale.Scale180Percent:
                        ret = porcentaje100 * 1.8f;
                        break;
                    case Windows.Graphics.Display.ResolutionScale.Scale225Percent:
                        ret = porcentaje100 * 2.25f;
                        break;
                }


                return ret;

            }


            public float CalculaEscalaImagen(iTextSharp.text.Image imagen)
            {
                float ret = 0f;

                var CurrentDpi = Windows.Graphics.Display.DisplayInformation.GetForCurrentView().LogicalDpi;

                //obtenemos el % de reducción respecto al valor de dpi del dispositivo
                float porcentajeReduccion = (dpiPdf / CurrentDpi) * 100;

                float anchoCuadroFirmaPixels = anchoCuadroFirmaInches * CurrentDpi;

                float porcentajeImagen = (anchoCuadroFirmaPixels * 100) / imagen.Width;

                ret = (porcentajeReduccion * porcentajeImagen) / 100;


                return ret;

            }
        
    }
}
