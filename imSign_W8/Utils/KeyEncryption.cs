﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using Windows.Security.Cryptography;
using Windows.Security.Cryptography.Core;
using Windows.Storage.Streams;

namespace imSign_W8.Utils
{
    public class KeyEncryption
    {

        static IBuffer buffKeyPair;

        public KeyEncryption()
        {
           

            // Create a symmetric session key.
           // String strSymmetricAlgName = SymmetricAlgorithmNames.AesCbc;
            String strSymmetricAlgName = SymmetricAlgorithmNames.AesCbcPkcs7;
            UInt32 symmetricKeyLength = 16;
            IBuffer buffSessionKey;
            this.SampleCreateSymmetricSessionKey(
                strSymmetricAlgName,
                symmetricKeyLength,
                out buffSessionKey);

            // Create an asymmetric key pair.
            String strAsymmetricAlgName = AsymmetricAlgorithmNames.RsaPkcs1;
            UInt32 asymmetricKeyLength = 1024;
            IBuffer buffPublicKey;
            this.SampleCreateAsymmetricKeyPair(
                strAsymmetricAlgName,
                asymmetricKeyLength,
                out buffPublicKey);

            // Encrypt the symmetric session key by using the asymmetric public key.
            IBuffer buffEncryptedSessionKey;
            this.SampleAsymmetricEncryptSessionKey(
                strAsymmetricAlgName,
                buffSessionKey,
                buffPublicKey,
                out buffEncryptedSessionKey);

            // Decrypt the symmetric session key by using the asymmetric private key
            // that corresponds to the public key used to encrypt the session key.
            this.SampleAsymmetricDecryptSessionKey(
                strAsymmetricAlgName,
                strSymmetricAlgName,
                buffEncryptedSessionKey);
        }

        public void SampleCreateSymmetricSessionKey(
            string strSymmetricAlgName,
            UInt32 keyLength,
            out IBuffer buffSessionKey)
        {
            // Open a symmetric algorithm provider for the specified algorithm.
            SymmetricKeyAlgorithmProvider objAlg = SymmetricKeyAlgorithmProvider.OpenAlgorithm(strSymmetricAlgName);

            // Create a symmetric session key.
            IBuffer keyMaterial = CryptographicBuffer.GenerateRandom(keyLength);
            CryptographicKey sessionKey = objAlg.CreateSymmetricKey(keyMaterial);

            buffSessionKey = keyMaterial;
        }

        public void SampleCreateAsymmetricKeyPair(
            String strAsymmetricAlgName,
            UInt32 keyLength,
            out IBuffer buffPublicKey)
        {
            // Open the algorithm provider for the specified asymmetric algorithm.
            AsymmetricKeyAlgorithmProvider objAlgProv = AsymmetricKeyAlgorithmProvider.OpenAlgorithm(strAsymmetricAlgName);

            // Demonstrate use of the AlgorithmName property (not necessary to create a key pair).
            String strAlgName = objAlgProv.AlgorithmName;

            // Create an asymmetric key pair.
            CryptographicKey keyPair = objAlgProv.CreateKeyPair(keyLength);

            // Export the public key to a buffer for use by others.
            buffPublicKey = keyPair.ExportPublicKey();

            // You should keep your private key (embedded in the key pair) secure. For  
            // the purposes of this example, however, we're just copying it into a
            // static class variable for later use during decryption.
            buffKeyPair = keyPair.Export();
        }

        public void SampleAsymmetricEncryptSessionKey(
            String strAsymmetricAlgName,
            IBuffer buffSessionKeyToEncrypt,
            IBuffer buffPublicKey,
            out IBuffer buffEncryptedSessionKey)
        {
            // Open the algorithm provider for the specified asymmetric algorithm.
            AsymmetricKeyAlgorithmProvider objAlgProv = AsymmetricKeyAlgorithmProvider.OpenAlgorithm(strAsymmetricAlgName);

            // Import the public key from a buffer.
            CryptographicKey publicKey = objAlgProv.ImportPublicKey(buffPublicKey);

            // Encrypt the session key by using the public key.
            buffEncryptedSessionKey = CryptographicEngine.Encrypt(publicKey, buffSessionKeyToEncrypt, null);
        }

        public void SampleAsymmetricDecryptSessionKey(
            String strAsymmetricAlgName,
            String strSymmetricAlgName,
            IBuffer buffEncryptedSessionKey)
        {
            // Open the algorithm provider for the specified asymmetric algorithm.
            AsymmetricKeyAlgorithmProvider objAsymmAlgProv = AsymmetricKeyAlgorithmProvider.OpenAlgorithm(strAsymmetricAlgName);

            // Import the public key from a buffer. You should keep your private key
            // secure. For the purposes of this example, however, the private key is
            // just stored in a static class variable.
            CryptographicKey keyPair = objAsymmAlgProv.ImportKeyPair(buffKeyPair);

            // Use the private key embedded in the key pair to decrypt the session key.
            IBuffer buffDecryptedSessionKey = CryptographicEngine.Decrypt(keyPair, buffEncryptedSessionKey, null);

            // Convert the decrypted session key into a CryptographicKey object that
            // can be used to decrypt the message that it previously encrypted (not shown).
            SymmetricKeyAlgorithmProvider objSymmAlgProv = SymmetricKeyAlgorithmProvider.OpenAlgorithm(strSymmetricAlgName);
            CryptographicKey sessionKey = objSymmAlgProv.CreateSymmetricKey(buffDecryptedSessionKey);
        }

    }
}
