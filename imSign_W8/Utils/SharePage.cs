﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.DataTransfer;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Navigation;
using Windows.Foundation;
using Windows.Storage;
using Windows.Storage.Pickers;


namespace imSign_W8.Utils
{
    public class SharePage
    {
        private DataTransferManager dataTransferManager;
        StorageFile pdfFirmado = null;
        string pdfName = String.Empty;



        public bool enviarPorMail(StorageFile pdf)
        {
            bool resultado = false;

            // Register the current page as a share source.
            this.dataTransferManager = DataTransferManager.GetForCurrentView();
            this.dataTransferManager.DataRequested += new TypedEventHandler<DataTransferManager, DataRequestedEventArgs>(this.OnDataRequested);
            this.pdfFirmado = pdf;

            DataTransferManager.ShowShareUI();

            return resultado;
        }

      
        protected void OnNavigatedTo(NavigationEventArgs e)
        {
            // Register the current page as a share source.
            this.dataTransferManager = DataTransferManager.GetForCurrentView();
            this.dataTransferManager.DataRequested += new TypedEventHandler<DataTransferManager, DataRequestedEventArgs>(this.OnDataRequested);
        }

        protected void OnNavigatedFrom(NavigationEventArgs e)
        {
            // Unregister the current page as a share source.
            this.dataTransferManager.DataRequested -= new TypedEventHandler<DataTransferManager, DataRequestedEventArgs>(this.OnDataRequested);
        }

         //When share is invoked (by the user or programatically) the event handler we registered will be called to populate the datapackage with the
         //data to be shared.
        private void OnDataRequested(DataTransferManager sender, DataRequestedEventArgs e)
        {
            // Call the scenario specific function to populate the datapackage with the data to be shared.
            if (GetShareContent(e.Request))
            {
                // Out of the datapackage properties, the title is required. If the scenario completed successfully, we need
                // to make sure the title is valid since the sample scenario gets the title from the user.
                if (String.IsNullOrEmpty(e.Request.Data.Properties.Title))
                {
                    e.Request.FailWithDisplayText("Se ha producido un error al intentar compartir la información.");
                }
            }
        }

        protected bool GetShareContent(DataRequest request)
        {
            bool succeeded = false;




            List<StorageFile> pickedFiles = new List<StorageFile>();
            pickedFiles.Add(pdfFirmado);

           

            Uri uri = new Uri("imsign:navigate?page=SecondaryPage");


            if (pickedFiles != null)
            {
                DataPackage requestData = request.Data;
                requestData.Properties.Title = "imSign";
                requestData.Properties.ContentSourceApplicationLink = uri;
                requestData.SetStorageItems(pickedFiles);
                
                succeeded = true;
            }
            else
            {
                request.FailWithDisplayText("Select the files you would like to share and try again.");
            }
            return succeeded;
        }



    }
}
