﻿using System;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using Windows.Storage;
using SDKTemplate;
using imSign_W8;


namespace SDKTemplate
{
    /// <summary>
    /// Proporciona un comportamiento específico de la aplicación para complementar la clase Application predeterminada.
    /// </summary>
    sealed partial class App : Application
    {


        public String uriProtocol = null;
        /// <summary>
        /// Inicializa el objeto de aplicación Singleton.  Esta es la primera línea de código creado
        /// ejecutado y, como tal, es el equivalente lógico de main() o WinMain().
        /// </summary>
        public App()
        {
            this.InitializeComponent();
            this.Suspending += OnSuspending;
        }

        /// <summary>
        /// Se invoca cuando la aplicación la inicia normalmente el usuario final.  Se usarán otros puntos
        /// se usará por ejemplo cuando la aplicación se inicie para abrir un archivo específico.
        /// </summary>
        /// <param name="e">Información detallada acerca de la solicitud y el proceso de inicio.</param>
        protected override void OnLaunched(LaunchActivatedEventArgs e)
        {

#if DEBUG
            if (System.Diagnostics.Debugger.IsAttached)
            {
                this.DebugSettings.EnableFrameRateCounter = true;
            }
#endif

            Frame rootFrame = Window.Current.Content as Frame;

            // No repetir la inicialización de la aplicación si la ventana tiene contenido todavía,
            // solo asegurarse de que la ventana está activa.
            if (rootFrame == null)
            {
                // Crear un marco para que actúe como contexto de navegación y navegar a la primera página.
                rootFrame = new Frame();
                // Establecer el idioma predeterminado
                rootFrame.Language = Windows.Globalization.ApplicationLanguages.Languages[0];

                rootFrame.NavigationFailed += OnNavigationFailed;

                if (e.PreviousExecutionState == ApplicationExecutionState.Terminated)
                {
                    //TODO: Cargar el estado de la aplicación suspendida previamente
                }

                // Poner el marco en la ventana actual.
                Window.Current.Content = rootFrame;
            }

            if (rootFrame.Content == null)
            {
                // Cuando no se restaura la pila de navegación para navegar a la primera página,
                // configurar la nueva página al pasar la información requerida como parámetro
                // parámetro
               // rootFrame.Navigate(typeof(MainPage), e.Arguments);
               // rootFrame.Navigate(typeof(imSign_W8.SecondaryPage));
                rootFrame.Navigate(typeof(imSign_W8.Views.Login));
               
            }
            // Asegurarse de que la ventana actual está activa.
            Window.Current.Activate();
        }

        /// <summary>
        /// Se invoca cuando se produce un error en la navegación a una página determinada
        /// </summary>
        /// <param name="sender">Marco que produjo el error de navegación</param>
        /// <param name="e">Detalles sobre el error de navegación</param>
        void OnNavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            throw new Exception("Failed to load Page " + e.SourcePageType.FullName);
        }

        /// <summary>
        /// Se invoca al suspender la ejecución de la aplicación.  El estado de la aplicación se guarda
        /// sin saber si la aplicación se terminará o se reanudará con el contenido
        /// de la memoria aún intacto.
        /// </summary>
        /// <param name="sender">Origen de la solicitud de suspensión.</param>
        /// <param name="e">Detalles sobre la solicitud de suspensión.</param>
        private void OnSuspending(object sender, SuspendingEventArgs e)
        {
            var deferral = e.SuspendingOperation.GetDeferral();
            //TODO: Guardar el estado de la aplicación y detener toda actividad en segundo plano
            deferral.Complete();
        }


        protected override void OnActivated(IActivatedEventArgs args)
        {
            base.OnActivated(args);


            if (args.Kind == ActivationKind.Protocol)
            {
                ProtocolActivatedEventArgs eventArgs = args as ProtocolActivatedEventArgs;

                Frame rootFrame = Window.Current.Content as Frame;

                this.uriProtocol = eventArgs.Uri.AbsoluteUri.ToString();

                
                    // Cuando no se restaura la pila de navegación para navegar a la primera página,
                    // configurar la nueva página al pasar la información requerida como parámetro
                    // parámetro
                     rootFrame.Navigate(typeof(MainPage));  
                               // Asegurarse de que la ventana actual está activa.
                Window.Current.Activate();



                 
            }
        }

        protected override void OnFileActivated(FileActivatedEventArgs args)
        {
         
            Frame rootFrame = Window.Current.Content as Frame;
            StorageFile documentoACargar = null;

            if(args.Files.Count > 0)
            {
                documentoACargar = args.Files[0] as StorageFile;
            }

            rootFrame.Navigate(typeof(SecondaryPage), documentoACargar);  
        }
    }
}
