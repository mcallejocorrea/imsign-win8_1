﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace imSign_W8.BiometricsData
{
    public class Parameters
    {
        private String Keyrelationship;
        private String Hash_algorithm;
        private String KeyEncryptionAlgorithm;
        private String KeyEncryption;
        private String KeyEncryptionLocator;
        //private byte[] key;
        private String Key;
        private String BlockEncryptionAlgorithm;
        private String BlockEncryption;


        public String keyrelationship
        {
            get
            {
                return this.Keyrelationship;
            }
            set
            {
                this.Keyrelationship = value;
            }
        }

        public String hash_algorithm
        {
            get
            {
                return this.Hash_algorithm;
            }
            set
            {
                this.Hash_algorithm = value;
            }
        }

        public String keyEncryptionAlgorithm
        {
            get
            {
                return this.KeyEncryptionAlgorithm;
            }
            set
            {
                this.KeyEncryptionAlgorithm = value;
            }
        }

        public String keyEncryption
        {
            get
            {
                return this.KeyEncryption;
            }
            set
            {
                this.KeyEncryption = value;
            }
        }
        
        public String keyEncryptionLocator
        {
            get
            {
                return this.KeyEncryptionLocator;
            }

            set
            {
                this.KeyEncryptionLocator = value;
            }
        }

        //public byte[] Key
        //{
        //    get
        //    {
        //        return this.key;
        //    }
        //    set
        //    {
        //        this.key = value;
        //    }
        //}
        public String key
        {
            get
            {
                return this.Key;
            }
            set
            {
                this.Key = value;
            }
        }

        public String blockEncryptionAlgorithm
        {
            get
            {
                return this.BlockEncryptionAlgorithm;
            }
            set
            {
                this.BlockEncryptionAlgorithm = value;
            }
        }

        public String blockEncryption
        {
            get
            {
                return this.BlockEncryption;
            }
            set
            {
                this.BlockEncryption = value;
            }
        }

    }
}
