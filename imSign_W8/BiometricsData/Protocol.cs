﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace imSign_W8.BiometricsData
{
    public class Protocol
    {
        private String City;
        private String Country;
        //private DateTime date;
        private String Date;
        private String Name;
        //private int number;
        private String Number;

        public String city
        {
            get
            {
                return this.City;
            }
            set
            {
                this.City = value;
            }
        }

        public String country
        {
            get
            {
                return this.Country;
            }
            set
            {
                this.Country = value;
            }
        }

        //public DateTime Date
        //{
        //    get
        //    {
        //        return this.date;
        //    }
        //    set
        //    {
        //        this.date = value;
        //    }
        //}
        public String date
        {
            get
            {
                return this.Date;
            }
            set
            {
                this.Date = value;
            }
        }
        public String name
        {
            get
            {
                return this.Name;
            }
            set
            {
                this.Name = value;
            }
        }

        //public int Number
        //{
        //    get
        //    {
        //        return this.number;
        //    }
        //    set
        //    {
        //        this.number = value;
        //    }

        //}
        public String number
        {
            get
            {
                return this.Number;
            }
            set
            {
                this.Number = value;
            }

        }
        
    }
}
