﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace imSign_W8.BiometricsData
{
    public class UserInfo
    {
        private String Name;
        private String SurName;
        private String IdCode;
        private String Email;
        private String BusinessName;
        private String Tlf;
        private Boolean Premium;
        private int IdUser;
        private int NKey;
        private String UserName;
        private String Password;

        private String Namedoc;



        public String name
        {
            get
            {
                return this.Name;
            }
            set
            {
                this.Name = value;
            }
        }

        public String surName
        {
            get
            {
                return this.SurName;
            }
            set
            {
                this.SurName = value;
            }
        }

        public String idCode
        {
            get
            {
                return this.IdCode;
            }
            set
            {
                this.IdCode = value;
            }
        }

        public String email
        {
            get
            {
                return this.Email;
            }
            set
            {
                this.Email = value;
            }
        }
        public String businessName
        {
            get
            {
                return this.BusinessName;
            }
            set
            {
                this.BusinessName = value;
            }
        }

        public String tlf
        {
            get
            {
                return this.Tlf;
            }
            set
            {
                this.Tlf = value;
            }
        }

        public Boolean premium
        {
            get
            {
                return this.Premium;
            }
            set
            {
                this.Premium = value;
            }
        }

        public int idUser
        {
            get
            {
                return this.IdUser;
            }
            set
            {
                this.IdUser = value;
            }
        }
        public int nKey
        {
            get
            {
                return this.NKey;
            }
            set
            {
                this.NKey = value;
            }
        }


        public String namedoc
        {
            get
            {
                return this.Namedoc;
            }
            set
            {
                this.Namedoc = value;
            }
        }

        public String username
        {
            get
            {
                return this.UserName;
            }
            set
            {
                this.UserName = value;
            }
        }

        public String password
        {
            get
            {
                return this.Password;
            }
            set
            {
                this.Password = value;
            }
        }

        public UserInfo() {
            name = "";
            surName = "";
            idCode = "";
            email = "";
            businessName = "";
            tlf = "";
            premium = false;
            idUser = -1;
            nKey = -1;
            namedoc = "";
        }

    }
}
