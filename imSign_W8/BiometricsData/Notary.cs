﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace imSign_W8.BiometricsData
{
    public class Notary
    {
        private Parameters Parameters;
        private Protocol Protocol;

        public Parameters parameters
        {
            get
            {
                return this.Parameters;
            }
            set
            {
                this.Parameters = value;
            }
        }

        public Protocol protocol
        {
            get
            {
                return this.Protocol;
            }
            set
            {
                this.Protocol = value;
            }

        }
    }
}
