﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using Windows.Storage;
using Windows.Storage.Streams;
using imSign_W8.ISOBiometrics;
using imSign_W8.clases;
using imSign_W8.Entidades;
using System.IO;

namespace imSign_W8.BiometricsData
{
    public class DeviceRequest
    {

        private AdvancedSignage AdvancedSignage;
        private SystemBiometrics System;
        private Validation Validation;

        public AdvancedSignage advancedSignage
        {
            get
            {
                return this.AdvancedSignage;
            }
            set
            {
                this.AdvancedSignage = value;
            }

        }

        public SystemBiometrics system
        {
            get
            {
                return this.System;
            }
            set
            {
                this.System = value;
            }
        }

        public Validation validation
        {
            get
            {
                return this.Validation;
            }
            set
            {
                this.Validation = value;
            }
        }

        public async Task<String> crearEstructura(StorageFile imagenFirma, List<List<Punto>> listaPuntos, StorageFile pdf, UserData datosUsuario)
        {


            //obtenemos la clave pública del certificado local

            //  await obtenerClavePublica();

            //Inicializamos el ISO Parser
            ISOUtils isoUtils = new ISOUtils();
            isoUtils.addBodyToBiometricData(listaPuntos);



            MetadataBuilder metadata = new MetadataBuilder();


            String signMetadata = string.Empty;
            signMetadata += await metadata.metadataXML(isoUtils.getmBiometricsByteList(), imagenFirma, pdf.Name, pdf, datosUsuario);


            return signMetadata;
        }




        public async Task<String> crearEstructura(StorageFile imagenFirma, ParametrosInvocacion parametros, Stream StreamPdf, string pdfName)
        {


            //obtenemos la clave pública del certificado local

            //  await obtenerClavePublica();

            //Inicializamos el ISO Parser
            ISOUtils isoUtils = new ISOUtils();
            isoUtils.addBodyToBiometricData(parametros.AllPoints);



            MetadataBuilder metadata = new MetadataBuilder();


            String signMetadata = string.Empty;
            signMetadata += await metadata.metadataXML(isoUtils.getmBiometricsByteList(), imagenFirma, pdfName, StreamPdf, parametros.NombreUsuario);


            return signMetadata;
        }




    }
}
