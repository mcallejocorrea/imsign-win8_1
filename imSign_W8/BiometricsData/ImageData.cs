﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace imSign_W8.BiometricsData
{
    public class ImageData
    {
        private byte[] Data;
        private String Digest;
        private int Id;
        private String MimeType;


        public byte[] data
        {
            get
            {
                return this.Data;
            }
            set
            {
                this.Data = value;
            }
        }

        public String digest
        {
            get
            {
                return this.Digest;
            }
            set
            {
                this.Digest = value;
            }
        }


        public int id
        {
            get
            {
                return this.Id;
            }
            set
            {
                this.Id = value;
            }
        }

        public String mimeType
        {
            get
            {
                return this.MimeType;
            }
            set
            {
                this.MimeType = value;
            }
        }
    }
}
