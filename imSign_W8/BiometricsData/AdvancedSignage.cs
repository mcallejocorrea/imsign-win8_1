﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace imSign_W8.BiometricsData
{
    public class AdvancedSignage
    {

        private Biometrics Biometrics;
        private String ClientData;
        private String FormValidation;
        private String PetitionID;
        private int SerialNumber;
        private String SignValidation;
        private String Status;
        private String User;
        private String VersionID;

        public Biometrics biometrics
        {
            get
            {
                return this.Biometrics;
            }
            set
            {
                this.Biometrics = value;

            }
        }

        public String clientData
        {
            get
            {
                return this.ClientData;
            }
            set
            {
                this.ClientData = value;
            }
        }

        public String formValidation
        {
            get
            {
                return this.FormValidation;
            }
            set
            {
                this.FormValidation = value;
            }
        }

        public String petitionID
        {
            get
            {
                return this.PetitionID;
            }
            set
            {
                this.PetitionID = value;
            }
        }

        public int serialNumber
        {
            get
            {
                return this.SerialNumber;
            }
            set
            {
                this.SerialNumber = value;
            }
        }


        public String signValidation
        {
            get
            {
                return this.SignValidation;
            }
            set
            {
                this.SignValidation = value;
            }
        }

        public String status
        {
            get
            {
                return this.Status;
            }
            set
            {
                this.Status = value;
            }
        }


        public String user
        {
            get
            {
                return this.User;
            }
            set
            {
                this.User = value;
            }
        }

        public String versionID
        {
            get
            {
                return this.VersionID;
            }
            set
            {
                this.VersionID = value;
            }
        }



    }
}
