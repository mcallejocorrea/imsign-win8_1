﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace imSign_W8.BiometricsData
{
    public class SystemBiometrics
    {
        private Boolean CertificateRequired;
        private String DeviceID;
        private String DeviceTimeStamp;
        private double Latitude;
        private double Longitude;
        private String Precision;
        private String TrainingVersion;
        private String Version;
        


        public bool certificateRequired
        {
            get
            {
                return this.CertificateRequired;
            }
            set
            {
                this.CertificateRequired = value;
            }
        }

        public String deviceID
        {
            get
            {
                return this.DeviceID;
            }
            set
            {
                this.DeviceID = value;
            }
        }

        public String deviceTimeStamp
        {
            get
            {
                return this.DeviceTimeStamp;
            }
            set
            {
                this.DeviceTimeStamp = value;
            }
        }

        public double latitude
        {
            get
            {
                return this.Latitude;
            }
            set
            {
                this.Latitude = value;
            }
        }
        public double longitude
        {
            get
            {
                return this.Longitude;
            }
            set
            {
                this.Longitude = value;
            }
        }

        public String precision
        {
            get
            {
                return this.Precision;
            }
            set
            {
                this.Precision = value;
            }
        }

        public String trainingVersion
        {
            get
            {
                return this.TrainingVersion;
            }
            set
            {
                this.TrainingVersion = value;
            }
        }
        public String version
        {
            get
            {
                return this.Version;
            }
            set
            {
                this.Version = value;
            }
        }
    }
}
