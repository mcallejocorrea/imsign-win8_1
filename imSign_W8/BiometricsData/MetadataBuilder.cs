﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using imSign_W8.Utils;
using imSign_W8.Entidades;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.Devices.Geolocation;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using Windows.System.Profile;
using Windows.Security.Cryptography;
using Windows.Security.Cryptography.Core;
using Windows.Security.Cryptography.Certificates;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Data.Xml.Dom;
using System.Xml;
using System.Xml.Serialization;
using System.Runtime.Serialization.Json;
using System.Xml.Linq;
using imSign_W8.BiometricsData;




namespace imSign_W8.BiometricsData
{
    public class MetadataBuilder
    {
        private static  long max = long.MaxValue;
        private static  String CHAIN = "indraSignature";
        private PreferenceUtils mPreferenceUtils = new PreferenceUtils();

        private EncryptionUtils encryptionUtils = new EncryptionUtils();

        private Geoposition pos = null;

        private CryptographicKey symmetricKey = null;



        


        public async Task<String> metadataXML(byte[] biometricsBytes, StorageFile imageBytes, String pdfTitle, StorageFile pdf, UserData datosUsuario )
         {


            //generamos la clave simétrica que utilizaremos para cifrar la información
             symmetricKey = EncryptionUtils.GenerateSymmetricKey();
          
            UserInfo mUserInfo = null;
           
            mUserInfo = new UserInfo();
           
            UserInfo clientData = new UserInfo();
            clientData.namedoc = pdfTitle;
            clientData.name = datosUsuario.name;
            clientData.surName = datosUsuario.surName;
            clientData.idCode = datosUsuario.idCode;
            clientData.email = datosUsuario.email;
            clientData.businessName = datosUsuario.businessName;
            clientData.tlf = datosUsuario.tlf;
            clientData.premium = datosUsuario.premium;


            //generamos el json con los datos de configuración del usuario junto con el nombre del pdf
         
            String json = JsonConvert.SerializeObject(clientData);

           


            //***********************************************************************
            //<deviceRequest><advancedSignage><biometrics><biometricsdata>
            //***********************************************************************
           // randomKey = GenerateRandomKey(json);

            //***********************************************************************
            //<deviceRequest><advancedSignage><biometrics><decryption><notary><parameters>
            //***********************************************************************

            Parameters parametros = new Parameters();

            //en el caso concreto de Mémora, este valor es constante porque solo hay una clave pública para todo el aplicativo. en caso de ImSign, este campo debe rellenarse
            //con el IdKey que nos devuelve el servicio Validation
            parametros.keyrelationship = Constantes.keyRelationship;
            parametros.hash_algorithm = Constantes.hashAlg;
            parametros.keyEncryptionAlgorithm = Constantes.keyEcryptionAlg;
            parametros.keyEncryption = Constantes.keyEncryption;

            //<advancedSignage><biometrics><decryption><notary><parameters><keyEncryptionLocator> es el MD5 de la clave pública
            //if (mPreferenceUtils.getPublicKey() != null)
            //{
            //    parametros.KeyEncryptionLocator = EncryptionUtils.encodeMD5(mPreferenceUtils.getPublicKey());
            //    parametros.Key = mPreferenceUtils.getPublicKey();
            //}


            //obtenemos la clave pública del certificado
            
            CryptographicKey publicKey;
            
            //si es un usuario premium, el certificado se ha cargado en la App a través del servicio de validation
            if(datosUsuario.premium == true)
            {
                publicKey = await EncryptionUtils.getCertificateKeys();
            }
            else
            {

                publicKey = await EncryptionUtils.getPublicKey();
                
            }
         


            //<advancedSignage><biometrics><decryption><notary><parameters><keyEncryptionLocator> es el MD5 de la clave pública
           // parametros.keyEncryptionLocator = EncryptionUtils.encodeMD5(publicKeyStr);

            //<advancedSignage><biometrics><decryption><notary><parameters><key> es la clave simétrica cifrada con la clave pública

            //parametros.key = EncryptionUtils.cifradoClave(publicKey.ExportPublicKey(), EncryptionUtils.keyMaterial);
            parametros.key = EncryptionUtils.cifradoClave2(publicKey, EncryptionUtils.keyMaterial).Result;



            parametros.blockEncryption = Constantes.blockEncryption;
            parametros.blockEncryptionAlgorithm = Constantes.blockEncryptionAlg;


            //***********************************************************************
            //<deviceRequest><advancedSignage><biometrics><decryption><notary><protocol>
            //***********************************************************************
            Protocol protocol = new Protocol();
            protocol.city = Constantes.city;
            protocol.country = Constantes.country;
            protocol.date = Constantes.date;
            protocol.number = Constantes.number;
            protocol.name = Constantes.name;

            Notary notary = new Notary();
            notary.parameters = parametros;
            notary.protocol = protocol;


            //***********************************************************************
            //<deviceRequest><advancedSignage><biometrics><imageData>
            //***********************************************************************
            ImageData imageData = new ImageData();
            


            //data --> imagen de la firma sin cifrar y codificada en base64
            string data = await StorageFileToBase64(imageBytes);
            imageData.data = Encoding.UTF8.GetBytes(data);


            //es el hash de la etiqueta [imageData][data] anterior convertido en hexadecimal
            imageData.digest = EncryptionUtils.GetHash(data);
            imageData.mimeType = Constantes.mimeType;


            //***********************************************************************
            //<deviceRequest><advancedSignage><biometrics>
            //***********************************************************************
            Biometrics biometrics = new Biometrics();

            //***********************************************************************
            //<deviceRequest><advancedSignage><biometrics><decryption>
            //***********************************************************************

            Decryption decryption = new Decryption();
            decryption.method = Constantes.method;
            decryption.notary = notary;

            //digestBiometrics = EncryptionUtils.encodeSHA(Encoding.UTF8.GetString(biometricsBytes, 0, biometricsBytes.Length));
                     

            AdvancedSignage advancedSignage = new AdvancedSignage();
            advancedSignage.biometrics = biometrics;

            

            //***********************************************************************
            //<deviceRequest><advancedSignage><clientData>
            //***********************************************************************
          
            ////generamos el json con los datos de configuración del usuario junto con el nombre del pdf
            //clientData.Namedoc = pdfTitle;
            //String json = JsonConvert.SerializeObject(clientData);

            advancedSignage.clientData = json;


            ////***********************************************************************
            ////<deviceRequest><advancedSignage><biometrics><biometricsdata>
            ////***********************************************************************
            //son los datos de la firma cifrados con la clave simétrica

                      
            biometrics.biometricsData = EncryptionUtils.encryptAES_PKCS7(biometricsBytes, symmetricKey);

            biometrics.decription = decryption;
            biometrics.imageData = imageData;


             //***********************************************************************
            //<deviceRequest><advancedSignage><formValidation>
            //***********************************************************************
          
            //hash de (clientData + pdf completo original)

            byte[] pdfBytes = await ReadFile(pdf); //bytes del PDF completo original
            byte[] userDataBytes = Encoding.UTF8.GetBytes(json); //bytes de clientData

            ////concatenamos ambos
            IEnumerable<byte> concatenado =  userDataBytes.Concat(pdfBytes);

            //lo convertimos en un array de bytes
            byte [] resultado = concatenado.ToArray();

            //hacemos el hash           
            String formValidationStr = EncryptionUtils.encodeSHA(resultado);

            advancedSignage.formValidation = formValidationStr;


            //***********************************************************************
            //<deviceRequest><advancedSignage><petitionID>
            //***********************************************************************
            //es el nombre del PDF original
            advancedSignage.petitionID = pdfTitle;


            //***********************************************************************
            //<deviceRequest><advancedSignage><serialNumber>
            //***********************************************************************
          
            //advancedSignage.setSerialNumber(001);


            //***********************************************************************
            //<deviceRequest><advancedSignage><signValidation>
            //***********************************************************************

            //es el hash de la etiqueta [imageData][data] anterior convertido en hexadecimal
            advancedSignage.signValidation = EncryptionUtils.GetHash(data);

           
            //***********************************************************************
            //<deviceRequest><advancedSignage><status>
            //***********************************************************************
            
            advancedSignage.status = Constantes.status;

            //***********************************************************************
            //<deviceRequest><advancedSignage><user>
            //***********************************************************************
            advancedSignage.user = clientData.idUser.ToString();

            //***********************************************************************
            //<deviceRequest><advancedSignage><versionID>
            //***********************************************************************

            advancedSignage.versionID = Constantes.versionID;


            //***********************************************************************
            //<deviceRequest><system>
            //***********************************************************************

            SystemBiometrics system = new SystemBiometrics();

            //***********************************************************************
            //<deviceRequest><system><certificateRequired>
            //***********************************************************************
            system.certificateRequired = false;


            //***********************************************************************
            //<deviceRequest><system><deviceID>
            //***********************************************************************
            //obtenemos el id del dispositivo
            string deviceId = mPreferenceUtils.GetHardwareId();

            system.deviceID = deviceId;

            //***********************************************************************
            //<deviceRequest><system><deviceTimeStamp>
            //***********************************************************************

            system.deviceTimeStamp = DateTime.Now.ToString();


            //***********************************************************************
            //<deviceRequest><system><latitude>
            //***********************************************************************
            //***********************************************************************
            //<deviceRequest><system><longitude>
            //***********************************************************************

           

            Geolocator geolocator = new Geolocator();
            try
            {
                var geoposition = await geolocator.GetGeopositionAsync();
                system.latitude = geoposition.Coordinate.Latitude;
                system.longitude = geoposition.Coordinate.Longitude;
            }
            catch (Exception e)
            {
                system.latitude = 0;
                system.longitude = 0;
            }


            //***********************************************************************
            //<deviceRequest><system><precision>
            //***********************************************************************

            system.precision = Constantes.precision;

            //***********************************************************************
            //<deviceRequest><system><trainingVersion>
            //***********************************************************************

            system.trainingVersion = Constantes.trainingVersion;

            //***********************************************************************
            //<deviceRequest><system><version>
            //***********************************************************************

            system.version = Constantes.version;
            
            //***********************************************************************
            //<deviceRequest><validation>
            //***********************************************************************
            
            Validation validation = new Validation();


            //***********************************************************************
            //<deviceRequest><validation><formDigest>
            //***********************************************************************

            //es el hash de la concatenación de clientdata + pdf completo original .. es lo mismo que <deviceRequest><advancedsignage><formValidation>

            validation.formDigest = formValidationStr;


            //***********************************************************************
            //<deviceRequest><validation><rawDigest>
            //***********************************************************************
            //es el hash de biometricsbytes (que son los datos de los puntos codificados segun la ISO)

            string rawDigestStr = EncryptionUtils.encodeSHA(biometricsBytes);

            validation.rawDigest = rawDigestStr;

            //***********************************************************************
            //<deviceRequest><validation><signatureDigest>
            //***********************************************************************

            validation.signatureDigest = advancedSignage.signValidation;



            //***********************************************************************
            //<deviceRequest><validation><id>
            //***********************************************************************

            //concatenación: [rawDigest] + [signatureDigest] + [formDigest]

            String validationId = validation.rawDigest + validation.signatureDigest + validation.formDigest;

            validation.id = validationId;

            //en el caso de Mémora no hay servicio de timeStamp y por tanto los campos van vacíos
            TimeStamp timeStamp = new TimeStamp();

            validation.timestamp = timeStamp;
           



            DeviceRequest deviceRequest = new DeviceRequest();
            deviceRequest.advancedSignage = advancedSignage;
            deviceRequest.system = system;
            deviceRequest.validation = validation;
            


            String result = ConvertToXMLString(deviceRequest);
            

                 
            return result;

            
        }

        public async Task<String> metadataXML(byte[] biometricsBytes, StorageFile imageBytes, String pdfTitle, Stream StreamPdf, String idUser)
        {


            //generamos la clave simétrica que utilizaremos para cifrar la información
            symmetricKey = EncryptionUtils.GenerateSymmetricKey();

            UserInfo mUserInfo = null;

            mUserInfo = new UserInfo();

            UserInfo clientData = new UserInfo();
            clientData.namedoc = pdfTitle;
            clientData.name = idUser;

            //generamos el json con los datos de configuración del usuario junto con el nombre del pdf
            clientData.namedoc = pdfTitle;
            String json = JsonConvert.SerializeObject(clientData);




            //***********************************************************************
            //<deviceRequest><advancedSignage><biometrics><biometricsdata>
            //***********************************************************************
            // randomKey = GenerateRandomKey(json);

            //***********************************************************************
            //<deviceRequest><advancedSignage><biometrics><decryption><notary><parameters>
            //***********************************************************************

            Parameters parametros = new Parameters();

            //en el caso concreto de Mémora, este valor es constante porque solo hay una clave pública para todo el aplicativo. en caso de ImSign, este campo debe rellenarse
            //con el IdKey que nos devuelve el servicio Validation
            parametros.keyrelationship = Constantes.keyRelationship;
            parametros.hash_algorithm = Constantes.hashAlg;
            parametros.keyEncryptionAlgorithm = Constantes.keyEcryptionAlg;
            parametros.keyEncryption = Constantes.keyEncryption;

           

            CryptographicKey publicKey = await EncryptionUtils.getCertificateKeys();



           

            //<advancedSignage><biometrics><decryption><notary><parameters><key> es la clave simétrica cifrada con la clave pública

           parametros.key = EncryptionUtils.cifradoClave2(publicKey, EncryptionUtils.keyMaterial).Result;



            parametros.blockEncryption = Constantes.blockEncryption;
            parametros.blockEncryptionAlgorithm = Constantes.blockEncryptionAlg;


            //***********************************************************************
            //<deviceRequest><advancedSignage><biometrics><decryption><notary><protocol>
            //***********************************************************************
            Protocol protocol = new Protocol();
            protocol.city = Constantes.city;
            protocol.country = Constantes.country;
            protocol.date = Constantes.date;
            protocol.number = Constantes.number;
            protocol.name = Constantes.name;

            Notary notary = new Notary();
            notary.parameters = parametros;
            notary.protocol = protocol;


            //***********************************************************************
            //<deviceRequest><advancedSignage><biometrics><imageData>
            //***********************************************************************
            ImageData imageData = new ImageData();



            //data --> imagen de la firma sin cifrar y codificada en base64
            string data = await StorageFileToBase64(imageBytes);
            imageData.data = Encoding.UTF8.GetBytes(data);


            //es el hash de la etiqueta [imageData][data] anterior convertido en hexadecimal
            imageData.digest = EncryptionUtils.GetHash(data);
            imageData.mimeType = Constantes.mimeType;


            //***********************************************************************
            //<deviceRequest><advancedSignage><biometrics>
            //***********************************************************************
            Biometrics biometrics = new Biometrics();

            //***********************************************************************
            //<deviceRequest><advancedSignage><biometrics><decryption>
            //***********************************************************************

            Decryption decryption = new Decryption();
            decryption.method = Constantes.method;
            decryption.notary = notary;

            //digestBiometrics = EncryptionUtils.encodeSHA(Encoding.UTF8.GetString(biometricsBytes, 0, biometricsBytes.Length));


            AdvancedSignage advancedSignage = new AdvancedSignage();
            advancedSignage.biometrics = biometrics;



            //***********************************************************************
            //<deviceRequest><advancedSignage><clientData>
            //***********************************************************************

            ////generamos el json con los datos de configuración del usuario junto con el nombre del pdf
            //clientData.Namedoc = pdfTitle;
            //String json = JsonConvert.SerializeObject(clientData);

            advancedSignage.clientData = json;


            ////***********************************************************************
            ////<deviceRequest><advancedSignage><biometrics><biometricsdata>
            ////***********************************************************************
            //son los datos de la firma cifrados con la clave simétrica


            biometrics.biometricsData = EncryptionUtils.encryptAES_PKCS7(biometricsBytes, symmetricKey);

            biometrics.decription = decryption;
            biometrics.imageData = imageData;


            //***********************************************************************
            //<deviceRequest><advancedSignage><formValidation>
            //***********************************************************************

            //hash de (clientData + pdf completo original)

            //byte[] pdfBytes = await ReadFile(StreamPdf); //bytes del PDF completo original
            byte[] pdfBytes = ReadFully(StreamPdf);
            byte[] userDataBytes = Encoding.UTF8.GetBytes(json); //bytes de clientData

            ////concatenamos ambos
            IEnumerable<byte> concatenado = userDataBytes.Concat(pdfBytes);

            //lo convertimos en un array de bytes
            byte[] resultado = concatenado.ToArray();

            //hacemos el hash           
            String formValidationStr = EncryptionUtils.encodeSHA(resultado);

            advancedSignage.formValidation = formValidationStr;


            //***********************************************************************
            //<deviceRequest><advancedSignage><petitionID>
            //***********************************************************************
            //es el nombre del PDF original
            advancedSignage.petitionID = pdfTitle;


            //***********************************************************************
            //<deviceRequest><advancedSignage><serialNumber>
            //***********************************************************************

            //advancedSignage.setSerialNumber(001);


            //***********************************************************************
            //<deviceRequest><advancedSignage><signValidation>
            //***********************************************************************

            //es el hash de la etiqueta [imageData][data] anterior convertido en hexadecimal
            advancedSignage.signValidation = EncryptionUtils.GetHash(data);


            //***********************************************************************
            //<deviceRequest><advancedSignage><status>
            //***********************************************************************

            advancedSignage.status = Constantes.status;

            //***********************************************************************
            //<deviceRequest><advancedSignage><user>
            //***********************************************************************
            advancedSignage.user = clientData.idUser.ToString();

            //***********************************************************************
            //<deviceRequest><advancedSignage><versionID>
            //***********************************************************************

            advancedSignage.versionID = Constantes.versionID;


            //***********************************************************************
            //<deviceRequest><system>
            //***********************************************************************

            SystemBiometrics system = new SystemBiometrics();

            //***********************************************************************
            //<deviceRequest><system><certificateRequired>
            //***********************************************************************
            system.certificateRequired = false;


            //***********************************************************************
            //<deviceRequest><system><deviceID>
            //***********************************************************************
            //obtenemos el id del dispositivo
            string deviceId = mPreferenceUtils.GetHardwareId();

            system.deviceID = deviceId;

            //***********************************************************************
            //<deviceRequest><system><deviceTimeStamp>
            //***********************************************************************

            system.deviceTimeStamp = DateTime.Now.ToString();


            //***********************************************************************
            //<deviceRequest><system><latitude>
            //***********************************************************************
            //***********************************************************************
            //<deviceRequest><system><longitude>
            //***********************************************************************



            Geolocator geolocator = new Geolocator();
            try
            {
                var geoposition = await geolocator.GetGeopositionAsync();
                system.latitude = geoposition.Coordinate.Latitude;
                system.longitude = geoposition.Coordinate.Longitude;
            }
            catch (Exception e)
            {
                system.latitude = 0;
                system.longitude = 0;
            }


            //***********************************************************************
            //<deviceRequest><system><precision>
            //***********************************************************************

            system.precision = Constantes.precision;

            //***********************************************************************
            //<deviceRequest><system><trainingVersion>
            //***********************************************************************

            system.trainingVersion = Constantes.trainingVersion;

            //***********************************************************************
            //<deviceRequest><system><version>
            //***********************************************************************

            system.version = Constantes.version;

            //***********************************************************************
            //<deviceRequest><validation>
            //***********************************************************************

            Validation validation = new Validation();


            //***********************************************************************
            //<deviceRequest><validation><formDigest>
            //***********************************************************************

            //es el hash de la concatenación de clientdata + pdf completo original .. es lo mismo que <deviceRequest><advancedsignage><formValidation>

            validation.formDigest = formValidationStr;


            //***********************************************************************
            //<deviceRequest><validation><rawDigest>
            //***********************************************************************
            //es el hash de biometricsbytes (que son los datos de los puntos codificados segun la ISO)

            string rawDigestStr = EncryptionUtils.encodeSHA(biometricsBytes);

            validation.rawDigest = rawDigestStr;

            //***********************************************************************
            //<deviceRequest><validation><signatureDigest>
            //***********************************************************************

            validation.signatureDigest = advancedSignage.signValidation;



            //***********************************************************************
            //<deviceRequest><validation><id>
            //***********************************************************************

            //concatenación: [rawDigest] + [signatureDigest] + [formDigest]

            String validationId = validation.rawDigest + validation.signatureDigest + validation.formDigest;

            validation.id = validationId;

            //en el caso de Mémora no hay servicio de timeStamp y por tanto los campos van vacíos
            TimeStamp timeStamp = new TimeStamp();

            validation.timestamp = timeStamp;




            DeviceRequest deviceRequest = new DeviceRequest();
            deviceRequest.advancedSignage = advancedSignage;
            deviceRequest.system = system;
            deviceRequest.validation = validation;



            String result = ConvertToXMLString(deviceRequest);


            StreamPdf.Dispose();

            return result;


        }

        private String GenerateRandomKey(String datosUsuario) {

            String ticket1 = EncryptionUtils.generateTicket1();
            String ticket2 = EncryptionUtils.generateTicket2(ticket1, datosUsuario);

            return ticket2;
         }

        

        private async Task<string> StorageFileToBase64(StorageFile file)
        {
            string Base64String = "";

            if (file != null)
            {
                IRandomAccessStream fileStream = await file.OpenAsync(FileAccessMode.Read);
                var reader = new DataReader(fileStream.GetInputStreamAt(0));
                await reader.LoadAsync((uint)fileStream.Size);
                byte[] byteArray = new byte[fileStream.Size];
                reader.ReadBytes(byteArray);
                Base64String = Convert.ToBase64String(byteArray);
            }

            return Base64String;
        }


        public static async Task<byte[]> ReadFile(StorageFile file)
        {
            byte[] fileBytes = null;
            using (IRandomAccessStreamWithContentType stream = await file.OpenReadAsync())
            {
                fileBytes = new byte[stream.Size];
                using (DataReader reader = new DataReader(stream))
                {
                    await reader.LoadAsync((uint)stream.Size);
                    reader.ReadBytes(fileBytes);
                }
            }

            return fileBytes;
        }
        private async Task<byte[]> ReadFile(Stream Streamfile)
        {
            byte[] fileBytes = null;

            try
            {
                fileBytes = new byte[Streamfile.Length];
                using (DataReader reader = new DataReader(Streamfile as IInputStream))
                {
                    //await reader.LoadAsync((uint)Streamfile.Length);
                    reader.ReadBytes(fileBytes);
                }
            }
            catch (Exception e)
            {

            }
            //using (IRandomAccessStreamWithContentType stream = await file.OpenReadAsync())
            //{
           
            //}

            return fileBytes;
        }


        //public static byte[] ReadFully(Stream input)
        //{
        //    byte[] buffer = new byte[16 * 1024];
        //    using (MemoryStream ms = new MemoryStream())
        //    {
        //        int read;
        //        while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
        //        {
        //            ms.Write(buffer, 0, read);
        //        }
        //        return ms.ToArray();
        //    }
        //}

        public static byte[] ReadFully(Stream mystream)
        {
            // The mystream.length is still full here.
            byte[] buffer = null;
            using (BinaryReader br = new BinaryReader(mystream))
            {
                mystream.Position = 0;
                buffer = br.ReadBytes(Convert.ToInt32(mystream.Length));
            }
            // But imageData.length is 0
            return buffer;
        }



        public String ConvertToXMLString(DeviceRequest obj)
        {
            string partialResult;
            string result = "<deviceRequest><advancedSignage>";


            using (var memoryStream = new MemoryStream())
            {
                using (TextWriter streamWriter = new StreamWriter(memoryStream))
                {
                    var xmlSerializer = new XmlSerializer(typeof(DeviceRequest));
                    xmlSerializer.Serialize(streamWriter, obj);

                    partialResult = Encoding.UTF8.GetString(memoryStream.ToArray(), 0, (int)memoryStream.Length);
                 
                }
            }


            string[] separator = {"<advancedSignage>"};
            StringSplitOptions options = new StringSplitOptions();
            string[] members = partialResult.Split(separator, options);

            result += members[1];

            result = result.Replace("\n", string.Empty).Replace("\r", string.Empty);
            result = result.Replace("DeviceRequest", "deviceRequest");
            
            return result;
        }



       

        





    }
}
