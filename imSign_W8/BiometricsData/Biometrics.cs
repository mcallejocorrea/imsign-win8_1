﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace imSign_W8.BiometricsData
{
    public class Biometrics
    {
        private String BiometricsData;
        private Decryption Decription;
        private ImageData ImageData;


        public String biometricsData
        {
            get
            {
                return this.BiometricsData;
            }
            set
            {
                this.BiometricsData = value;
            }
        }

        public Decryption decription
        {
            get 
            {
                return this.Decription; 
            }
            set
            {
                this.Decription = value;
            }
        }

        public ImageData imageData
        {
            get
            {
                return this.ImageData;
            }
            set
            {
                this.ImageData = value;
            }
        }


    }
}
