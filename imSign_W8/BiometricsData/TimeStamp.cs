﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace imSign_W8.BiometricsData
{
    public class TimeStamp
    {
        private String GenTime;
        private String MessageImprint;
        private String Policy;
        private String Serial;
        private Status Status;
        private String StatusCode;
        private String StatusString;
        private String TimeStampToken;


        public String genTime
        {
            get
            {
                return this.GenTime;
            }
            set
            {
                this.GenTime = value;
            }
        }
        public String messageImprint
        {
            get
            {
                return this.MessageImprint;
            }
            set
            {
                this.MessageImprint = value;
            }
        }

        public String policy
        {
            get
            {
                return this.Policy;
            }
            set
            {
                this.Policy = value;
            }
        }

        public String serial
        {
            get
            {
                return this.Serial;
            }
            set
            {
                this.Serial = value;
            }
        }

        public Status status
        {
            get
            {
                return this.Status;
            }
            set
            {
                this.Status = value;
            }
        }
        public String statusCode
        {
            get
            {
                return this.StatusCode;
            }
            set
            {
                this.StatusCode = value;
            }
        }

        public String statusString
        {
            get
            {
                return this.StatusString;
            }
            set
            {
                this.StatusString = value;
            }
        }

        public String timeStampToken
        {
            get
            {
                return this.TimeStampToken;
            }
            set
            {
                this.TimeStampToken = value;
            }
        }

        public TimeStamp()
        {
            genTime = string.Empty;
            messageImprint = string.Empty;
            policy = string.Empty;
            serial = string.Empty;
            status = new Status();
            status.code = string.Empty;
            status.message = string.Empty;
            statusCode = string.Empty;
            statusString = string.Empty;
            timeStampToken = string.Empty;


        }
    }
}
