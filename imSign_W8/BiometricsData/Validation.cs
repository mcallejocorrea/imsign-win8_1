﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace imSign_W8.BiometricsData
{
    public class Validation
    {
        private String FormDigest;
        private String Id;
        private UserInfo UserInfo;
        private String RawDigest;
        private String SignatureDigest;
        private TimeStamp Timestamp;


        public String formDigest
        {
            get
            {
                return this.FormDigest;
            }
            set
            {
                this.FormDigest = value;
            }
        }

        public String id
        {
            get
            {
                return this.Id;
            }
            set
            {
                this.Id = value;
            }
        }

        public UserInfo userInfo
        {
            get
            {
                return this.UserInfo;
            }
            set
            {
                this.UserInfo = value;
            }
        }
        public String rawDigest
        {
            get
            {
                return this.RawDigest;
            }
            set
            {
                this.RawDigest = value;
            }
        }
        public String signatureDigest
        {
            get
            {
                return this.SignatureDigest;
            }
            set
            {
                this.SignatureDigest = value;
            }
        }
        public TimeStamp timestamp
        {
            get
            {
                return this.Timestamp;
            }
            set
            {
                this.Timestamp = value;
            }
        }
    }
}
