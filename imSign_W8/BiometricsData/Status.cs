﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace imSign_W8.BiometricsData
{
    public class Status
    {
        private String Code;
        private String Message;

        public String code
        {
            get
            {
                return this.Code;
            }
            set
            {
                this.Code = value;
            }
        }

        public String message
        {
            get
            {
                return this.Message;
            }
            set
            {
                this.Message = value;
            }
        }

    }
}
