﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace imSign_W8.BiometricsData
{
    public class Constantes
    {


        public const string IMSIgn = "ImSign";


        //advancedSignage.biometrics.decryption
        public const string method = "notary";

        //advancedSignage.biometrics.decryption.notary.parameters
        public const string keyRelationship = "0001";
        public const string hashAlg = "SHA2";
        public const string keyEcryptionAlg = "RSA";
        public const string keyEncryption = "Public key";
        public const string blockEncryptionAlg = "AES128";
        public const string blockEncryption = "Symmetric key";

        //advancedSignage.biometrics.decryption.notary.protocol
        public const string city = "Madrid";
        public const string country = "ES (ISO 3166)";
        public const string date = "2012-11-06T08:54:17 + 01:00";
        public const string name = "D. Rafael Bonardell Lenzano";
        public const string number = "Protocol Number 12345";


        //advancedSignage.biometrics.imageData
        public const string mimeType = "image/png";

        //deviceRequest.advancedSignage
        public const string serialNumber = "001";
        public const string status = "COMPLETE";
        public const string versionID = "1.0";

        //deviceRequest.system

        public const string certificateReq = "false";
        public const string deviceID = "83";
        public const string precision = "65";
        public const string trainingVersion = "1";
        public const string version = "1.0";

        //deviceRequest.Validation.timestamp

        public const string genTime = "Tue Dec 17 17:20:36 UTC 2013";
        public const string messageImprint = "B82F08490205F5C96CB9C5B5583D4AE6295BE92E04460802AFE2FB7D4616BD45";
        public const string policy = "1.3.6.1.4.1.19126.2.2.3.1";
        public const string serial = "6787023004484533195";
        public const string statusCode = "0";
        public const string statusString = "Operation Okay";
        public const string timestampToken = "MIAGCSqGSIb3DQEHAqCAMIIDTwIBAzELMAkGBSsOAwIaBQAwcgYLKoZIhvcNAQkQAQSgYwRhMF8CAQEGDCsGAQQBgZU2AgIDATAxMA0GCWCGSAFlAwQCAQUABCC4LwhJAgX1yWy5xbVYPUrmKVvpLgRGCAKv4vt9Rha9RQIIXjBZdqC0e8sYDzIwMTMxMjE3MTcyMDM2WjGCAscwggLDAgEBMIGQMIGDMQswCQYDVQQGEwJFUzE3MDUGA1UEChMuRXVyb3BlYW4gQWdlbmN5IG9mIERpZ2l0YWwgVHJ1c3QgLSBFU0I4NTYyNjI0MDEgMB4GA1UECxMXaHR0cDovL3d3dy5lYWR0cnVzdC5uZXQxGTAXBgNVBAMTEEVBRFRydXN0IFJvb3QgQ0ECCACqAAAAAAAIMAkGBSsOAwIaBQCggYwwGgYJKoZIhvcNAQkDMQ0GCyqGSIb3DQEJEAEEMBwGCSqGSIb3DQEJBTEPFw0xMzEyMTcxNzIwMzZaMCMGCSqGSIb3DQEJBDEWBBThZwizXunZPD6Drl";


        //deviceRequest.Validation.timestamp.status
        public const string code = "10000000";
        public const string message = "[10000000] Petition successful";


        
       
    }
}
