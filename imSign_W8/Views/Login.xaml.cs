﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Shapes;   
using Windows.Media;
using Windows.UI;
using Windows.Storage;
using Windows.Storage.Streams;
using imSign_W8.Utils;
using imSign_W8.Entidades;
using imSign_W8.Servicios;


// La plantilla de elemento Página en blanco está documentada en http://go.microsoft.com/fwlink/?LinkId=234238

namespace imSign_W8.Views
{
    /// <summary>
    /// Página vacía que se puede usar de forma independiente o a la que se puede navegar dentro de un objeto Frame.
    /// </summary>
    public sealed partial class Login : Page
    {
        //por defecto, el tipo de usuario es gratuito
        Utils.EncryptionUtils.tipoUsuario tipoUsuario = Utils.EncryptionUtils.tipoUsuario.gratis;
        enum tipoAccion  {registro, login};

        tipoAccion accion = tipoAccion.registro;
        public UserData datosUsuario = new UserData();
        Windows.Storage.ApplicationDataContainer localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
        Color colorDefecto =  Color.FromArgb(0xff, 0x00, 0xb0, 0xca);
        Color colorUsuario;

        public Login()
        {
            this.InitializeComponent();




            //si el usuario ha configurado un color y/o icono personalizado, lo cargamos
            if (localSettings.Values["colorUsuario"] != null)
            {
                colorUsuario = WinRTXamlToolkit.Imaging.ColorExtensions.FromString(localSettings.Values["colorUsuario"].ToString());
                
                datosUsuario.colorUsuario = new SolidColorBrush(colorUsuario);
                this.aceptarBtn.Background = datosUsuario.colorUsuario;
                this.cancelarBtn.BorderBrush = datosUsuario.colorUsuario;
                this.aceptarBtnRegistro.Background = datosUsuario.colorUsuario;
                this.cancelarBtnRegistro.BorderBrush = datosUsuario.colorUsuario;
                this.elipseRegistro.Fill = new SolidColorBrush(colorUsuario);
                this.btnRegistrar.Foreground = new SolidColorBrush(colorUsuario);
            }
            else
                colorUsuario = colorDefecto;


           
            
        }

        private async void btnAceptarLogin_Click(object sender, RoutedEventArgs e)
        {
            String popupMessage = String.Empty;

            ImSignResponse loginResponse = null;


            //comprobamos el tipo de acción seleccionada para desencadenar unas acciones u otras
            if(accion == tipoAccion.registro)
            {
                //mostramos el formulario de registro de usuario con todos los campos vacíos
                limpiarFormularioRegistro();

                this.menuConfiguracion.Visibility = Visibility.Visible;

            }
            else if (accion == tipoAccion.login)
            {
                Boolean resLogin = false;
                var loader = new Windows.ApplicationModel.Resources.ResourceLoader();





                //si no están rellenos el usuario/contraseña
                if((String.IsNullOrEmpty(this.tbUser.Text)) || (String.IsNullOrEmpty(this.passLogin.Password.ToString())))
                {
                    popupMessage = loader.GetString("loginDatosObligatorios");

                }
                else
                {
                    // TODO: llamamos al servicio de login de usuario

                    Servicios.WebServices servicios = new Servicios.WebServices();

                    loginResponse = await servicios.loginWebService(this.tbUser.Text, this.passLogin.Password.ToString());

                    if (loginResponse != null)
                    {


                        //guardamos en los datos del usuario el valor de isPremium y el idUser
                        datosUsuario.premium = loginResponse.UserInfo.premium;
                        datosUsuario.idUser = loginResponse.UserInfo.idUser;

                        localSettings.Values["isPremium"] = datosUsuario.premium;
                        localSettings.Values["idUser"] = datosUsuario.idUser;

                        //si el login es OK, navegamos a la siguiente página
                        this.Frame.Navigate(typeof(SecondaryPage), tipoUsuario);
                    }
                    //mostramos mensaje de error de login
                    else
                    {
                        popupMessage = loader.GetString("loginDatosError");
                    }
                }
               

               if( ! String.IsNullOrEmpty(popupMessage))
               {
                   this.tbPopUp.Text = popupMessage;
                   this.PopUp.Visibility = Windows.UI.Xaml.Visibility.Visible;
               }
            }



            

        }

        private void btnCancelarLogin_Click(object sender, RoutedEventArgs e)
        {
            //cerramos el formulario de login

            this.seeAlsoPanel.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }

        private void btnRegistrar_Click(object sender, RoutedEventArgs e)
        {


            //el tipo de accion es el registro
            accion = tipoAccion.registro;

            limpiarFormularioRegistro();

            //pintamos del color seleccionado el texto del botón "Registrar" y cambiamos la imagen del radio por la coloreada
            //this.btnRegistrar.Foreground = new SolidColorBrush(Color.FromArgb(100, 0, 176, 202));
            //this.checkRegistrar.Source = new BitmapImage(new Uri("ms-appx:///Assets/radio_on.png"));
            this.btnRegistrar.Foreground = new SolidColorBrush(colorUsuario);
            this.elipseRegistro.Fill = new SolidColorBrush(colorUsuario);
            this.checkRegistrar.Source = new BitmapImage(new Uri("ms-appx:///Assets/radio_on_white.png"));
            

            //pintamos del color desactivado el texto del botón "Identifíquese" y cambiamos la imagen del radio por la inactiva
            //this.btnLogin.Foreground = new SolidColorBrush(Color.FromArgb(100, 184, 177, 177));
            //this.checkLogin.Source = new BitmapImage(new Uri("ms-appx:///Assets/radio_off.png"));


            this.btnLogin.Foreground = new SolidColorBrush(Colors.LightGray);
            this.elipseLogin.Fill = new SolidColorBrush(Colors.Transparent);
            this.checkLogin.Source = new BitmapImage(new Uri("ms-appx:///Assets/radio_off_white.png"));


            //desactivamos el textbox para la password de la versión premium
            this.panelPassword.Visibility = Visibility.Collapsed;
            this.passLogin.Visibility = Visibility.Collapsed;
            this.tbUser.Visibility = Visibility.Collapsed;
        }

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {

            Windows.Storage.ApplicationDataContainer localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
            SolidColorBrush colorActivo = new SolidColorBrush(colorUsuario);

            //el tipo de accion es el login
            accion = tipoAccion.login;

            //activamos los botones
            this.cancelarBtn.Visibility = Visibility.Visible;
            this.aceptarBtn.Visibility = Visibility.Visible;

            //pintamos del color seleccionado el texto del botón "Trial Version" y cambiamos la imagen del radio por la coloreada
            //this.btnLogin.Foreground = new SolidColorBrush(Color.FromArgb(100, 0, 176, 202));
            //this.checkLogin.Source = new BitmapImage(new Uri("ms-appx:///Assets/radio_on.png"));
            this.btnLogin.Foreground = colorActivo;
            this.elipseLogin.Fill = colorActivo;
            this.checkLogin.Source = new BitmapImage(new Uri("ms-appx:///Assets/radio_on_white.png"));

            //activamos el textbox para la password y el usuario del registro
            this.panelPassword.Visibility = Visibility.Visible;
            this.passLogin.Visibility = Visibility.Visible;

            //si hay un nombre de usuario almacenado en los settings del dispositivo, lo cargamos en el textbox


            if (localSettings.Values["username"] != null)               
            {
                this.tbUser.Text = localSettings.Values["username"].ToString();
                this.tbUser.IsEnabled = false;
                
            }

            this.tbUser.Visibility = Visibility.Visible;


            //pintamos del color estandar el texto del botón "Premium Version" y cambiamos la imagen del radio por la inactiva
            //this.btnRegistrar.Foreground = new SolidColorBrush(Color.FromArgb(100, 184, 177, 177));
            //this.checkRegistrar.Source = new BitmapImage(new Uri("ms-appx:///Assets/radio_off.png"));

            this.btnRegistrar.Foreground = new SolidColorBrush(Colors.LightGray);
            this.elipseRegistro.Fill = new SolidColorBrush(Colors.Transparent);
            
            this.checkRegistrar.Source = new BitmapImage(new Uri("ms-appx:///Assets/radio_off_white.png"));




        }

        private void btnCancelarConfiguracion_click(object sender, RoutedEventArgs e)
        {
            //cerramos el formulario
            menuConfiguracion.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }

        private async void btnAceptarConfiguracion_click(object sender, RoutedEventArgs e)
        {
            Windows.Storage.ApplicationDataContainer localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
            String popupMessage = string.Empty;
            var loader = new Windows.ApplicationModel.Resources.ResourceLoader();

            //validación de datos
            //if( datosValidos())
            if (Validaciones.datosValidos(this.tbUserRegistro.Text, this.passwordBoxRegistro.Password, this.tbDNI.Text, this.tbEmail.Text, this.tbMovil.Text, out popupMessage))
            {
                //guardamos en las preferencias de la aplicación la información
                localSettings.Values["name"] = this.tbNombre.Text.ToString();
                localSettings.Values["surname"] = this.tbApellidos.Text.ToString();
                localSettings.Values["email"] = this.tbEmail.Text.ToString();
                localSettings.Values["tlf"] = this.tbMovil.Text.ToString();
                localSettings.Values["businessName"] = this.tbCompañia.Text.ToString();
                localSettings.Values["idCode"] = this.tbDNI.Text.ToString();
                localSettings.Values["username"] = this.tbUserRegistro.Text.ToString();
                localSettings.Values["password"] = this.passwordBoxRegistro.Password;
                localSettings.Values["isPremium"] = false;
                localSettings.Values["idUser"] = -1;

                //TODO llamamos al servicio de registro
                //1. creamos una entidad con los datos del usuario

                datosUsuario.name = this.tbNombre.Text.ToString();
                datosUsuario.surName = this.tbApellidos.Text.ToString();
                datosUsuario.email = this.tbEmail.Text.ToString();
                datosUsuario.idCode = this.tbDNI.Text.ToString();
                datosUsuario.tlf = this.tbMovil.Text.ToString();
                datosUsuario.businessName = this.tbCompañia.Text.ToString();
                datosUsuario.username = this.tbUserRegistro.Text.ToString();
                datosUsuario.password = this.passwordBoxRegistro.Password.ToString();
                datosUsuario.idUser = -1;
                datosUsuario.nKey = 1;
                datosUsuario.premium = false;
                //datosUsuario.premium = true;

                //hacemos invisible el stack panel
                menuConfiguracion.Visibility = Windows.UI.Xaml.Visibility.Collapsed;


                UserDataServices datosUsuarioServicios = new UserDataServices(datosUsuario);




                popupMessage = await ConfiguracionUsuario.RegistrarUsuario(datosUsuarioServicios);


                

                this.tbPopUp.Text = popupMessage;
                this.PopUp.Visibility = Windows.UI.Xaml.Visibility.Visible;
            }

            else
            {
                this.tbPopUp.Text = popupMessage;
                this.PopUp.Visibility = Windows.UI.Xaml.Visibility.Visible;
            }
        }

        private void btnCerrar_Click(object sender, RoutedEventArgs e)
        {
            this.PopUp.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }


        private void limpiarFormularioRegistro()
        {
            this.tbNombre.Text = String.Empty;
            this.tbApellidos.Text = String.Empty;
            this.tbDNI.Text = String.Empty;
            this.tbEmail.Text = String.Empty;
            this.tbMovil.Text = String.Empty;
            this.tbCompañia.Text = String.Empty;
            this.tbUserRegistro.Text = String.Empty;
            this.passwordBoxRegistro.Password = String.Empty;
        }

        



       

    }
}
