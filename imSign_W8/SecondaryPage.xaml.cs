﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Shapes;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.Storage.Streams;
using SDKTemplate;
using System.Threading.Tasks;
using Windows.UI.Popups;
using iTextSharp.text.pdf;
using iTextSharp.text.xml.xmp;
using Windows.Graphics.Imaging;
using System.Text.RegularExpressions;
using System.Collections;
using Windows.Security.Cryptography;
using Windows.Security.Cryptography.Core;
using System.Xml.Serialization;
using Windows.UI.Input;
using Windows.Devices.Geolocation;
using imSign_W8.Servicios;
using imSign_W8.ISOBiometrics;
using imSign_W8.BiometricsData;
using imSign_W8.Utils;
using imSign_W8.Entidades;
using imSign_W8.clases;
using Windows.Security.Cryptography.Certificates;
using Windows.ApplicationModel.DataTransfer;
using Windows.Networking.Connectivity;
using System.Reflection;
using System.ComponentModel;
using Windows;
using Windows.Media;
using System.Text;
using System.Net;
using Cubisoft.Winrt.Ftp;
using Windows.Networking.BackgroundTransfer;


// La plantilla de elemento Página en blanco está documentada en http://go.microsoft.com/fwlink/?LinkId=234238

namespace imSign_W8
{
    /// <summary>
    /// Página vacía que se puede usar de forma independiente o a la que se puede navegar dentro de un objeto Frame.
    /// </summary>
    public sealed partial class SecondaryPage : Page, INotifyPropertyChanged
    {
        private gestionDocumentosPDF gestionPDF;
        private StorageFolder carpetaActual;
        private StorageFile documentoFirma;
        private StorageFile imagenFirma;
        private String nombrePdf;
        private String nombreImagenFirma;
        Point _previousPosition = default(Point);
        SolidColorBrush _stroke = new SolidColorBrush(Colors.Black);
        bool _pressed = false;
        public UserData datosUsuario = new UserData();
        private StorageFile imagenUsuario = null;
        private Boolean isPendingDocument = false;

        //en _allPoints se almacena la información de todos los puntos de la firma. Cada uno de sus componentes de tipo List<Punto> se ha recogido en listaPuntos
        List<List<Punto>> _allPoints = new List<List<Punto>>();

        //en listaPuntos guardamos la información de los puntos de un trazo
        List<Punto> listaPuntos = null;

        private double pdfScrollOffset = 0.0;
        private int pdfPageNumber = 0;      
        private StorageFile pdfOriginal;
        private EncryptionUtils.tipoUsuario tipoUsuario;


        //private Brush colorSeleccionado;
        private Brush colorDefecto;
        private BitmapImage iconoSeleccionado;
        private BitmapImage iconoDefecto;


        private List<PendingFileInfo> docsPendientes = null;

        Windows.ApplicationModel.Resources.ResourceLoader loader = new Windows.ApplicationModel.Resources.ResourceLoader();
        Windows.Storage.ApplicationDataContainer localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
        String popupMessage = string.Empty;


        private GetDocumentInfo pendingDocInfo = new GetDocumentInfo();

        #region ViewModelProperty: Background
        private Brush _colorSeleccionado;
        public Brush ColorSeleccionado
        {
            get
            {
                return _colorSeleccionado;
            }

            set
            {
                _colorSeleccionado = value;
                OnPropertyChanged("ColorSeleccionado");
            }
        }
        #endregion



        public  SecondaryPage() 
        {
            this.InitializeComponent();

            DataContext = this;

        

            pdfScrollOffset = 0.0;
            pdfPageNumber = flipView.SelectedIndex;


            //si hay datos de configuración del usuario, los recogemos
            datosUsuario = obtenerDatosRegistro().Result;
            
            
            loadcolors();

            colorDefecto = new SolidColorBrush(Color.FromArgb(0xff, 0x00, 0xb0, 0xca));
            iconoDefecto = new BitmapImage(new Uri("ms-appx:///Assets/imSign_logo_white.png", UriKind.Absolute));
            isPendingDocument = false;


            if (datosUsuario.colorUsuario != null)
            {
                ColorSeleccionado = datosUsuario.colorUsuario;
            }
            else
                ColorSeleccionado = colorDefecto;

            if (datosUsuario.iconoUsuario != null)
                iconoSeleccionado = datosUsuario.iconoUsuario;
            else
                iconoSeleccionado = iconoDefecto;


            this.barraMenu.Background = ColorSeleccionado;

            this.iconoUsuarioBarra.Source = iconoSeleccionado;
           
           
        }
        

        #region EVENTOS BOTONES
        private async void EnviarPorServicio_click(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("-----------------------------------");
            System.Diagnostics.Debug.WriteLine("EnviarPorServicio_Click");
            System.Diagnostics.Debug.WriteLine("-----------------------------------");

            if(this.pendingDocInfo == null)
            {
                //no se ha cargado un documento pendiente, así que no se puede enviar por servicio
            }

            else
            {
                //generamos una estructura de datos con toda la información del documento
                DocInfo docInfo = new DocInfo();

                docInfo.fileName = this.pendingDocInfo.fileName;
                docInfo.content_type = this.pendingDocInfo.content_type;
                docInfo.date = System.DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss");
                docInfo.user = datosUsuario.username;
                docInfo.documentUID = this.pendingDocInfo.idDoc;


                //leemos el storageFile para obtener su contenido y longitud

                IRandomAccessStream fileStream = await documentoFirma.OpenAsync(FileAccessMode.Read);
                //IRandomAccessStream fileStream = await pdfOriginal.OpenAsync(FileAccessMode.Read);
                var reader = new DataReader(fileStream.GetInputStreamAt(0));
                await reader.LoadAsync((uint)fileStream.Size);
                byte[] byteArray = new byte[fileStream.Size];
                reader.ReadBytes(byteArray);



                docInfo.content = Convert.ToBase64String(byteArray);

                docInfo.length = docInfo.content.Length;

               // docInfo.md5 = EncryptionUtils.encodeMD5(docInfo.content).ToUpper();

                docInfo.md5 = EncryptionUtils.encodeMD5(byteArray).ToUpper();

                String uidStr = docInfo.documentUID + docInfo.md5 + docInfo.user + docInfo.date;


                docInfo.uid = EncryptionUtils.encodeSHA512(uidStr);
                System.Diagnostics.Debug.WriteLine("uidStr: " + uidStr);

                //String hashString = docInfo.uid + '#' + docInfo.content_type + '#' + EncryptionUtils.encodeMD5(docInfo.content).ToUpper() + '#' + docInfo.fileName + '#' + docInfo.date + '#' + docInfo.length.ToString()+ '#' + docInfo.user;
                String hashString = docInfo.uid + '#' + docInfo.content_type + '#' + EncryptionUtils.encodeMD5(byteArray).ToUpper() + '#' + docInfo.fileName + '#' + docInfo.date + '#' + docInfo.length.ToString() + '#' + docInfo.user;
                System.Diagnostics.Debug.WriteLine("hashString: " + hashString);

                docInfo.hash = EncryptionUtils.encodeMD5(hashString).ToUpper();




                SendFileResponse resSendFile = null;
                //llamamos al WS de envío de documentos pendientes firmados para el usuario
                Servicios.WebServices servicios = new Servicios.WebServices();

                resSendFile = await servicios.sendFileWebService(datosUsuario.username, datosUsuario.password, docInfo);
            }

           



        }
        private void BorrarFirmaCanvas_click(object sender, RoutedEventArgs e)
        {
            BorrarFirma();
        }
        private async void MenuConfiguracion_click(object sender, RoutedEventArgs e)
        {

            this.cancelarBtn.BorderBrush = ColorSeleccionado;
            //si hay datos de configuración del usuario, los recogemos
            datosUsuario = await obtenerDatosRegistro();

            if (datosUsuario != null)
                cargarDatosUsuario(datosUsuario);
           

            //hacemos visible el stack panel
            menuConfiguracion.Visibility = Windows.UI.Xaml.Visibility.Visible;
        }
        private void MenuPersonalizacion_click(object sender, RoutedEventArgs e)
        {

            //si hay guardadas preferencias de usuario de color e icono, las cargamos

            if (datosUsuario.iconoUsuario != null)
            {
                this.iconoUsuario.Source = datosUsuario.iconoUsuario;
            }
          


            //hacemos visible el menu
            this.menuPersonalizacion.Visibility = Windows.UI.Xaml.Visibility.Visible;



            //si se ha personalizado el color o el icono, mostramos el botón de restaurar
            if ((ColorSeleccionado != colorDefecto) || (iconoSeleccionado != iconoDefecto))
            {
                restaurarPersonalizacionBtn.Visibility = Windows.UI.Xaml.Visibility.Visible;
            }
            else
                restaurarPersonalizacionBtn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;

            
        }
        private async void BorrarFirmaDocumento_click(object sender, RoutedEventArgs e)
        {
            // Eliminamos la firma del Canvas
            BorrarFirma();
            pdfPageNumber = 1;
            // Recargamos el documento
            await LoadFile(pdfOriginal);
            documentoFirma = pdfOriginal;
            // Volvemos a activar el Canvas
            this.ComponenteFirma.Visibility = Visibility.Visible;
            this.canvasFirma.Opacity = 100;

            // Habilitamos el menú de firmas.
            this.menuFirmar.Visibility = Visibility.Visible;
            this.borrarFirmaMenuItem.IsEnabled = true;
            this.firmaActualMenuItem.IsEnabled = true;
            this.firmaTodasMenuItem.IsEnabled = true;

            // TODO Eliminar documentos temporales generados
            //EliminarTemporales();
            //var del = await deleteTempLocalFiles();
            this.menuEnviar.Visibility = Visibility.Collapsed;
            menuFirmaVisibleOpcDesactivadas();
        }
        private void btnCancelarConfiguracion_click(object sender, RoutedEventArgs e)
        {
            //cerramos el formulario
            menuConfiguracion.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }
        private async void btnAceptarConfiguracion_click(object sender, RoutedEventArgs e)
        {
            
            //validación de datos
             if (Validaciones.datosValidos(this.tbUser.Text, this.passwordBox.Password, this.tbDNI.Text, this.tbEmail.Text, this.tbMovil.Text, out popupMessage))
            {
               //guardamos en las preferencias de la aplicación la información
               localSettings.Values["name"] = this.tbNombre.Text.ToString();
               localSettings.Values["surname"] = this.tbApellidos.Text.ToString();
               localSettings.Values["email"] = this.tbEmail.Text.ToString();
               localSettings.Values["tlf"] = this.tbMovil.Text.ToString();
               localSettings.Values["businessName"] = this.tbCompañia.Text.ToString();
               localSettings.Values["idCode"] = this.tbDNI.Text.ToString();
               localSettings.Values["username"] = this.tbUser.Text.ToString();
               localSettings.Values["password"] = this.passwordBox.Password;

               //1. creamos una entidad con los datos del usuario

               datosUsuario.name = this.tbNombre.Text.ToString();
               datosUsuario.surName = this.tbApellidos.Text.ToString();
               datosUsuario.email = this.tbEmail.Text.ToString();
               datosUsuario.idCode = this.tbDNI.Text.ToString();
               datosUsuario.tlf = this.tbMovil.Text.ToString();
               datosUsuario.businessName = this.tbCompañia.Text.ToString();
               datosUsuario.username = this.tbUser.Text.ToString();
               datosUsuario.password = this.passwordBox.Password.ToString();
               datosUsuario.idUser = -1;
               datosUsuario.nKey = 1;
               datosUsuario.premium = true;

               //hacemos invisible el stack panel
               menuConfiguracion.Visibility = Windows.UI.Xaml.Visibility.Collapsed;



               UserDataServices datosUsuarioServicios = new UserDataServices(datosUsuario);
               popupMessage = await ConfiguracionUsuario.RegistrarUsuario(datosUsuarioServicios);


               this.tbPopUp.Text = popupMessage;
               this.PopUp.Visibility = Windows.UI.Xaml.Visibility.Visible;
           }

            else
            {
                this.tbPopUp.Text = popupMessage;
                this.PopUp.Visibility = Windows.UI.Xaml.Visibility.Visible;
            }

           
        }

      


        private async void CargarDocumentoLocal_click(object sender, RoutedEventArgs e)
        {

            var del = await deleteTempLocalFiles();


            isPendingDocument = false;
            //para cargar un documento local, abrimos un cuadro de diálogo para seleccionar el documento
            FileOpenPicker openPicker = new FileOpenPicker();
            openPicker.ViewMode = PickerViewMode.Thumbnail;
            openPicker.ViewMode = PickerViewMode.List;
            openPicker.SuggestedStartLocation = PickerLocationId.DocumentsLibrary;
            openPicker.FileTypeFilter.Add(".pdf");
           // openPicker.FileTypeFilter.Add(".html");
            documentoFirma = await openPicker.PickSingleFileAsync();

            if (documentoFirma != null)
            {
                if (documentoFirma.Name.Contains(".pdf"))
                {
                    pdfOriginal = documentoFirma;
                    pdfPageNumber = 1;
                    var carga = await LoadFile(documentoFirma);
                }
                else
                {
                    carpetaActual = await documentoFirma.GetParentAsync();
                    string resultadoGenerar = await generar_pdf_desde_html(documentoFirma.Name);
                    documentoFirma = await ApplicationData.Current.LocalFolder.GetFileAsync(resultadoGenerar);
                    pdfOriginal = documentoFirma;
                    pdfPageNumber = 1;
                    var carga =  LoadFile(documentoFirma);
                }

                //mostramos el menú de firma pero con todas las opciones inactivas porque aún no hay firma
                this.menuFirmar.Visibility = Visibility.Visible;
                this.borrarFirmaMenuItem.IsEnabled = false;
                this.firmaActualMenuItem.IsEnabled = false;
                this.firmaTodasMenuItem.IsEnabled = false;

                //posicionamos el componente de firma en la parte central 
                double posX = ((MainCanvas.ActualWidth / 2) - (marcoFirma.ActualWidth / 2));
                double posY = ((MainCanvas.ActualHeight / 2) - (marcoFirma.ActualHeight / 2));

                Canvas.SetLeft(ComponenteFirma, posX);
                Canvas.SetTop(ComponenteFirma, posY);
                BorrarFirma();
                this.ComponenteFirma.Visibility = Visibility.Visible;
                this.canvasFirma.Opacity = 100;
            }
            flipView.SelectedIndex = 0;
            menuFirmaVisibleOpcDesactivadas();
          
        }
        private void CargarDocumentoServicio_click(object sender, RoutedEventArgs e)
        {
            mensajePremiumVersion();
        }
        private  void CargarDocumentoWeb_click(object sender, RoutedEventArgs e)
        {
            this.menuCargarDocWeb.Visibility = Windows.UI.Xaml.Visibility.Visible;

        
            
        }
        private void VolverAInicio_click(object sender, RoutedEventArgs e)
        {
            //navegamos a la página inicial
            this.Frame.Navigate(typeof(MainPage));
        }
       
        private async void FirmarPaginaActual_click(object sender, RoutedEventArgs e)
        {
            List<PdfPageWrapper> listaPaginas = new List<PdfPageWrapper>();


            if (!estaFirmaVacia())
            {
                //activamos la barra de progreso de firma de documento
                this.border.Visibility = Windows.UI.Xaml.Visibility.Visible;

                //1. obtenemos la posición actual del componente de firma
                PdfPageWrapper pagina = (PdfPageWrapper)flipView.SelectedItem;

                listaPaginas.Add(pagina);

                var ttv = flipView.TransformToVisual(this.canvasFirma);
                Point screenCords = ttv.TransformPoint(new Point(0, 0));

                var resultado = await Firmar(listaPaginas, screenCords,tipoUsuario);

                
            }
            else
            {
                
                popupMessage = loader.GetString(" necesariaFirma");

                this.tbPopUp.Text = popupMessage;
                this.PopUp.Visibility = Windows.UI.Xaml.Visibility.Visible;
            }
        }

        private async Task<Boolean> Firmar (List<PdfPageWrapper> listaPaginas, Point coordenadasInsercion, EncryptionUtils.tipoUsuario tipoUsuario)
        {
            Boolean resultado = false;

           // var del = await deleteTempLocalFiles();


            //2. obtenemos el contenido del Canvas en formato imagen
            StorageFolder tempFolder = ApplicationData.Current.TemporaryFolder;
            StorageFile imagenFirma = await tempFolder.CreateFileAsync(string.Format("{0}_firma.png",
            DateTimeOffset.Now.ToString("MM_dd_yy_hh_mm_ss")), CreationCollisionOption.ReplaceExisting);
            nombreImagenFirma = imagenFirma.Name;

            var renderTargetBitMap = new RenderTargetBitmap();
            canvasFirma.Background = new SolidColorBrush(Colors.Transparent);
            await renderTargetBitMap.RenderAsync(canvasFirma);

            //ocultamos el control de firma
            this.ComponenteFirma.Visibility = Windows.UI.Xaml.Visibility.Collapsed;

            //conseguimos todos los píxeles del Canvas y los guardamos en encoder
            var pixelBuffer = await renderTargetBitMap.GetPixelsAsync();
            var width = renderTargetBitMap.PixelWidth;
            var height = renderTargetBitMap.PixelHeight;

            using (IRandomAccessStream stream = await imagenFirma.OpenAsync(FileAccessMode.ReadWrite))
            {
                BitmapEncoder encoder = await BitmapEncoder.CreateAsync(selectGuid(imagenFirma.FileType), stream);
                Stream pixelStream = pixelBuffer.AsStream();
                byte[] pixels = new byte[pixelStream.Length];
                await pixelStream.ReadAsync(pixels, 0, pixels.Length);

                var CurrentDpi = Windows.Graphics.Display.DisplayInformation.GetForCurrentView().LogicalDpi;

              
                encoder.SetPixelData(BitmapPixelFormat.Bgra8, BitmapAlphaMode.Straight,
                                     (uint)width,
                                     (uint)height,
                                     CurrentDpi,
                                     CurrentDpi,
                                     pixels);


                await encoder.FlushAsync();
                var decoder = await BitmapDecoder.CreateAsync(stream);
                await stream.FlushAsync();
            }


            await InsertarFirma(listaPaginas, imagenFirma, (float)coordenadasInsercion.X, (float)coordenadasInsercion.Y);

           
            canvasFirma.Background = new SolidColorBrush(Colors.White);
            canvasFirma.Opacity = 1;

            //ocultamos la barra de progreso de firma del documento
            this.border.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            //hacemos visible el menú de envío
            menuEnviarVisibleOpcActivadas();

            if (!isPendingDocument)
            {
                this.enviarDocServicioMenuItem.IsEnabled = false;
            }
            // Deshabilitamos el menú de firmas.
            menuFirmaVisibleOpcDesactivadas();




            return resultado;
        }


        private void EnviarPorMail_click(object sender, RoutedEventArgs e)
        {
            //MCC comprobamos conexión a internet
            ConnectionProfile profile =       NetworkInformation.GetInternetConnectionProfile();

            if (profile.GetNetworkConnectivityLevel() >=
                        NetworkConnectivityLevel.InternetAccess)
            {
                SharePage compartir = new SharePage();

                StorageFile PdfEnviar = documentoFirma;
                compartir.enviarPorMail(PdfEnviar);
            }
                //no hay conexión a internet, no se puede enviar el mail
            else
            {
                //mostrar mensaje

            }
          

           // EliminarTemporales();
        }
        private void ConfiguracionUsuario_click(object sender, RoutedEventArgs e)
        {
            //mostramos una pantalla con los datos de configuración del usuario
        }
        #endregion

        #region EVENTOS FIRMA
        private void inkcanvas_PointerReleased(object sender, PointerRoutedEventArgs e)
        {
            //almacenamos la información de los puntos de la firma que nos interesan
            //guardamos la información del punto 
            Punto miPunto = new Punto();

            miPunto.xPos = e.GetCurrentPoint(canvasFirma).Position.X;
            miPunto.yPos = e.GetCurrentPoint(canvasFirma).Position.Y;
            miPunto.time = System.DateTime.Now;

            //límites del canvas de firma
            double firmaTop = contenedorCanvasFirma.Height - 10;
            double firmaLeft = contenedorCanvasFirma.Width - 10;

            //si el puntero está dentro de los límites, pinta la firma
            if ((firmaLeft > e.GetCurrentPoint(canvasFirma).Position.X && e.GetCurrentPoint(canvasFirma).Position.X > 0) && (firmaTop > e.GetCurrentPoint(canvasFirma).Position.Y && e.GetCurrentPoint(canvasFirma).Position.Y > 0))
            {
                if (listaPuntos == null)
                    listaPuntos = new List<Punto>();

                miPunto.xVel = 0;
                miPunto.yVel = 0;
                miPunto.xAceleracion = 0;
                miPunto.yAceleracion = 0;

                listaPuntos.Add(miPunto);

                Ellipse el1 = new Ellipse();
                el1.Height = 4;
                el1.Width = 4;

                el1.StrokeThickness = 3;
                el1.Stroke = _stroke;

                Canvas.SetLeft(el1, e.GetCurrentPoint(canvasFirma).Position.X - el1.Width / 2);
                Canvas.SetTop(el1, e.GetCurrentPoint(canvasFirma).Position.Y - el1.Height / 2);

                canvasFirma.Children.Add(el1);
            }

            Point pt;
            pt.X = e.GetCurrentPoint(canvasFirma).Position.X;
            pt.Y = e.GetCurrentPoint(canvasFirma).Position.Y;
            _previousPosition = pt;
        }
        private void inkcanvas_ManipulationInertiaStarting(object sender, ManipulationInertiaStartingRoutedEventArgs e)
        {
            inkcanvas_ManipulationCompleted(null, null);
        }
        private void moverFirma_ManipulationStarted(object sender, ManipulationStartedRoutedEventArgs e)
        {
            canvasFirma.Opacity = 0.2;
        }
        private void inkcanvas_ManipulationDelta(object sender, ManipulationDeltaRoutedEventArgs e)
        {
            //almacenamos la información de los puntos de la firma que nos interesan
            //guardamos la información del punto 
            Punto miPunto = new Punto();
            miPunto.xPos = e.Position.X;
            miPunto.yPos = e.Position.Y;
            miPunto.time = System.DateTime.Now;
            miPunto.xVel = e.Velocities.Linear.X;
            miPunto.yVel = e.Velocities.Linear.Y;

            //límites del canvas de firma
            double firmaTop = contenedorCanvasFirma.Height - 10;
            double firmaLeft = contenedorCanvasFirma.Width - 10;


            if (_pressed)
            {
                //si el puntero está dentro de los límites, pinta la firma
                if ((firmaLeft > e.Position.X && e.Position.X > 0) && (firmaTop > e.Position.Y && e.Position.Y > 0))
                {
                    //si es el primer punto, su velocidad y aceleración serán 0
                    if (listaPuntos.Count == 0)
                    {
                        miPunto.xVel = 0;
                        miPunto.yVel = 0;
                        miPunto.xAceleracion = 0;
                        miPunto.yAceleracion = 0;
                    }
                    else
                    {
                        calculaAceleracionVelocidad(listaPuntos.ElementAt(listaPuntos.Count - 1), miPunto);
                    }

                    //calculamos el grosor de la firma en base a la velocidad en ese punto
                    double grosorTrazo = calculaGrosor(miPunto.velocidad);

                    listaPuntos.Add(miPunto);

                    if (listaPuntos.Count != 1)
                    {
                        Line trazo = new Line()
                        {
                            X1 = _previousPosition.X,
                            Y1 = _previousPosition.Y,
                            X2 = e.Position.X,
                            Y2 = e.Position.Y,
                            Stroke = _stroke,
                            StrokeThickness = 3
                        };

                        canvasFirma.Children.Add(trazo);
                    }
                    else
                    {
                        canvasFirma.Children.Add(
                           new Line()
                           {
                               X1 = e.Position.X,
                               Y1 = e.Position.Y,
                               X2 = e.Position.X,
                               Y2 = e.Position.Y,
                               Stroke = _stroke,
                               StrokeThickness = 3
                           }
                         );
                    }

                    Point pt;
                    pt.X = e.Position.X;
                    pt.Y = e.Position.Y;
                    _previousPosition = pt;
                }
            }
        }
        private void inkcanvas_ManipulationStarted(object sender, ManipulationStartedRoutedEventArgs e)
        {
            //creamos la lista de puntos donde almacenar los puntos de este trazo
            this.listaPuntos = new List<Punto>();
            _pressed = true;
        }
        private void inkcanvas_ManipulationCompleted(object sender, ManipulationCompletedRoutedEventArgs e)
        {
            //añadimos el lsitado de puntos en la variable _allPoints
            _allPoints.Add(listaPuntos);
            _pressed = false;

            menuFirmaVisibleOpcActivadas();
        }
        private void scroll_viewChanged(object sender, ScrollViewerViewChangedEventArgs e)
        {
            ScrollViewer scrollPDF = (ScrollViewer)sender;
            this.pdfScrollOffset = scrollPDF.VerticalOffset;
        }
        private void OnManipulationDelta(object sender, Windows.UI.Xaml.Input.ManipulationDeltaRoutedEventArgs e)
        {

            var pagina = this.flipView.Items.ElementAt(0) as PdfPageWrapper;
            
            //obtenemos los límites del MainCanvas
            //double maxLeft = Canvas.GetLeft(MainCanvas);
            //double maxTop = Canvas.GetTop(MainCanvas);


            double minLeft = ((this.MainCanvas.ActualWidth - pagina.Width) / 2);
            double maxLeft = (minLeft + (pagina.Width - marcoFirma.ActualWidth));
            double maxTop = this.bordeFlipView.ActualHeight;



            double componenteFirmaLeft = Canvas.GetLeft(ComponenteFirma);
            double componenteFirmaTop = Canvas.GetTop(ComponenteFirma);

            //comprobamos si con el movimiento, el control se sale de los márgenes establecidos por el elemento MainCanvas
            componenteFirmaLeft += e.Delta.Translation.X;
            componenteFirmaTop += e.Delta.Translation.Y;

            
            //izquierda
            if (componenteFirmaLeft < minLeft)
                componenteFirmaLeft = minLeft;
            //derecha
            if (componenteFirmaLeft > maxLeft)
                componenteFirmaLeft = maxLeft;
            //arriba
            if (componenteFirmaTop < 0)
            {
                componenteFirmaTop = 0;
            }
            if ((componenteFirmaTop + marcoFirma.ActualHeight) > (Canvas.GetTop(MainCanvas) + MainCanvas.ActualHeight))
            {
                componenteFirmaTop = (Canvas.GetTop(MainCanvas) + MainCanvas.ActualHeight) - marcoFirma.ActualHeight;
            }



            Canvas.SetTop(ComponenteFirma, componenteFirmaTop);
            Canvas.SetLeft(ComponenteFirma, componenteFirmaLeft);
        }
        private void OnManipulationCompleted(object sender, ManipulationCompletedRoutedEventArgs e)
        {
            //fijamos la opacidad del canvas firma al 100%
            canvasFirma.Background = new SolidColorBrush(Colors.White);
            canvasFirma.Opacity = 1;


            _pressed = false;
        }
        private void inkCanvas_PointerPressed_1(object sender, PointerRoutedEventArgs e)
        {
            if (_pressed)
            {
                _pressed = false;
            }
        }
        private void inkCanvas_PointerReleased_1(object sender, PointerRoutedEventArgs e)
        {
            if (_pressed)
            {
                _pressed = false;
            }
        }
        #endregion

        #region MISCELANEOS
        private bool estaFirmaVacia()
        {
            bool resultado = true;

            if (listaPuntos != null)
            {
                resultado = false;
            }

            return resultado;
        }
        private async Task<string> generar_pdf_desde_html(string nombreHtml)
        {
            string resultado;

            try
            {
                StorageFolder sfCSS = await carpetaActual.GetFolderAsync("css");
                StorageFolder sfIMG = await carpetaActual.GetFolderAsync("images");

                htmlToPDF htmlTOpdf = new htmlToPDF();
                gestionPDF = new gestionDocumentosPDF(carpetaActual, sfCSS, sfIMG, ApplicationData.Current.LocalFolder);
                gestionPDF.nombreDocumentoPDF = nombreHtml.Replace(".html", ".pdf"); //nombre del documento pdf de salida
                gestionPDF.nombreDocHtml = nombreHtml; //nombre del html de entrada
                if (imagenFirma != null)
                {
                    gestionPDF.nombreDocFirma = imagenFirma.Name;
                    gestionPDF.directorio_origen_firma = await imagenFirma.GetParentAsync();
                }

                resultado = await gestionPDF.transformar_html();
            }
            catch (Exception ex)
            {
                resultado = ex.Message;
            }

            return resultado;
        }
        private void flipView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {


        }
        private void flipView_DataContextChanged(FrameworkElement sender, DataContextChangedEventArgs args)
        {
            try
            {
                flipView.UpdateLayout();
                flipView.ContainerFromIndex(0);
            }
            catch (Exception ex)
            {
                string error;

                error = ex.ToString();
            }
            
        }
        private void checkUsoEmpresarial_Checked(object sender, RoutedEventArgs e)
        {
            //al marcar el check, mostramos el campo de Compañía
            this.lbCompañia.Visibility = Windows.UI.Xaml.Visibility.Visible;
            this.tbCompañia.Visibility = Windows.UI.Xaml.Visibility.Visible;
        }
        private void checkUsoEmpresarial_Unchecked(object sender, RoutedEventArgs e)
        {
            //al desmarcar el check, ocultamos el campo de Compañía

            this.tbCompañia.Text = "";
            this.lbCompañia.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.tbCompañia.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }
       
        private async void EliminarTemporales()
        {
            var localFolder = Windows.Storage.ApplicationData.Current.TemporaryFolder;
            StorageFile thefile = await localFolder.GetFileAsync(nombrePdf);
            await thefile.DeleteAsync(StorageDeleteOption.Default);
            thefile = await localFolder.GetFileAsync(nombreImagenFirma);
            await thefile.DeleteAsync(StorageDeleteOption.Default);
        }


        public async Task<Boolean> deleteTempLocalFiles()
        {
            Boolean res = false;

            //si se han generado ficheros, los borramos antes de salir de la app
            //limpiamos la carpeta de documentos locales donde se generan los temporales

            try
            {
                StorageFolder tempFolder = Windows.Storage.ApplicationData.Current.TemporaryFolder;
                var files = (await tempFolder.GetFilesAsync());


                foreach (var file in files)
                {
                    await file.DeleteAsync(StorageDeleteOption.Default);
                }

                res = true;
            }

            catch (Exception e)
            {
                res = false;
            }

            return res;
        }


        public async Task<Boolean> LoadFile(StorageFile pdf)
        {
            Boolean res = false;

            Windows.Data.Pdf.PdfDocument doc = await Windows.Data.Pdf.PdfDocument.LoadFromFileAsync(pdf);

            double pdfWidth = doc.GetPage(0).Dimensions.ArtBox.Width;
            double pdfHeight = doc.GetPage(0).Dimensions.ArtBox.Height;

            flipView.DataContext = null;
            flipView.SelectedItem = null;


            try
            {
                pages = new ObservableCollection<PdfPageWrapper>();
                PdfPageWrapper pageWrapperSeleccionada = null;

                for (uint count = 0; count < doc.PageCount; count++)
                {
                    PdfPageWrapper pageWrapper = new PdfPageWrapper(doc, count);
                    pages.Add(pageWrapper);
                    //if (count == (uint)(pdfPageNumber - 1))
                    //{
                    //    pageWrapperSeleccionada = pageWrapper;
                    //}
                }

                flipView.DataContext = pages;

                //if (pageWrapperSeleccionada != null)
                //{
                //    flipView.SelectedItem = pageWrapperSeleccionada;
                //}

                nombrePdf = pdf.Name;
                flipView.Focus(Windows.UI.Xaml.FocusState.Pointer);
                res = true;
            }
            catch (Exception ex)
            {
                string error;

                error = ex.ToString();
                res = false;
            }

            return res;

            

            
        }
        private ObservableCollection<PdfPageWrapper> pages;

        private Guid selectGuid(string type)
        {
            Guid BitmapEncoderGuid;

            switch (type)
            {
                case "jpg":
                    BitmapEncoderGuid = BitmapEncoder.JpegEncoderId;
                    break;

                case "png":
                    BitmapEncoderGuid = BitmapEncoder.PngEncoderId;
                    break;

                case "bmp":
                    BitmapEncoderGuid = BitmapEncoder.BmpEncoderId;
                    break;

                case "gif":
                    BitmapEncoderGuid = BitmapEncoder.GifEncoderId;
                    break;

                default:
                    BitmapEncoderGuid = BitmapEncoder.GifEncoderId;
                    break;
            }

            return BitmapEncoderGuid;
        }
        public async Task InsertarFirma(PdfPageWrapper paginaOriginal, StorageFile fileFirma, float x, float y)
        {
            try
            {
                Stream pdfStream = await documentoFirma.OpenStreamForReadAsync();
                PdfReader reader = new PdfReader(pdfStream);

                //2. creamos el pdf de salida
                StorageFolder tempFolder = ApplicationData.Current.TemporaryFolder;
                StorageFile pdfOutputFile;

                try
                {
                    pdfOutputFile = await tempFolder.CreateFileAsync(string.Format("{0}_signature.pdf",
                    DateTimeOffset.Now.ToString("MM_dd_yy_hh_mm_ss")), CreationCollisionOption.ReplaceExisting);
                }
                catch (Exception e)
                {
                    String mensaje = e.InnerException.ToString();
                    return;
                }

                IRandomAccessStream outputPdfStream = null;

                if (pdfOutputFile != null)
                {
                    outputPdfStream = await pdfOutputFile.OpenAsync(FileAccessMode.ReadWrite);

                }

                PdfStamper stamper = new PdfStamper(reader, outputPdfStream.AsStream(), '1', true);
                PdfContentByte pdfContentByte = stamper.GetOverContent((int)paginaOriginal.pageIndex);
                BitmapImage imagenFirma = new BitmapImage();
                imagenFirma = await LoadImage(fileFirma);

                IRandomAccessStream firmaStream = null;
                firmaStream = await fileFirma.OpenAsync(FileAccessMode.Read);
                iTextSharp.text.Image imagen = iTextSharp.text.Image.GetInstance(firmaStream.AsStream());

                //convertimos las posiciones en x e y del canvas de firma a positivo
                x = Math.Abs(x);
                y = Math.Abs(y);

                //las dimensiones del flipview reales son las de su primer item (en realidad es el primer item de tipo PdfPageWrapper)
                var flipViewitem = flipView.Items.ElementAt(0);

                /*
                float flipViewHeight = (float)((Windows.UI.Xaml.FrameworkElement)(flipView)).ActualHeight;
                float flipViewWidth = (float)((Windows.UI.Xaml.FrameworkElement)(flipView)).ActualWidth;*/
                // Obtener la anchura real de la página.
                PdfPageWrapper visorPDF = (PdfPageWrapper)flipView.Items.ElementAt(0);
                pdfPageNumber = (int)((PdfPageWrapper)flipView.SelectedItem).pageIndex;
                float flipViewHeight = (float)visorPDF.pdfPage.Size.Height;
                float flipViewWidth = (float)visorPDF.pdfPage.Size.Width;


                //escala en el eje x: ancho de la página real del PDF entre el ancho del flipview
                double scaleX = (reader.GetPageSize(1).Width) / flipViewWidth;


                //escala en el eje y: alto de la página real del PDF entre el alto del flipview
                double scaleY = (reader.GetPageSize(1).Height) / flipViewHeight;


                //escalamos la imagen de la firma siguiendo los factores calculados
                imagen.ScaleAbsolute((float)(canvasFirma.ActualWidth * scaleX), (float)(canvasFirma.ActualHeight * scaleY));

                //se obtienen las coordenadas absolutas de la firma y se escalan respecto de las del pdf
                //obtenemos la posición del contentRoot respecto de su hijo, el flipView
                var ttv = ContentRoot.TransformToVisual(this.flipView);

                Point screenCords = ttv.TransformPoint(new Point(0, 0));
                string OrientacionPapel = "";
                if (visorPDF.pdfPage.Size.Height > visorPDF.pdfPage.Size.Width)
                {
                    OrientacionPapel = "V";
                }
                else
                {
                    OrientacionPapel = "H";
                }
                double positionPdfViewX = Math.Abs(screenCords.X);
                double positionPdfViewY = Math.Abs(screenCords.Y);
                // mcx: Desplazamiento entre la izquierda del flipView y el documento pdf en sí. Será la mitad de la diferencia de anchuras.
                float mcx = 0;
                float mcy = 0;
                float cx = 0;
                float cy = 0;
                string OrientacionPantalla = "";
                if ((float)((Windows.UI.Xaml.FrameworkElement)(flipView)).ActualWidth > (float)((Windows.UI.Xaml.FrameworkElement)(flipView)).ActualHeight)
                {
                    OrientacionPantalla = "H";
                    mcx = ((float)((Windows.UI.Xaml.FrameworkElement)(flipView)).ActualWidth - (float)visorPDF.pdfPage.Size.Width) / 2;
                    mcy = ((float)((Windows.UI.Xaml.FrameworkElement)(flipView)).ActualHeight - (float)visorPDF.pdfPage.Size.Height);
                    cx = (float)((((positionPdfViewX + x) - mcx) * scaleX));
                    cy = 0;
                }
                else
                {
                    OrientacionPantalla = "V";
                    mcx = ((float)((Windows.UI.Xaml.FrameworkElement)(flipView)).ActualWidth - (float)visorPDF.pdfPage.Size.Width);
                    mcy = ((float)((Windows.UI.Xaml.FrameworkElement)(flipView)).ActualHeight - (float)visorPDF.pdfPage.Size.Height) / 2;
                    mcx = mcx > 0 ? mcx : 0;
                    cx = (float)((((positionPdfViewX + x) - mcx) * scaleX));
                    cy = 0;
                }
                if (OrientacionPapel == "H")
                {
                    if (OrientacionPantalla == "V")
                    {
                        mcy = mcy * 2;
                    }
                    mcy = mcy > 0 ? mcy : 0; // Sólo valores positivos, si es negativo es que llena toda la pantalla
                    float valorMaximoY = ((float)((Windows.UI.Xaml.FrameworkElement)(flipView)).ActualHeight) > ((float)visorPDF.pdfPage.Size.Height) ? ((float)((Windows.UI.Xaml.FrameworkElement)(flipView)).ActualHeight) : ((float)visorPDF.pdfPage.Size.Height);
                    cy = (float)(valorMaximoY - (mcy / 2) - (float)canvasFirma.ActualHeight - y - this.pdfScrollOffset) * (float)scaleY;
                }
                else
                {
                    cy = (float)(((float)((Windows.UI.Xaml.FrameworkElement)(flipView)).ActualHeight - (y) - positionPdfViewY - canvasFirma.ActualHeight - mcy - this.pdfScrollOffset) * scaleY);
                }

                //posicionamos la imagen del grafo de la firma
                imagen.SetAbsolutePosition(cx, cy);

                //tratamiento de la imagen de marca de agua
                StorageFolder InstallationFolder = Windows.ApplicationModel.Package.Current.InstalledLocation;
                StorageFile file = await InstallationFolder.GetFileAsync(@"Assets\watermark_INDRA_ALPHA.png");
                Stream marcaAgua = await file.OpenStreamForReadAsync();

                iTextSharp.text.Image imagenMarcaAgua = iTextSharp.text.Image.GetInstance(marcaAgua);

                //factor de escala de la marca de agua
                float factor = imagenMarcaAgua.Width / imagenMarcaAgua.Height;
                //offset para centrar la marca de agua en el centro de la firma
                float offset = 0;

                if (imagen.Height > (imagen.Width / factor))
                {
                    offset = (float)(((imagen.Height - imagen.Width / factor) / 2) * scaleX);
                }

                imagenMarcaAgua.SetAbsolutePosition(cx + offset, cy + offset);
                imagenMarcaAgua.ScaleAbsolute(imagen.ScaledWidth, imagen.ScaledHeight / factor);

                //pdfContentByte.AddImage(imagenMarcaAgua); PA89: Eliminamos la marca de agua
                pdfContentByte.AddImage(imagen);

                //añadimos a los metadatos del pdf la estructura xml con la información

                String structura = string.Empty;

                

                BiometricsData.DeviceRequest request = new DeviceRequest();
                structura = await request.crearEstructura(fileFirma, _allPoints, documentoFirma, datosUsuario);
                //String structura = await crearEstructura(fileFirma);



                IDictionary<string, string> info = new Dictionary<string, string>();
                info.Add("Keywords", structura);

                stamper.MoreInfo = info;

                MemoryStream os = new System.IO.MemoryStream();
                XmpWriter xmp = new XmpWriter(os, info);
                xmp.Close();        

                //FIN añadimos a los metadatos del pdf la estructura xml con la información
                stamper.Close();
                
                outputPdfStream.Dispose();
                pdfStream.Dispose();
                reader.Close();
                reader.Dispose();
                stamper.Dispose();
               

                documentoFirma = pdfOutputFile;
                LoadFile(documentoFirma);
            }
            catch (Exception e)
            {
                string error;
            }
        }


        public async Task InsertarFirma(List<PdfPageWrapper> listaPaginas, StorageFile fileFirma, float x, float y)
        {
            try
            {
                Stream pdfStream = await documentoFirma.OpenStreamForReadAsync();
                PdfReader reader = new PdfReader(pdfStream);

                //2. creamos el pdf de salida
                StorageFolder tempFolder = ApplicationData.Current.TemporaryFolder;
                StorageFile pdfOutputFile;

                try
                {
                    pdfOutputFile = await tempFolder.CreateFileAsync(string.Format("{0}_signature.pdf",
                    DateTimeOffset.Now.ToString("MM_dd_yy_hh_mm_ss")), CreationCollisionOption.ReplaceExisting);
                }
                catch (Exception e)
                {
                    String mensaje = e.InnerException.ToString();
                    return;
                }

                IRandomAccessStream outputPdfStream = null;

                if (pdfOutputFile != null)
                {
                    outputPdfStream = await pdfOutputFile.OpenAsync(FileAccessMode.ReadWrite);

                }

               
                BitmapImage imagenFirma = new BitmapImage();
                imagenFirma = await LoadImage(fileFirma);

                IRandomAccessStream firmaStream = null;
                firmaStream = await fileFirma.OpenAsync(FileAccessMode.Read);
                iTextSharp.text.Image imagen = iTextSharp.text.Image.GetInstance(firmaStream.AsStream());

                //convertimos las posiciones en x e y del canvas de firma a positivo
                x = Math.Abs(x);
                y = Math.Abs(y);

                //las dimensiones del flipview reales son las de su primer item (en realidad es el primer item de tipo PdfPageWrapper)
                var flipViewitem = flipView.Items.ElementAt(0);

                /*
                float flipViewHeight = (float)((Windows.UI.Xaml.FrameworkElement)(flipView)).ActualHeight;
                float flipViewWidth = (float)((Windows.UI.Xaml.FrameworkElement)(flipView)).ActualWidth;*/
                // Obtener la anchura real de la página.
                PdfPageWrapper visorPDF = (PdfPageWrapper)flipView.Items.ElementAt(0);
                pdfPageNumber = (int)((PdfPageWrapper)flipView.SelectedItem).pageIndex;
                float flipViewHeight = (float)visorPDF.pdfPage.Size.Height;
                float flipViewWidth = (float)visorPDF.pdfPage.Size.Width;


                //escala en el eje x: ancho de la página real del PDF entre el ancho del flipview
                double scaleX = (reader.GetPageSize(1).Width) / flipViewWidth;


                //escala en el eje y: alto de la página real del PDF entre el alto del flipview
                double scaleY = (reader.GetPageSize(1).Height) / flipViewHeight;


                //escalamos la imagen de la firma siguiendo los factores calculados
                imagen.ScaleAbsolute((float)(canvasFirma.ActualWidth * scaleX), (float)(canvasFirma.ActualHeight * scaleY));

                //se obtienen las coordenadas absolutas de la firma y se escalan respecto de las del pdf
                //obtenemos la posición del contentRoot respecto de su hijo, el flipView
                var ttv = ContentRoot.TransformToVisual(this.flipView);

                Point screenCords = ttv.TransformPoint(new Point(0, 0));
                string OrientacionPapel = "";
                if (visorPDF.pdfPage.Size.Height > visorPDF.pdfPage.Size.Width)
                {
                    OrientacionPapel = "V";
                }
                else
                {
                    OrientacionPapel = "H";
                }
                double positionPdfViewX = Math.Abs(screenCords.X);
                double positionPdfViewY = Math.Abs(screenCords.Y);
                // mcx: Desplazamiento entre la izquierda del flipView y el documento pdf en sí. Será la mitad de la diferencia de anchuras.
                float mcx = 0;
                float mcy = 0;
                float cx = 0;
                float cy = 0;
                string OrientacionPantalla = "";
                if ((float)((Windows.UI.Xaml.FrameworkElement)(flipView)).ActualWidth > (float)((Windows.UI.Xaml.FrameworkElement)(flipView)).ActualHeight)
                {
                    OrientacionPantalla = "H";
                    mcx = ((float)((Windows.UI.Xaml.FrameworkElement)(flipView)).ActualWidth - (float)visorPDF.pdfPage.Size.Width) / 2;
                    mcy = ((float)((Windows.UI.Xaml.FrameworkElement)(flipView)).ActualHeight - (float)visorPDF.pdfPage.Size.Height);
                    cx = (float)((((positionPdfViewX + x) - mcx) * scaleX));
                    cy = 0;
                }
                else
                {
                    OrientacionPantalla = "V";
                    mcx = ((float)((Windows.UI.Xaml.FrameworkElement)(flipView)).ActualWidth - (float)visorPDF.pdfPage.Size.Width);
                    mcy = ((float)((Windows.UI.Xaml.FrameworkElement)(flipView)).ActualHeight - (float)visorPDF.pdfPage.Size.Height) / 2;
                    mcx = mcx > 0 ? mcx : 0;
                    cx = (float)((((positionPdfViewX + x) - mcx) * scaleX));
                    cy = 0;
                }
                if (OrientacionPapel == "H")
                {
                    if (OrientacionPantalla == "V")
                    {
                        mcy = mcy * 2;
                    }
                    mcy = mcy > 0 ? mcy : 0; // Sólo valores positivos, si es negativo es que llena toda la pantalla
                    float valorMaximoY = ((float)((Windows.UI.Xaml.FrameworkElement)(flipView)).ActualHeight) > ((float)visorPDF.pdfPage.Size.Height) ? ((float)((Windows.UI.Xaml.FrameworkElement)(flipView)).ActualHeight) : ((float)visorPDF.pdfPage.Size.Height);
                    cy = (float)(valorMaximoY - (mcy / 2) - (float)canvasFirma.ActualHeight - y - this.pdfScrollOffset) * (float)scaleY;
                }
                else
                {
                    cy = (float)(((float)((Windows.UI.Xaml.FrameworkElement)(flipView)).ActualHeight - (y) - positionPdfViewY - canvasFirma.ActualHeight - mcy - this.pdfScrollOffset) * scaleY);
                }

                //posicionamos la imagen del grafo de la firma
                imagen.SetAbsolutePosition(cx, cy);


                iTextSharp.text.Image imagenMarcaAgua = null;


                //si el tipo de usuario es "gratis", hay que meter la marca de agua a la imagen de la firma
                //if (tipoUsuario == EncryptionUtils.tipoUsuario.gratis)
                if(datosUsuario.premium == false)
                {
                    //tratamiento de la imagen de marca de agua
                    StorageFolder InstallationFolder = Windows.ApplicationModel.Package.Current.InstalledLocation;
                    StorageFile file = await InstallationFolder.GetFileAsync(@"Assets\watermark_INDRA_ALPHA.png");
                    Stream marcaAgua = await file.OpenStreamForReadAsync();

                    imagenMarcaAgua = iTextSharp.text.Image.GetInstance(marcaAgua);

                    //factor de escala de la marca de agua
                    float factor = imagenMarcaAgua.Width / imagenMarcaAgua.Height;
                    //offset para centrar la marca de agua en el centro de la firma
                    float offset = 0;

                    if (imagen.Height > (imagen.Width / factor))
                    {
                        offset = (float)(((imagen.Height - imagen.Width / factor) / 2) * scaleX);
                    }



                    //calculamos el punto de inserción de la marca de agua para que quede centrada respecto de la firma
                    var desplazH = (imagen.Width - imagenMarcaAgua.Width) / 2;
                    var desplazV = (imagen.Height - imagenMarcaAgua.Height) / 2;

                 

                    //imagen.ScaleAbsolute((float)(canvasFirma.ActualWidth * scaleX), (float)(canvasFirma.ActualHeight * scaleY));

                    imagenMarcaAgua.ScaleAbsolute(imagen.ScaledWidth / factor, imagen.ScaledHeight / factor);
                    imagenMarcaAgua.SetAbsolutePosition(cx + desplazH, cy + desplazV);
                }

                


                //añadimos la imagen a cada una de las páginas 

                PdfStamper stamper = new PdfStamper(reader, outputPdfStream.AsStream(), '1', true);


                foreach (PdfPageWrapper pagina in listaPaginas)
                {
                    PdfContentByte pdfContentByte = stamper.GetOverContent((int)pagina.pageIndex);
                    
                    if(imagenMarcaAgua != null)
                        pdfContentByte.AddImage(imagenMarcaAgua);

                    //pdfContentByte.AddImage(imagenMarcaAgua); PA89: Eliminamos la marca de agua
                    pdfContentByte.AddImage(imagen);
                }

                

                //añadimos a los metadatos del pdf la estructura xml con la información

                String structura = string.Empty;



                BiometricsData.DeviceRequest request = new DeviceRequest();
                structura = await request.crearEstructura(fileFirma, _allPoints, documentoFirma, datosUsuario);
               

                IDictionary<string, string> info = new Dictionary<string, string>();
                info.Add("Keywords", structura);

                stamper.MoreInfo = info;

                MemoryStream os = new System.IO.MemoryStream();
                XmpWriter xmp = new XmpWriter(os, info);
                xmp.Close();

                //FIN añadimos a los metadatos del pdf la estructura xml con la información
                stamper.Close();

                outputPdfStream.Dispose();
                pdfStream.Dispose();
                reader.Close();
                reader.Dispose();
                stamper.Dispose();


                documentoFirma = pdfOutputFile;
                LoadFile(documentoFirma);
            }
            catch (Exception e)
            {
                string error;
            }
        }




        private void encrypt()
        {

        }
        private static async Task<BitmapImage> LoadImage(StorageFile file)
        {
            BitmapImage bitmapImage = new BitmapImage();
            FileRandomAccessStream stream = (FileRandomAccessStream)await file.OpenAsync(FileAccessMode.Read);

            bitmapImage.SetSource(stream);

            return bitmapImage;

        }
        private async Task<String> crearEstructura(StorageFile imagenFirma)
        {
            //obtenemos la clave pública del certificado local
            //Inicializamos el ISO Parser
            ISOUtils isoUtils = new ISOUtils();
            isoUtils.addBodyToBiometricData(_allPoints);

            MetadataBuilder metadata = new MetadataBuilder();
            String signMetadata = "";
            signMetadata += await metadata.metadataXML(isoUtils.getmBiometricsByteList(), imagenFirma, documentoFirma.Name, documentoFirma, datosUsuario);

            return signMetadata;
        }
      
        private void BorrarFirma()
        {
            if (!estaFirmaVacia())
            {
                //tenemos que limpiar todos los puntos pintados en canvasFirma
                canvasFirma.Children.Clear();

                //desactivamos las opciones del menú de firma
                this.borrarFirmaMenuItem.IsEnabled = false;
                this.firmaActualMenuItem.IsEnabled = false;
                this.firmaTodasMenuItem.IsEnabled = false;

                //tenemos que limpiar todos los puntos pintados en canvasFirma
                canvasFirma.Children.Clear();
                listaPuntos.Clear();
                listaPuntos = null;
                pdfScrollOffset = 0.0;
                _pressed = false;
                _previousPosition = default(Point);
                _stroke = new SolidColorBrush(Colors.Black);
                _allPoints = new List<List<Punto>>();
                listaPuntos = null;
            }
        }
        private async void mensajePremiumVersion()
        {
            var messageDialog = new MessageDialog("Funcionalidad no disponible en la versión Trial. Por favor, acceda a la versión Premium.");
            await messageDialog.ShowAsync();
        }
        public bool IsValidEmail(string strIn)
        {
            if (String.IsNullOrEmpty(strIn))
                return true;

          
            try
            {
                return Regex.IsMatch(strIn,
                      @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                      @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                      RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }
        public bool IsValidPhoneNumber(string strIn)
        {
            Regex Val = new Regex(@"^[+-]?\d+(\.\d+)?$");

            if (String.IsNullOrEmpty(strIn))
                return true;
            if (Val.IsMatch(strIn))
            {
                return true;
            }
            else
                return false;
        }
        private void calculaAceleracionVelocidad(Punto punto0, Punto punto1)
        {
            //velocidad --> se calcula en base a la posición y el tiempo del punto actual respecto del punto anterior en sus dos componentes, X e Y
            //                     (X1 - X0)
            //    VelocidadX =   ------------
            //                     (T1 - T0)
            //
            //                     (Y1 - Y0)
            //    VelocidadY =   ------------
            //                     (T1 - T0)

            double xAceleracion, yAceleracion;

            //obtenemos la diferencia de tiempo (en segundos)
            double difTiempo = Math.Abs((punto0.time - punto1.time).TotalMilliseconds);

            //calculamos la aceleracion
            xAceleracion = (punto1.xVel - punto0.xVel) / difTiempo;
            yAceleracion = (punto1.yVel - punto0.yVel) / difTiempo;

            punto1.xAceleracion = xAceleracion;
            punto1.yAceleracion = yAceleracion;

            ////calculamos el módulo de la aceleración como a = raizCuadrada((Ax *Ax) + (Ay * Ay))

            double modVelocidad = Math.Sqrt(Math.Pow(punto1.xVel, 2) + Math.Pow(punto1.yVel, 2));
            punto1.velocidad = modVelocidad;
        }
        private double calculaGrosor(double aceleracion)
        {
            double retorno = 0;
            double factor = 0.5;

            if (factor / aceleracion < 2)
            {
                retorno = 2;
            }
            else if (factor / aceleracion > 4)
            {
                retorno = 4;
            }
            else
                retorno = factor / aceleracion;

            return retorno;
        }
        PolyLineSegment GetBezierApproximation(Point[] controlPoints, int outputSegmentCount)
        {
            PolyLineSegment poliSegment = new PolyLineSegment();
            PointCollection colPoint = new PointCollection();

            foreach (Point p in controlPoints)
                colPoint.Add(p);
            poliSegment.Points = colPoint;

            return poliSegment;
        }
        Point GetBezierPoint(double t, Point[] controlPoints, int index, int count)
        {
            if (count == 1)
                return controlPoints[index];
            var P0 = GetBezierPoint(t, controlPoints, index, count - 1);
            var P1 = GetBezierPoint(t, controlPoints, index + 1, count - 1);
            return new Point((1 - t) * P0.X + t * P1.X, (1 - t) * P0.Y + t * P1.Y);
        }
        #endregion



        protected override async void OnNavigatedTo(Windows.UI.Xaml.Navigation.NavigationEventArgs e)
        {


            var aux = e.Parameter.GetType().AssemblyQualifiedName;
            var aux2 = e.Parameter.GetType().FullName;
            var aux3 = e.Parameter.GetType().Name;

            if(e.Parameter.GetType().Name.Equals("StorageFile"))
            {
                //cargamos el documento pdf
                CargarDocumentoWeb(e.Parameter as StorageFile);
                tipoUsuario = EncryptionUtils.tipoUsuario.gratis;
            }
            else

                tipoUsuario = (EncryptionUtils.tipoUsuario) e.Parameter;
        }




        private async void FirmarTodasLasPaginas_click(object sender, RoutedEventArgs e)
        {
            Boolean res = false;
            
            List<PdfPageWrapper> listaPaginas = new List<PdfPageWrapper>();


            if (!estaFirmaVacia())
            {
                //activamos la barra de progreso de firma de documento
                this.border.Visibility = Windows.UI.Xaml.Visibility.Visible;

               
                //añadimos todas las páginas a un listado
                foreach (PdfPageWrapper pagina in flipView.Items)
                {
                    listaPaginas.Add(pagina);
                }
               

                //1. obtenemos la posición actual del componente de firma
                var ttv = flipView.TransformToVisual(this.canvasFirma);
                Point screenCords = ttv.TransformPoint(new Point(0, 0));


                try
                {
                    var resultado = await Firmar(listaPaginas, screenCords,tipoUsuario);
                }
                catch (Exception ex)
                {
                    string error = ex.ToString();
                    res = false;
                }



                res = true;
            }
            else
            {
                MessageDialog dialog = new MessageDialog("Es necesario dibujar una firma para continuar.");
                await dialog.ShowAsync();
                res = true;
            }

            if (!res)
            {
                res = true;
            }
        }


        private void menuFirmaVisibleOpcDesactivadas()
        {
            this.menuFirmar.Visibility = Visibility.Visible;
            this.firmaActualMenuItem.IsEnabled = false;
            this.firmaTodasMenuItem.IsEnabled = false;
            this.borrarFirmaMenuItem.IsEnabled = false;
        }

        private void menuFirmaVisibleOpcActivadas()
        {
            this.menuFirmar.Visibility = Visibility.Visible;
            this.firmaActualMenuItem.IsEnabled = true;
            this.firmaTodasMenuItem.IsEnabled = true;
            this.borrarFirmaMenuItem.IsEnabled = true;
        }

        private void menuEnviarVisibleOpcDesactivadas()
        {
            this.menuEnviar.Visibility = Visibility.Visible;
            this.enviarDocMenuItem.IsEnabled = false;
            this.enviarDocMailMenuItem.IsEnabled = false;
            this.enviarDocFTPMenuItem.IsEnabled = false;
            this.enviarDocServicioMenuItem.IsEnabled = false;
            this.borrarFirmaDocMenuItem.IsEnabled = false;
        }
        private void menuEnviarVisibleOpcActivadas()
        {
            this.menuEnviar.Visibility = Visibility.Visible;
            this.enviarDocMenuItem.IsEnabled = true;
            this.enviarDocMailMenuItem.IsEnabled = true;
            this.enviarDocFTPMenuItem.IsEnabled = true;
            this.enviarDocServicioMenuItem.IsEnabled = true;
            this.borrarFirmaDocMenuItem.IsEnabled = true;
            
        }



        

        private void btnCerrar_Click(object sender, RoutedEventArgs e)
        {
            this.PopUp.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }



        #region datos registro

        public async Task<UserData> obtenerDatosRegistro()
        {

            UserData datosUsuario = new UserData();

            //comprobamos si hay valores para los campos en las preferencias del sistema
            Windows.Storage.ApplicationDataContainer localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;

            if (localSettings.Values["name"] != null)
                datosUsuario.name = localSettings.Values["name"].ToString();

            if (localSettings.Values["surname"] != null)
                datosUsuario.surName = localSettings.Values["surname"].ToString();

            if (localSettings.Values["email"] != null)
                datosUsuario.email = localSettings.Values["email"].ToString();

            if (localSettings.Values["tlf"] != null)
                datosUsuario.tlf = localSettings.Values["tlf"].ToString();

            if (localSettings.Values["BusinessName"] != null)
                datosUsuario.businessName = localSettings.Values["BusinessName"].ToString();

            if (localSettings.Values["idCode"] != null)
                datosUsuario.idCode = localSettings.Values["idCode"].ToString();

            if (localSettings.Values["username"] != null)
                datosUsuario.username = localSettings.Values["username"].ToString();

            if (localSettings.Values["password"] != null)
                datosUsuario.password = localSettings.Values["password"].ToString();

            if (localSettings.Values["userFTP"] != null)
                datosUsuario.userFTP = localSettings.Values["userFTP"].ToString();

            if (localSettings.Values["passwFTP"] != null)
                datosUsuario.passwFTP = localSettings.Values["passwFTP"].ToString();

            if (localSettings.Values["direccionFTP"] != null)
                datosUsuario.direccionFTP = localSettings.Values["direccionFTP"].ToString();


            if (localSettings.Values["colorUsuario"] != null)
            {
               

               Color colorUsuario =  WinRTXamlToolkit.Imaging.ColorExtensions.FromString(localSettings.Values["colorUsuario"].ToString());


               datosUsuario.colorUsuario = new SolidColorBrush(colorUsuario);
            }
                

            if (localSettings.Values["iconoUsuario"] != null)
            {


                  StorageFile imagen = await StorageFile.GetFileFromPathAsync(localSettings.Values["iconoUsuario"] .ToString());

                  var stream = await imagen.OpenReadAsync();
                  iconoSeleccionado = new BitmapImage();

                  iconoSeleccionado.SetSource(stream);
                  imagenUsuario = imagen;

                  datosUsuario.iconoUsuario = iconoSeleccionado;
                
            }

            if(localSettings.Values["idUser"] != null)
            {
                datosUsuario.idUser = (int)localSettings.Values["idUser"] ;
            }

            if (localSettings.Values["isPremium"] != null)
            {
                datosUsuario.premium = (bool)localSettings.Values["isPremium"];
            }
                

            return datosUsuario;
        }

        private void cargarDatosUsuario(UserData datosUsuario)
        {


            if (datosUsuario.name != null)
                tbNombre.Text = datosUsuario.name;
            if (datosUsuario.surName != null)
                tbApellidos.Text = datosUsuario.surName;
            if (datosUsuario.email != null)
                tbEmail.Text = datosUsuario.email;
            if (datosUsuario.tlf != null)
                tbMovil.Text = datosUsuario.tlf;
            if (datosUsuario.businessName != null)
                tbCompañia.Text = datosUsuario.businessName;
            if (datosUsuario.idCode != null)
                tbDNI.Text = datosUsuario.idCode;
            if (datosUsuario.username != null)
            {
                tbUser.Text = datosUsuario.username;
               
                tbUser.IsEnabled = false;
               
            }
            else
                tbUser.IsEnabled = true;

            if (datosUsuario.password != null)
            {
                passwordBox.Password = datosUsuario.password;
                passwordBox.IsPasswordRevealButtonEnabled = true;             
                passwordBox.IsEnabled = false;
               
            }
            else
                passwordBox.IsEnabled = true;

        }


        #endregion

        private void btnCancelarPersonalizacion_click(object sender, RoutedEventArgs e)
        {
            this.menuPersonalizacion.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }

        private void btnAceptarPersonalizacion_click(object sender, RoutedEventArgs e)
        {

            //cerramos el formulario de personalización
            this.menuPersonalizacion.Visibility = Windows.UI.Xaml.Visibility.Collapsed;

            //guardamos el color del usuario si se ha seleccionado alguno
            if (ColorSeleccionado != null)
            {
                datosUsuario.colorUsuario = ColorSeleccionado;
              

                localSettings.Values["colorUsuario"] = ((Windows.UI.Xaml.Media.SolidColorBrush)(ColorSeleccionado)).Color.ToString();
            }
                

            //cambiamos la imagen del icono de la barra de menú si se ha seleccionado alguna
            if(iconoSeleccionado != iconoDefecto)
            {
                this.iconoUsuarioBarra.Source = iconoSeleccionado;
                datosUsuario.iconoUsuario = iconoSeleccionado;
               
                localSettings.Values["iconoUsuario"] = imagenUsuario.Path;
            }
            

            this.barraMenu.Background = ColorSeleccionado;
            
        }

        public void loadcolors()
        {
            List<colors> mycolors = new List<colors>();
            Type type = typeof(Windows.UI.Colors);
            PropertyInfo[] Properties = type.GetRuntimeProperties().ToArray<PropertyInfo>();
            foreach (var item in Properties)
            {
                string s = item.Name;
                Color c = (Color)item.GetValue(null, null);
                colors newcolor = new colors();
                mycolors.Add(new colors()
                {
                    color = item.Name,
                    ColorBrush = new SolidColorBrush(c)
                });
            }
            colorList.ItemsSource = mycolors;
        }

        private void colorList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var color = (colors)e.AddedItems[0];

            var rectangulo = this.gridIconoColor.Children[0] as Rectangle;


            ColorSeleccionado = color.ColorBrush;

            
        }

        private async void selectImage_Click(object sender, RoutedEventArgs e)
        {
            //abrimos un file picker en la carpeta de imágenes

              //para cargar un documento local, abrimos un cuadro de diálogo para seleccionar el documento
            FileOpenPicker openPicker = new FileOpenPicker();
            openPicker.ViewMode = PickerViewMode.Thumbnail;
            openPicker.ViewMode = PickerViewMode.List;
            openPicker.SuggestedStartLocation = PickerLocationId.PicturesLibrary;
            openPicker.FileTypeFilter.Add(".png");
            
            imagenUsuario = await openPicker.PickSingleFileAsync();

            if (imagenUsuario != null)
            {
                var stream = await imagenUsuario.OpenReadAsync();
                iconoSeleccionado = new BitmapImage();

                iconoSeleccionado.SetSource(stream);

                this.iconoUsuario.Source = iconoSeleccionado;
              
            }
            
        }

        #region INotifiedProperty Block
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        private void btnRestaurarPersonalizacion_click(object sender, RoutedEventArgs e)
        {
            this.menuPersonalizacion.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            //restablecemos los colores y la imagen a los valores predeterminados
            this.iconoUsuarioBarra.Source = iconoDefecto;
            this.iconoUsuario.Source = iconoDefecto;
            this.ColorSeleccionado = colorDefecto;
            this.barraMenu.Background = colorDefecto;
            iconoSeleccionado = iconoDefecto;


            //limpiamos las preferencias de usuario
            datosUsuario.iconoUsuario = null;
            localSettings.Values.Remove("iconoUsuario");
            datosUsuario.colorUsuario = null;
            localSettings.Values.Remove("colorUsuario");
        }

        private async void CargarDocumentosPendientes_click(object sender, RoutedEventArgs e)
        {
            
            GetPetitionsResponse resConsultaPendientes = null;
            //llamamos al WS de consulta de documentos pendientes para el usuario
            Servicios.WebServices servicios = new Servicios.WebServices();

            resConsultaPendientes = await servicios.getPetitionsWebService(datosUsuario.username, datosUsuario.password);

            if (resConsultaPendientes.Petitions.Count > 0)
            {
                //si hay documentos pendientes, mostramos un listado con el nombre
                docsPendientes = resConsultaPendientes.Petitions;

                this.listadoDocsPendientes.ItemsSource = docsPendientes;                
                this.menuDocPendientes.Visibility = Windows.UI.Xaml.Visibility.Visible;


            }
            //no hay documentos pendientes, mostramos mensaje
            else
            {
                popupMessage = loader.GetString("noDocumentosPendientes");
            }


            if (!String.IsNullOrEmpty(popupMessage))
            {
                this.tbPopUp.Text = popupMessage;
                this.PopUp.Visibility = Windows.UI.Xaml.Visibility.Visible;
            }
        }

        private void btnCancelarCargarDocWeb_click(object sender, RoutedEventArgs e)
        {
            this.menuCargarDocWeb.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }

        private async void btnAceptarCargarDocWeb_click(object sender, RoutedEventArgs e)
        {

            isPendingDocument = false;

            //descargamos el pdf de la web
            gestionDocumentosPDF gestorPDF = new gestionDocumentosPDF();
            String resultadoDescarga = await gestorPDF.cargarPdfWeb(this.direccionWebBox.Text);

            if(String.Equals(resultadoDescarga, "OK"))
            {
                //obtenemos el archivo pdf que hemos descargado
                //obtenemos el nombre del archivo 
                int indexArchivo = this.direccionWebBox.Text.LastIndexOf('/');
                String nombreArchivo = this.direccionWebBox.Text.Substring(indexArchivo + 1);

                try
                {
                    StorageFile pdfACargar = await ApplicationData.Current.LocalFolder.GetFileAsync(nombreArchivo);

                    CargarDocumentoWeb(pdfACargar);

                    this.menuCargarDocWeb.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                }
                catch (Exception ex)
                {

                }
                
            }
            else
            {

                this.tbPopUp.Text = resultadoDescarga;
                this.PopUp.Visibility = Windows.UI.Xaml.Visibility.Visible;
                
            }

        }

        private void btnCancelarEnvioFTP_click(object sender, RoutedEventArgs e)
        {
            this.menuEnvioFTP.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }

        private async void btnAceptarEnvioFTP_click(object sender, RoutedEventArgs e)
        {
            

            Boolean subidoFTP = false;

            //comprobación datos obligatorios
            if(String.IsNullOrEmpty(this.nombreUsuarioFTPBox.Text))
            {
                popupMessage = loader.GetString("validacionNombreFtp");
               
            }
            if(String.IsNullOrEmpty(this.passwordBoxUsuarioFTP.Password.ToString()))
            {
                popupMessage += "\n" + loader.GetString("validacionPasswFtp");
            }
            if (String.IsNullOrEmpty(this.urlFTPBox.Text))
            {
                popupMessage += "\n" + loader.GetString("validacionServerFtp");
            }
            
            if( !String.IsNullOrEmpty(popupMessage))
            {
                this.tbPopUp.Text = popupMessage;
                this.PopUp.Visibility = Windows.UI.Xaml.Visibility.Visible;
                return;
               
            }

            
            //si está marcado el check de guardar datos, los guardamos en las preferencias de la App
            if(this.guardaDatosCheck.IsChecked == true)
            {
                datosUsuario.userFTP = this.nombreUsuarioFTPBox.Text;
                datosUsuario.passwFTP = this.passwordBoxUsuarioFTP.Password;
                datosUsuario.direccionFTP = this.urlFTPBox.Text;

                localSettings.Values["userFTP"] = this.nombreUsuarioFTPBox.Text.ToString();
                localSettings.Values["passwFTP"] = this.passwordBoxUsuarioFTP.Password.ToString();
                localSettings.Values["direccionFTP"] = this.urlFTPBox.Text.ToString();
            }

            //si no está marcado, las borramos si estuvieran guardadas
            else
            {
                if (datosUsuario.userFTP != String.Empty)
                {
                    datosUsuario.userFTP = String.Empty;
                    localSettings.Values.Remove("userFTP");
                }
                if (datosUsuario.passwFTP != String.Empty)
                {
                    datosUsuario.passwFTP = String.Empty;
                    localSettings.Values.Remove("passwFTP");
                }                  
                if(datosUsuario.direccionFTP != String.Empty)
                {
                    datosUsuario.direccionFTP = String.Empty;
                    localSettings.Values.Remove("direccionFTP");
                }
            }


            //obtenemos la dirección del servidor FTP y la ruta de los datos introducidos por el usuario
            int indexBarra = this.urlFTPBox.Text.IndexOf('/');

            string ftpServer, ftpPath;


            if (indexBarra != -1)
            {
                ftpServer = this.urlFTPBox.Text.Substring(0, indexBarra);
                ftpPath = this.urlFTPBox.Text.Substring(indexBarra + 1);
            }
            else
            {
                ftpServer = this.urlFTPBox.Text;
                ftpPath = null;
            }
                

            subidoFTP = await UploadToFTP(ftpServer, ftpPath, pdfOriginal.Name, nombreUsuarioFTPBox.Text, passwordBoxUsuarioFTP.Password.ToString());

            if(!subidoFTP)
            {
                popupMessage = loader.GetString("errorEnvioFTP");
                this.tbPopUp.Text = popupMessage;
                this.PopUp.Visibility = Windows.UI.Xaml.Visibility.Visible;
                return;
            }




        }


        private async Task<bool> UploadToFTP(string ftpServer, string ftpPath, string filename, string username, string password)
        {
            Boolean resultado = false;

            ftpManager ftpManager = new ftpManager();
            FtpClient clienteFtp;


            try
            {
                clienteFtp = ftpManager.CreateFtpClient(ftpServer, username, password);
                Boolean isFtpConnected = await ftpManager.ConnectAsync(clienteFtp);
                await clienteFtp.SetWorkingDirectoryAsync(ftpPath);               

                StorageFile PdfEnviar = documentoFirma;
                await ftpManager.CreateFileAsync(clienteFtp, ftpPath, pdfOriginal, pdfOriginal.Name);
                resultado = true;
            }
            catch (Exception e)
            {
                resultado = false;
            }          


           
          

             return resultado;
        }

        private async void EnviarPorFTP_click(object sender, RoutedEventArgs e)
        {
            this.nombreUsuarioFTPBox.Text = string.Empty;
            this.passwordBoxUsuarioFTP.Password = string.Empty;
            this.urlFTPBox.Text = string.Empty;
            this.cancelarEnvioFTPBtn.BorderBrush = ColorSeleccionado;

            //si hay datos de configuración del usuario, los recogemos
            datosUsuario = await obtenerDatosRegistro();

            if (datosUsuario != null)
            {
                if (datosUsuario.userFTP != null)
                {
                    this.nombreUsuarioFTPBox.Text = datosUsuario.userFTP;
                }
                if(datosUsuario.passwFTP != null)
                {
                    this.passwordBoxUsuarioFTP.Password = datosUsuario.passwFTP;
                }
                if(datosUsuario.direccionFTP != null)
                {
                    this.urlFTPBox.Text = datosUsuario.direccionFTP;
                }
            }
            
            this.menuEnvioFTP.Visibility = Windows.UI.Xaml.Visibility.Visible;
        }



        private async void CargarDocumentoWeb(StorageFile pdfACargar)
        {

          //  var del = await deleteTempLocalFiles();
            if (pdfACargar != null)
            {
                if (pdfACargar.Name.Contains(".pdf"))
                {
                    pdfOriginal = pdfACargar;
                    documentoFirma = pdfACargar;
                    pdfPageNumber = 1;
                    var carga = await LoadFile(pdfACargar);
                }
                

                //mostramos el menú de firma pero con todas las opciones inactivas porque aún no hay firma
                this.menuFirmar.Visibility = Visibility.Visible;
                this.borrarFirmaMenuItem.IsEnabled = false;
                this.firmaActualMenuItem.IsEnabled = false;
                this.firmaTodasMenuItem.IsEnabled = false;

                //posicionamos el componente de firma en la parte central 
                double posX = ((MainCanvas.ActualWidth / 2) - (marcoFirma.ActualWidth / 2));
                double posY = ((MainCanvas.ActualHeight / 2) - (marcoFirma.ActualHeight / 2));

                Canvas.SetLeft(ComponenteFirma, posX);
                Canvas.SetTop(ComponenteFirma, posY);
                BorrarFirma();
                this.ComponenteFirma.Visibility = Visibility.Visible;
                this.canvasFirma.Opacity = 100;
            }
            flipView.SelectedIndex = 0;
            menuFirmaVisibleOpcDesactivadas();
            // menuEnviarVisibleOpcActivadas();
        }

        private async void listadoDocsPend_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            PendingDocsServices infoDocSeleccionado = new PendingDocsServices();
            StorageFile pdfDescargado;



            if (listadoDocsPendientes.SelectedItem == null)
                return;

            else
            {

                infoDocSeleccionado.idDocuments = new List<string>();
                infoDocSeleccionado.idDocuments.Add(((PendingFileInfo)listadoDocsPendientes.SelectedItem).IdDoc);
                infoDocSeleccionado.idUser = datosUsuario.idUser;


                //llamamos al servicio de consulta de documento
                Servicios.WebServices servicios = new Servicios.WebServices();

                var respuesta = await servicios.getDocumentWebService(infoDocSeleccionado, datosUsuario.username, datosUsuario.password);


                if (respuesta != null)
                {
                    gestionDocumentosPDF gestorPDF = new gestionDocumentosPDF();
                    pdfDescargado = await gestorPDF.Base64ToFile(respuesta.ListDocuments[0].content, respuesta.ListDocuments[0].fileName);
                    if (pdfDescargado != null)
                    {
                        //lo cargamos en el flipview
                        CargarDocumentoWeb(pdfDescargado);


                        //almacenamos la información del documento que hemos cargado en el flipview para su posterior envío
                        this.pendingDocInfo = respuesta.ListDocuments[0];
                        isPendingDocument = true;

                        this.menuDocPendientes.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                    }
                    else
                    {
                        //mostramos mensaje de error
                        popupMessage = loader.GetString("errorDescargaDocPendiente");
                        this.PopUp.Visibility = Windows.UI.Xaml.Visibility.Visible;
                    }
                }
            }
            
            
            

        }
        


    }






        

        



}

