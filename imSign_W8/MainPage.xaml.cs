﻿using System;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Markup;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Popups;
using Windows.Media;
using Windows.UI;


namespace SDKTemplate
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : SDKTemplate.Common.LayoutAwarePage
    {

      

        private char tipoLicencia = 'T';
       
            public MainPage()
            {
                this.InitializeComponent();

               
            }     

            

            private void btnAceptarLogin_click(object sender, RoutedEventArgs e)
            {
                //en caso de TrialVersion
                if(tipoLicencia.Equals( 'T'))
                {
                    //navegamos a la siguiente página
                    //this.Frame.Navigate(typeof(PaginaOpciones));
                    this.Frame.Navigate(typeof(imSign_W8.SecondaryPage));
                }
                //en caso de PremiumVersion
                else if (tipoLicencia.Equals('P'))
                {
                    //TODO: comprobación de contraseña contra el servidor para determinar si es un usuario válido

                    //usuario no válido, mostramos mensaje de error
                    contraseñaNoValida();

                }
            }


            private async void contraseñaNoValida()
            {

                var messageDialog = new MessageDialog("La contraseña no es válida");
                await messageDialog.ShowAsync();

            }

            private void btnCancelarLogin_click(object sender, RoutedEventArgs e)
            {
                //restauramos al estado inicial la apariencia de los botones e imágenes de Trial Version y Premium Version
                this.btnTrialVersion.Foreground = new SolidColorBrush(Color.FromArgb(100, 184, 177, 177));
                this.checkTrialVersion.Source = new BitmapImage(new Uri("ms-appx:///Assets/radio_off.png"));
                this.btnPremiumVersion.Foreground = new SolidColorBrush(Color.FromArgb(100, 184, 177, 177));
                this.checkPremiumVersion.Source = new BitmapImage(new Uri("ms-appx:///Assets/radio_off.png"));

                //desactivamos el textbox para la password de la versión premium
                this.panelPassword.Visibility = Visibility.Collapsed;
                this.passwPremium.Visibility = Visibility.Collapsed;

                //vaciamos el contenido del textbox de la password
                this.passwPremium.Password = "";

                //ocultamos los botones Aceptar y Cancelar
                this.aceptarBtn.Visibility = Visibility.Collapsed;
                this.cancelarBtn.Visibility = Visibility.Collapsed;
            }




            
            private void premiumVersion_click(object sender, RoutedEventArgs e)
            {

                tipoLicencia = 'P';

                //activamos los botones
                this.cancelarBtn.Visibility = Visibility.Visible;
                this.aceptarBtn.Visibility = Visibility.Visible;

                //pintamos del color seleccionado el texto del botón "Trial Version" y cambiamos la imagen del radio por la coloreada
                this.btnPremiumVersion.Foreground = new SolidColorBrush(Color.FromArgb(100, 0, 176, 202));
                this.checkPremiumVersion.Source = new BitmapImage(new Uri("ms-appx:///Assets/radio_on.png"));

                //activamos el textbox para la password de la versión premium
                this.panelPassword.Visibility = Visibility.Visible;
                this.passwPremium.Visibility = Visibility.Visible;

                //pintamos del color estandar el texto del botón "Premium Version" y cambiamos la imagen del radio por la inactiva
                this.btnTrialVersion.Foreground = new SolidColorBrush(Color.FromArgb(100, 184, 177, 177));
                this.checkTrialVersion.Source = new BitmapImage(new Uri("ms-appx:///Assets/radio_off.png"));

            }

            private void trialVersion_click(object sender, RoutedEventArgs e)
            {
                tipoLicencia = 'T';

                //activamos los botones
                this.cancelarBtn.Visibility = Visibility.Visible;
                this.aceptarBtn.Visibility = Visibility.Visible;

                //pintamos del color seleccionado el texto del botón "Trial Version" y cambiamos la imagen del radio por la coloreada
                this.btnTrialVersion.Foreground = new SolidColorBrush(Color.FromArgb(100,0,176,202));
                this.checkTrialVersion.Source = new BitmapImage(new Uri("ms-appx:///Assets/radio_on.png"));

                //pintamos del color estandar el texto del botón "Premium Version" y cambiamos la imagen del radio por la inactiva
                this.btnPremiumVersion.Foreground = new SolidColorBrush(Color.FromArgb(100, 184, 177, 177));
                this.checkPremiumVersion.Source = new BitmapImage(new Uri("ms-appx:///Assets/radio_off.png"));

                //desactivamos el textbox para la password de la versión premium
                this.panelPassword.Visibility = Visibility.Collapsed;
                this.passwPremium.Visibility = Visibility.Collapsed;
                
                                
            }
        
    }
       

        
}
