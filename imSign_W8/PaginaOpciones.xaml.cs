﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Shapes;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.Storage.Streams;
using SDKTemplate;
using System.Threading.Tasks;
using Windows.Data.Pdf;
using Windows.UI.Popups;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.security;
using iTextSharp.awt;
using Org.BouncyCastle;
using Org.BouncyCastle.Utilities;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Pkcs;
using Org.BouncyCastle.X509;
using Windows.Graphics;
using Windows.Graphics.Imaging;
using System.Windows.Input;



// La plantilla de elemento Página en blanco está documentada en http://go.microsoft.com/fwlink/?LinkId=234238

namespace SDKTemplate
{
    /// <summary>
    /// Página vacía que se puede usar de forma independiente o a la que se puede navegar dentro de un objeto Frame.
    /// </summary>
    public sealed partial class PaginaOpciones : SDKTemplate.Common.LayoutAwarePage
    {

        
        public static PaginaOpciones Current;
        PaginaOpciones rootPage = PaginaOpciones.Current;
       

        private Windows.Data.Pdf.PdfDocument _pdfDocument;

        //Scenario specific constants
        const uint SUPPORTEDCONTACTS = 5;
        const double STROKETHICKNESS = 2;
        Dictionary<uint, Point?> contacts;

        ////TODO. por ahora ponemos una ruta fija de certicado, pero entiendo que lo tendría que crear la propia aplicación para certificar la firma
        //private String pathCertificado = @"Assets\certificadoPrueba.pfx";
        //private Stream cert;
        //public static char[] PASSWORD = "1234".ToCharArray();
        private String nombrePdf;
        private StorageFile pdf;



        //MCC
        Point _previousPosition = default(Point);
        SolidColorBrush _stroke = new SolidColorBrush(Colors.Red);
        bool _pressed = false;
        List<List<Point>> _allPoints = new List<List<Point>>();
        //FIN MCC

        
        public PaginaOpciones()
        {
            this.InitializeComponent();

            string prueba = "prueba commit";

            // This is a static public property that will allow downstream pages to get 
            // a handle to the MainPage instance in order to call methods that are in this class.
            Current = this;


            
            contacts = new Dictionary<uint, Point?>((int)SUPPORTEDCONTACTS);
            //canvasFirma.PointerPressed += new PointerEventHandler(canvasFirma_PointerPressed);
            //canvasFirma.PointerMoved += new PointerEventHandler(canvasFirma_PointerMoved);
            //canvasFirma.PointerReleased += new PointerEventHandler(canvasFirma_PointerReleased);
            //canvasFirma.PointerExited += new PointerEventHandler(canvasFirma_PointerReleased);

            canvasFirma.PointerPressed += new PointerEventHandler(inkCanvas_PointerPressed_1);
            //canvasFirma.PointerMoved += new PointerEventHandler(inkCanvas_PointerMoved_1);
            canvasFirma.PointerReleased += new PointerEventHandler(inkCanvas_PointerReleased_1);
           

           
          
        }

        //void canvasFirma_PointerReleased(object sender, PointerRoutedEventArgs e)
        //{
        //    uint ptrId = e.GetCurrentPoint(sender as FrameworkElement).PointerId;

        //    if (contacts.ContainsKey(ptrId))
        //    {
        //        contacts[ptrId] = null;
        //        contacts.Remove(ptrId);
        //        --numActiveContacts;
        //    }
        //    e.Handled = true;
          
        //}

        //void canvasFirma_PointerMoved(object sender, PointerRoutedEventArgs e)
        //{
        //    Windows.UI.Input.PointerPoint pt = e.GetCurrentPoint(canvasFirma);
        //    uint ptrId = pt.PointerId;

        //    if (contacts.ContainsKey(ptrId) && contacts[ptrId].HasValue)
        //    {
        //        Point currentContact = pt.Position;
        //        Point previousContact = contacts[ptrId].Value;
        //        if (Distance(currentContact, previousContact) > 4)
        //        {
        //            Line l = new Line()
        //            {
        //                X1 = previousContact.X,
        //                Y1 = previousContact.Y,
        //                X2 = currentContact.X,
        //                Y2 = currentContact.Y,
        //                StrokeThickness = STROKETHICKNESS,
        //                Stroke = new SolidColorBrush(Colors.Black),
        //                StrokeEndLineCap = PenLineCap.Round
        //            };

        //            contacts[ptrId] = currentContact;
        //            ((System.Collections.Generic.IList<UIElement>)canvasFirma.Children).Add(l);
        //        }
        //    }

        //    e.Handled = true;
        //}

        private double Distance(Point currentContact, Point previousContact)
        {
            return Math.Sqrt(Math.Pow(currentContact.X - previousContact.X, 2) + Math.Pow(currentContact.Y - previousContact.Y, 2));
        }

        //void canvasFirma_PointerPressed(object sender, PointerRoutedEventArgs e)
        //{
        //    if (numActiveContacts >= SUPPORTEDCONTACTS)
        //    {
        //        // cannot support more contacts
        //        return;
        //    }
        //    Windows.UI.Input.PointerPoint pt = e.GetCurrentPoint(canvasFirma);

        //    contacts[pt.PointerId] = pt.Position;
        //    e.Handled = true;
        //    ++numActiveContacts;


        //    //activamos las opciones del menú de firma
        //    this.borrarFirmaMenuItem.IsEnabled = true;
        //    this.firmaActualMenuItem.IsEnabled = true;
        //    this.firmaTodasMenuItem.IsEnabled = true;
        //}

        private void inkCanvas_PointerPressed_1(object sender,
    PointerRoutedEventArgs e)
        {
            _previousPosition = e.GetCurrentPoint(canvasFirma).Position;

            _allPoints.Add(new List<Point>());
            _allPoints.Last().Add(_previousPosition);

            _pressed = true;

            //activamos las opciones del menú de firma
            this.borrarFirmaMenuItem.IsEnabled = true;
            this.firmaActualMenuItem.IsEnabled = true;
            this.firmaTodasMenuItem.IsEnabled = true;
        }

        private void inkCanvas_PointerReleased_1(object sender,
          PointerRoutedEventArgs e)
        {
            if (_pressed)
                _pressed = false;
        }

        private void inkCanvas_PointerMoved_1(object sender,
          PointerRoutedEventArgs e)
        {
            if (!_pressed) return;
            var positions = e.GetIntermediatePoints(canvasFirma).
              Select(ppt => ppt.Position);

            foreach (Point pt in positions)
            {

                canvasFirma.Children.Add(
                  new Line()
                  {
                      X1 = _previousPosition.X,
                      Y1 = _previousPosition.Y,
                      X2 = pt.X,
                      Y2 = pt.Y,
                      Stroke = _stroke,
                      StrokeThickness = 2
                  }
                  );
                _previousPosition = pt;
            }
            _allPoints.Last().AddRange(positions);
        }

        private void MenuFlyOutItem_click(object sender, RoutedEventArgs e)
        {

        }

        private void MenuFlyoutItem_Click_1(object sender, RoutedEventArgs e)
        {

        }

        private void MenuConfiguracion_click(object sender, RoutedEventArgs e)
        {

        }

        private void MenuPersonalizacion_click(object sender, RoutedEventArgs e)
        {

        }

        private async void CargarDocumentoLocal_click(object sender, RoutedEventArgs e)
        {

            //para cargar un documento local, abrimos un cuadro de diálogo para seleccionar el documento
            FileOpenPicker openPicker = new FileOpenPicker();
            openPicker.ViewMode = PickerViewMode.Thumbnail;
            openPicker.ViewMode = PickerViewMode.List;
            openPicker.SuggestedStartLocation = PickerLocationId.DocumentsLibrary;
            openPicker.FileTypeFilter.Add(".pdf");
            pdf = await openPicker.PickSingleFileAsync();
            if (pdf != null)
            {
                try
                {

                    LoadFile(pdf);
                    //mostramos el menú de firma pero con todas las opciones inactivas porque aún no hay firma
                    this.menuFirmar.Visibility = Visibility.Visible;
                    this.borrarFirmaMenuItem.IsEnabled = false;
                    this.firmaActualMenuItem.IsEnabled = false;
                    this.firmaTodasMenuItem.IsEnabled = false;
                    this.ComponenteFirma.Visibility = Visibility.Visible;
                                        
                }
                catch (Exception err)
                {
                    
                }
                
            }            
        }

        public async void LoadFile(StorageFile pdf)
        {
            Windows.Data.Pdf.PdfDocument doc = await Windows.Data.Pdf.PdfDocument.LoadFromFileAsync(pdf);
            
            double pdfWidth = doc.GetPage(0).Dimensions.ArtBox.Width;
            double pdfHeight = doc.GetPage(0).Dimensions.ArtBox.Height;



            flipView.DataContext = null;

            pages = new ObservableCollection<PdfPageWrapper>();
            for (uint count=0; count < doc.PageCount; count++)
            {
                PdfPageWrapper pageWrapper = new PdfPageWrapper(doc, count);
                pages.Add(pageWrapper);
            }

            flipView.DataContext = pages;
            
            nombrePdf = pdf.Name;
                        
            flipView.Focus(Windows.UI.Xaml.FocusState.Pointer);
    
        }

        private ObservableCollection<PdfPageWrapper> pages;


        public async Task RenderPDFPage(StorageFile pdfFile)
        {
            try
            {

                //Load Pdf File

                _pdfDocument = await Windows.Data.Pdf.PdfDocument.LoadFromFileAsync(pdfFile); ;

                if (_pdfDocument != null && _pdfDocument.PageCount > 0)
                {

                    //Get Pdf page
                    var pdfPage = _pdfDocument.GetPage(6);

                    if (pdfPage != null)
                    {
                        // next, generate a bitmap of the page
                        StorageFolder tempFolder = ApplicationData.Current.TemporaryFolder;
                       
                        StorageFile jpgFile = await tempFolder.CreateFileAsync(Guid.NewGuid().ToString() + ".png", CreationCollisionOption.ReplaceExisting);

                        if (jpgFile != null)
                        {
                            IRandomAccessStream randomStream = await jpgFile.OpenAsync(FileAccessMode.ReadWrite);

                            await pdfPage.RenderToStreamAsync(randomStream);


                            await randomStream.FlushAsync();

                            randomStream.Dispose();
                            pdfPage.Dispose();

                            //await DisplayImageFileAsync(jpgFile);
                        }
                    }
                }
            }
            catch (Exception err)
            {


            }
        }

        
        

        private void CargarDocumentoServicio_click(object sender, RoutedEventArgs e)
        {
            mensajePremiumVersion();
        }

        private void CargarDocumentoWeb_click(object sender, RoutedEventArgs e)
        {
            mensajePremiumVersion();
        }

        private void VolverAInicio_click(object sender, RoutedEventArgs e)
        {
            //navegamos a la página inicial
            this.Frame.Navigate(typeof(MainPage));
        }

        private async void FirmarPaginaActual_click(object sender, RoutedEventArgs e)
        {

            //1. obtenemos la posición actual del componente de firma
            PdfPageWrapper pagina = (PdfPageWrapper)flipView.SelectedItem;

            //var ttv = ComponenteFirma.TransformToVisual(this.MainCanvas);
            //Point screenCords = ttv.TransformPoint(new Point(0, 0));

            var ttv = GridEspacioFirma.TransformToVisual(this.canvasFirma);
            Point screenCords = ttv.TransformPoint(new Point(0, 0));

            



            //2. obtenemos el contenido del Canvas en formato imagen

            StorageFolder tempFolder = ApplicationData.Current.TemporaryFolder;



            StorageFile imagenFirma = await tempFolder.CreateFileAsync(string.Format("{0}_firma.png",
          DateTimeOffset.Now.ToString("MM_dd_yy_hh_mm_ss")), CreationCollisionOption.ReplaceExisting);
            //StorageFile imagenFirma = await tempFolder.CreateFileAsync("firma" + ".png", CreationCollisionOption.ReplaceExisting);

            var renderTargetBitMap = new RenderTargetBitmap();
            await renderTargetBitMap.RenderAsync(canvasFirma);

            //conseguimos todos los píxeles del Canvas y los guardamos en encoder
            var pixelBuffer = await renderTargetBitMap.GetPixelsAsync();
            var width = renderTargetBitMap.PixelWidth;
            var height = renderTargetBitMap.PixelHeight;

            using (IRandomAccessStream stream = await imagenFirma.OpenAsync(FileAccessMode.ReadWrite))
            {
                BitmapEncoder encoder = await BitmapEncoder.CreateAsync(selectGuid(imagenFirma.FileType), stream);
                Stream pixelStream = pixelBuffer.AsStream();
                byte[] pixels = new byte[pixelStream.Length];
                await pixelStream.ReadAsync(pixels, 0, pixels.Length);

                encoder.SetPixelData(BitmapPixelFormat.Bgra8, BitmapAlphaMode.Straight,
                                     (uint)width,
                                     (uint)height,
                                     96.0,
                                     96.0,
                                     pixels);

                await encoder.FlushAsync();


                var decoder = await BitmapDecoder.CreateAsync(stream);

                await stream.FlushAsync();

            }
           
            await InsertarFirma((PdfPageWrapper)flipView.SelectedItem, imagenFirma, (float)screenCords.X, (float)screenCords.Y);

            //volvemos a cargar el pdf

            LoadFile(pdf);




        }


        //public async void InsertarFirma(PdfPageWrapper paginaOriginal, BitmapDecoder decoderFirma)
        //{

        //    BitmapImage imagenOrigen = paginaOriginal.Source;

        //    //1. Tratamiento de la FIRMA
        //    var pxDataProviderFirma = await decoderFirma.GetPixelDataAsync();

        //    //obtenemos todos los pixels que componen la imagen de la firma
        //    byte[] pixelsFirma = pxDataProviderFirma.DetachPixelData();


        //    //2. Tratamiento de la PÁGINA DEL PDF DONDE SE FIRMA

        //    //2.1 Obtenemos el Stream de la página 
        //    InMemoryRandomAccessStream streamPaginaPdf = new InMemoryRandomAccessStream();

        //    Windows.Data.Pdf.PdfPageRenderOptions renderOptions = new Windows.Data.Pdf.PdfPageRenderOptions();
        //    renderOptions.DestinationHeight = (uint)(paginaOriginal.Height);
        //    paginaOriginal.pdfPage.RenderToStreamAsync(streamPaginaPdf, renderOptions);

        //    //2.2 Creamos el Decoder a partir de ese stream
        //    var decoderPaginaPdf = await BitmapDecoder.CreateAsync(streamPaginaPdf);

        //    //2.3 Obtenemos los pixels de la página PDF
        //    var pxDataProviderPaginaPdf = await decoderPaginaPdf.GetPixelDataAsync();

        //    //obtenemos todos los pixels que componen la imagen de la firma
        //    byte[] pixelsPaginaPdf = pxDataProviderPaginaPdf.DetachPixelData();






        //    //3. volcamos en el ENCODER
        //    var bmpstream = new InMemoryRandomAccessStream();
        //    var encoder = await BitmapEncoder.CreateAsync(BitmapEncoder.BmpEncoderId, bmpstream);


        //    //encoder.SetPixelData(BitmapPixelFormat.Bgra8, BitmapAlphaMode.Ignore, decoderFirma.PixelWidth, decoderFirma.PixelHeight, 96, 96, pixelsPaginaPdf);
        //    encoder.SetPixelData(BitmapPixelFormat.Bgra8, BitmapAlphaMode.Ignore, decoderFirma.PixelWidth, decoderFirma.PixelHeight, 96, 96, pixelsFirma);
        //    await encoder.FlushAsync();



        //    var bd = new BitmapImage();
        //    await bd.SetSourceAsync(bmpstream);
        //    bmpstream.Seek(0);


        //    this.imagenPrueba.Source = bd;

        //}

      

       

          private void Interpolate(Point pFrom, Point pTo,
   ref List<Point> results)
          {
              var xDiff = Math.Abs((pTo.X - pFrom.X));
              var yDiff = Math.Abs((pTo.Y - pFrom.Y));

              if (xDiff < 1 && yDiff < 1) return; //we stop 

              if (yDiff > xDiff) //interpolate along the x-axis
              {
                  Point p0 = pFrom.Y < pTo.Y ? pFrom : pTo;
                  Point p1 = pFrom.Y < pTo.Y ? pTo : pFrom;

                  double y = (yDiff / 2) + Math.Min(pFrom.Y, pTo.Y);
                  Point newPt = new Point(p0.X + (y - p0.Y) *
                    ((p1.X - p0.X) / (p1.Y - p0.Y)), y);
                  results.Add(newPt);
                  Interpolate(pFrom, newPt, ref results);
                  Interpolate(newPt, pTo, ref results);
              }
              else //interpolate along y-axis
              {
                  Point p0 = pFrom.X < pTo.X ? pFrom : pTo;
                  Point p1 = pFrom.X < pTo.X ? pTo : pFrom;

                  double x = (xDiff / 2) + Math.Min(pFrom.X, pTo.X);
                  Point newPt = new Point(x, p0.Y + (x - p0.X) *
                    ((p1.Y - p0.Y) / (p1.X - p0.X)));
                  results.Add(newPt);
                  Interpolate(pFrom, newPt, ref results);
                  Interpolate(newPt, pTo, ref results);
              }
          }


        public async Task InsertarFirma(PdfPageWrapper paginaOriginal, StorageFile fileFirma, float x, float y)
        {
            //1. obtenemos el stream de la página del pdf que queremos firmar
            //creamos el PdfReader del pdf de origen

            try
            {

                //en pdf tenemos el storagefile correspondiente al archivo cargado
                //lo convertimos en Stream

                Stream pdfStream = await pdf.OpenStreamForReadAsync();

                PdfReader reader = new PdfReader(pdfStream);

                //2. creamos el pdf de salida

              
                StorageFolder tempFolder = ApplicationData.Current.TemporaryFolder;            
                StorageFile pdfOutputFile;
                try 
                {
                    pdfOutputFile = await tempFolder.CreateFileAsync(string.Format("{0}_signature.pdf",
                    DateTimeOffset.Now.ToString("MM_dd_yy_hh_mm_ss")), CreationCollisionOption.ReplaceExisting);

                  
                }
                catch (Exception e)
                {
                    String mensaje = e.InnerException.ToString();
                    return;
                }


                IRandomAccessStream outputPdfStream = null;

                if (pdfOutputFile != null)
                {
                    outputPdfStream = await pdfOutputFile.OpenAsync(FileAccessMode.ReadWrite);

                }

                PdfStamper stamper = new PdfStamper(reader, outputPdfStream.AsStream(), '1', true);
                //stamper.FormFlattening = false;

                PdfContentByte pdfContentByte = stamper.GetOverContent((int)paginaOriginal.pageIndex);

                BitmapImage imagenFirma = new BitmapImage();


                imagenFirma = await LoadImage(fileFirma);


                IRandomAccessStream firmaStream = null;
                firmaStream = await fileFirma.OpenAsync(FileAccessMode.Read);
                iTextSharp.text.Image imagen = iTextSharp.text.Image.GetInstance(firmaStream.AsStream());


               // float posY = (float)(paginaOriginal.Height - marcoFirma.ActualHeight);
                //float posY = (float)(842 - 60 - canvasFirma.ActualHeight + 42.85);

                //float posY = (float)((842 - marcoFirma.ActualHeight) + 108);

                //convertimos las posiciones en x e y del canvas de firma a positivo
                x = Math.Abs(x);
                y = Math.Abs(y);

                //float posY = (float)(842 - y);

                //imagen.SetAbsolutePosition(x, posY);

                ////imagen.SetAbsolutePosition(x + 20, posY);
                ////imagen.SetAbsolutePosition(0,0);

                ////para el equipo local y el simulador:
                // imagen.ScalePercent((float)75.16);

                 

                //para la tablet:
                //imagen.ScalePercent((float)52);
                //imagen.SetAbsolutePosition(y, x);



                //escala en el eje x: ancho de la página real del PDF entre el ancho del flipview
               // double scaleX = (reader.GetPageSize(1).Width) / MainCanvas.ActualWidth;
                double scaleX = (reader.GetPageSize(1).Width) / 799.8;

                //escala en el eje y: alto de la página real del PDF entre el alto del flipview
                //double scaleY = (reader.GetPageSize(1).Height) / MainCanvas.ActualHeight;
                double scaleY = (reader.GetPageSize(1).Height) / 1179;

                //escalamos la imagen de la firma en el eje x y en el eje y
                imagen.ScaleAbsolute((float)(canvasFirma.ActualWidth * scaleX), (float)(canvasFirma.ActualHeight * scaleY));

                
                //se obtienen las coordenadas absolutas de la firma y se escalan respecto de las del pdf

                //obtenemos la posición del contentRoot respecto de su hijo, el flipView
                var ttv = ContentRoot.TransformToVisual(this.flipView);
                Point screenCords = ttv.TransformPoint(new Point(0, 0));

                double positionPdfViewX = Math.Abs(screenCords.X);
                double positionPdfViewY = Math.Abs(screenCords.Y);

                float cx = (float)((x - positionPdfViewX ) * scaleX);
                float cy = (float)((MainCanvas.ActualHeight - y - canvasFirma.ActualHeight) * scaleY);

                imagen.SetAbsolutePosition((float)(x * scaleX), (float)((MainCanvas.ActualHeight - y - canvasFirma.ActualHeight) * scaleY));


                pdfContentByte.AddImage(imagen);
                //pdfContentByte.AddImage(imagen,
                
                stamper.Close();
                //outputPdfStream.Close();
                outputPdfStream.Dispose();
                pdfStream.Dispose();

                pdf = pdfOutputFile;
                
            }
            catch (Exception e)
            {
                string error;
            }


        }


        //public async void InsertarFirma(PdfPageWrapper paginaOriginal, StorageFile fileFirma, float x, float y)
        //{
        //    //1. obtenemos el stream de la página del pdf que queremos firmar
        //    //creamos el PdfReader del pdf de origen

        //    try
        //    {

        //        //en pdf tenemos el storagefile correspondiente al archivo cargado
        //        //lo convertimos en Stream

        //        Stream pdfStream = await pdf.OpenStreamForWriteAsync();



        //        //PdfReader reader = new PdfReader(pdfStream);

        //        //using (var ms = new MemoryStream())
        //        //{
        //        //    using (var stamp = new PdfStamper(reader,ms))
        //        //    {
        //        //        var size = reader.GetPageSize(1);

        //        //        BitmapImage imagenFirma = new BitmapImage();


        //        //        imagenFirma = await LoadImage(fileFirma);


        //        //        IRandomAccessStream firmaStream = null;
        //        //        firmaStream = await fileFirma.OpenAsync(FileAccessMode.Read);
        //        //        iTextSharp.text.Image imagen = iTextSharp.text.Image.GetInstance(firmaStream.AsStream());
        //        //        imagen.SetAbsolutePosition(x, y);

        //        //        stamp.GetOverContent((int)paginaOriginal.pageIndex).AddImage(imagen);
        //        //        stamp.Close();
        //        //        reader.Close();


                        

        //        //    }
        //        //    ms.Flush();

        //        //}


        //        using (var ms = new MemoryStream())
        //        {
        //            var pdfReader = new PdfReader(pdfStream);
        //            var doc = new Document(pdfReader.GetPageSizeWithRotation(1));
        //            using (var writer = PdfWriter.GetInstance(doc,ms))
        //            {
        //                doc.Open();
        //                BitmapImage imagenFirma = new BitmapImage();


        //                imagenFirma = await LoadImage(fileFirma);


        //                IRandomAccessStream firmaStream = null;
        //                firmaStream = await fileFirma.OpenAsync(FileAccessMode.Read);
        //                iTextSharp.text.Image imagen = iTextSharp.text.Image.GetInstance(firmaStream.AsStream());
        //                imagen.SetAbsolutePosition(x, y);

        //                doc.Add(imagen);


        //                doc.Close();
        //                writer.Close();
        //            }

        //            ms.Flush();


        //        }

               
                
        //    }
        //    catch (Exception e)
        //    {
        //        string error;
        //    }


        //}


        private static async Task<BitmapImage> LoadImage(StorageFile file)
        {
            BitmapImage bitmapImage = new BitmapImage();
            FileRandomAccessStream stream = (FileRandomAccessStream)await file.OpenAsync(FileAccessMode.Read);

            bitmapImage.SetSource(stream);

            return bitmapImage;

        }

        private Guid selectGuid(string type)
        {
            Guid BitmapEncoderGuid;

            switch (type)
            {
                case "jpg":
                    BitmapEncoderGuid = BitmapEncoder.JpegEncoderId;
                    break;

                case "png":
                    BitmapEncoderGuid = BitmapEncoder.PngEncoderId;
                    break;

                case "bmp":
                    BitmapEncoderGuid = BitmapEncoder.BmpEncoderId;
                    break;

                case "gif":
                    BitmapEncoderGuid = BitmapEncoder.GifEncoderId;
                    break;

                default:
                    BitmapEncoderGuid = BitmapEncoder.GifEncoderId;
                    break;
            }

            return BitmapEncoderGuid;
        }

            
        public async void LeerCertificado(string path)
        {
            try
            {


                //  StorageFolder installationFolder = Windows.ApplicationModel.Package.Current.InstalledLocation;
                StorageFolder localFolder = ApplicationData.Current.LocalFolder;

                StorageFile certfile = await localFolder.GetFileAsync(path);

                var randomAccessStream = await certfile.OpenReadAsync();
               
                //cert = randomAccessStream.AsStreamForRead();



            }
            catch (Exception e)
            {

            }
        }

        private void FirmarTodasLasPaginas_click(object sender, RoutedEventArgs e)
        {

        }

        private async void EnviarPorMail_click(object sender, RoutedEventArgs e)
        {
            var mailto = new Uri("mailto:?to=mcallejo@indra.es&subject=Hello&body=Text goes here");
            await Windows.System.Launcher.LaunchUriAsync(mailto);
        }

        private void EnviarPorServicio_click(object sender, RoutedEventArgs e)
        {
            mensajePremiumVersion();
        }

        private void BorrarFirma_click(object sender, RoutedEventArgs e)
        {
            //tenemos que limpiar todos los puntos pintados en canvasFirma

            canvasFirma.Children.Clear();
            
            //desactivamos las opciones del menú de firma
            this.borrarFirmaMenuItem.IsEnabled = false;
            this.firmaActualMenuItem.IsEnabled = false;
            this.firmaTodasMenuItem.IsEnabled = false;

        }

        private void virtualizingStackPanel_CleanUpVirtualizedItem(object sender, CleanUpVirtualizedItemEventArgs e)
        {

        }

        private void scroll_viewChanged(object sender, ScrollViewerViewChangedEventArgs e)
        {

        }
        
        private void OnManipulationDelta(object sender, Windows.UI.Xaml.Input.ManipulationDeltaRoutedEventArgs e)
        {
           

            //obtenemos los límites del MainCanvas
            double maxLeft = Canvas.GetLeft(MainCanvas);
            double maxTop = Canvas.GetTop(MainCanvas);

            double componenteFirmaLeft = Canvas.GetLeft(ComponenteFirma);
            double componenteFirmaTop = Canvas.GetTop(ComponenteFirma);


            //comprobamos si con el movimiento, el control se sale de los márgenes establecidos por el elemento MainCanvas
            componenteFirmaLeft += e.Delta.Translation.X;
            componenteFirmaTop += e.Delta.Translation.Y;

            //izquierda
            if(componenteFirmaLeft <= maxLeft)
            {
                componenteFirmaLeft = maxLeft;
            }
            //derecha
            if((componenteFirmaLeft + marcoFirma.ActualWidth) > (Canvas.GetLeft(MainCanvas) + MainCanvas.ActualWidth))
            {

                componenteFirmaLeft = (Canvas.GetLeft(MainCanvas) + MainCanvas.ActualWidth) - marcoFirma.ActualWidth;

            
            }
            //superior
            if(componenteFirmaTop <= maxTop)
            {
                componenteFirmaTop = maxTop;
            }

            //inferior
            if ((componenteFirmaTop + marcoFirma.ActualHeight) > (Canvas.GetTop(MainCanvas) + MainCanvas.ActualHeight))
            {
                componenteFirmaTop = (Canvas.GetTop(MainCanvas) + MainCanvas.ActualHeight) - marcoFirma.ActualHeight;
            }
            Canvas.SetTop(ComponenteFirma, componenteFirmaTop);
            Canvas.SetLeft(ComponenteFirma, componenteFirmaLeft);

        }

        private void OnManipulationCompleted(object sender, ManipulationCompletedRoutedEventArgs e)
        {

        }

        private async void mensajePremiumVersion()
        {

            var messageDialog = new MessageDialog("Funcionalidad no disponible en la versión Trial. Por favor, acceda a la versión Premium.");
            await messageDialog.ShowAsync();

        }

        private void flipView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            

        }

        private void flipView_DataContextChanged(FrameworkElement sender, DataContextChangedEventArgs args)
        {

            flipView.UpdateLayout();
        }
        

    }
}
