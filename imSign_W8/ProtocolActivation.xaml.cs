﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;


// La plantilla de elemento Página en blanco está documentada en http://go.microsoft.com/fwlink/?LinkId=234238

namespace imSign_W8
{

    
    /// <summary>
    /// Página vacía que se puede usar de forma independiente o a la que se puede navegar dentro de un objeto Frame.
    /// </summary>
    public sealed partial class ProtocolActivation : Page
    {

        Application app;

        public ProtocolActivation()
        {
            this.InitializeComponent();
            //this.uriTb.Text = uri;
            
            
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            String uriParameters = e.Parameter as String;


            var parametros = uriParameters.Split('&');

            this.uriTb.Text = uriParameters;
        }
    }
}
