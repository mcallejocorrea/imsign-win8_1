﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using imSign_W8.BiometricsData;


namespace imSign_W8
{
    class DeviceRequestXML
    {

       
        XElement xmlStructure = 
            new XElement("deviceRequest", 
                new XElement("advancedSignage",
                    new XElement("biometrics",
                        new XElement("biometricsdata", ""),
                        new XElement("decryption",
                            new XElement("method", Constantes.method),
                            new XElement("notary",
                                new XElement("parameters",
                                    new XElement("keyrelationship", Constantes.keyRelationship),
                                    new XElement("hash_algorithm",Constantes.hashAlg),
                                    new XElement("keyEncryptionAlgorithm",Constantes.keyEcryptionAlg),
                                    new XElement("keyEncryption", Constantes.keyEncryption),
                                    new XElement("key",""),
                                    new XElement("blockEncryptionAlgorithm", Constantes.blockEncryptionAlg),
                                    new XElement("blockEncryption",Constantes.blockEncryption)
                                ),
                                new XElement("protocol",
                                    new XElement("city", Constantes.city),
                                    new XElement("country", Constantes.country),
                                    new XElement("date", Constantes.date),
                                    new XElement("name", Constantes.name),
                                    new XElement("number",Constantes.number)
                                )
                            )
                        ),
                        new XElement("imageData",
                            new XElement("data",""),
                            new XElement("digest",""),
                            new XElement("id",""),
                            new XElement("mimeType","")
                        )
                    ),
                    new XElement("clientData",""),
                    new XElement("formValidation",""),
                    new XElement("petitionID",""),
                    new XElement("serialNumber", Constantes.serialNumber),
                    new XElement("signValidation", ""),
                    new XElement("status", Constantes.status),
                    new XElement("user",""),
                    new XElement("versionID", Constantes.versionID)
                ),
                new XElement("system",
                    new XElement("certificateRequired", Constantes.certificateReq),
                    new XElement("deviceID", Constantes.deviceID),
                    new XElement("deviceTimeStamp",""),
                    new XElement("latitude",""),
                    new XElement("longitude",""),
                    new XElement("precision", Constantes.precision),
                    new XElement("trainingVersion",""),
                    new XElement("version",Constantes.version)
                ),
                new XElement("validation",
                    new XElement("formDigest",""),
                    new XElement("id",""),
                    new XElement("rawDigest",""),
                    new XElement("signatureDigest",""),
                    new XElement("timestamp",
                        new XElement("genTime", Constantes.genTime),
                        new XElement("messageImprint", Constantes.messageImprint),
                        new XElement("policy", Constantes.policy),
                        new XElement("serial", Constantes.serial),
                        new XElement("status",
                            new XElement("code", Constantes.code),
                            new XElement("message",Constantes.message)
                        ),
                        new XElement("statusCode", Constantes.statusCode),
                        new XElement("statusString", Constantes.statusString),
                        new XElement("timeStampToken", Constantes.timestampToken)
                    )
                )
            );


        public XElement XmlStructure
        {
            get{
                return xmlStructure;
            }
            set
            {
                xmlStructure = value;
            }
        }

        //public void CrearXml (DeviceRequest deviceRequest)
        //{
        //    AdvancedSignage advancedSignage = deviceRequest.AdvancedSignage;
        //    SystemBiometrics system = deviceRequest.System;
        //    Validation validation = deviceRequest.Validation;


        //    //advancedSignage
        //    this.xmlStructure.Element("advancedSignage")





        //}



        

    }

}
