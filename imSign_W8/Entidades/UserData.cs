﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;

namespace imSign_W8.Entidades
{
    public class UserData
    {

        private String Name;
        private String SurName;
        private String IdCode;
        private String Email;
        private String BusinessName;
        private int NKey;
        private Boolean Premium;
        private int IdUser;
        private String Tlf;        
        private String Username;
        private String Password;

        private String UserFTP;
        private String PasswFTP;
        private String DireccionFTP;
        private Brush ColorUsuario;
        private BitmapImage IconoUsuario;


        public String name
        {
            get
            {
                return this.Name;
            }
            set
            {
                this.Name = value;
            }
        }

        public String surName
        {
            get
            {
                return this.SurName;
            }
            set
            {
                this.SurName = value;
            }
        }

        public String idCode
        {
            get
            {
                return this.IdCode;
            }
            set
            {
                this.IdCode = value;
            }
        }

        public String email
        {
            get
            {
                return this.Email;
            }
            set
            {
                this.Email = value;
            }
        }

        public String businessName
        {
            get
            {
                return this.BusinessName;
            }
            set
            {
                this.BusinessName = value;
            }
        }

        public int nKey
        {
            get
            {
                return this.NKey;
            }
            set
            {
                this.NKey = value;
            }
        }
             
        public Boolean premium
        {
            get
            {
                return this.Premium;
            }
            set
            {
                this.Premium = value;
            }
        }

        public int idUser
        {
            get
            {
                return this.IdUser;
            }
            set
            {
                this.IdUser = value;
            }
        }  

        public String tlf
        {
            get
            {
                return this.Tlf;
            }
            set
            {
                this.Tlf = value;
            }
        }
        
        public String username
        {
            get
            {
                return this.Username;
            }
            set
            {
                this.Username = value;
            }
        }

        public String password
        {
            get
            {
                return this.Password;
            }
            set
            {
                this.Password = value;
            }
        }



        public String userFTP
        {
            get
            {
                return this.UserFTP;
            }
            set
            {
                this.UserFTP = value;
            }
        }

        public String passwFTP
        {
            get
            {
                return this.PasswFTP;
            }
            set
            {
                this.PasswFTP = value;
            }
        }

        public Brush colorUsuario
        {
            get
            {
                return this.ColorUsuario;
            }
            set
            {
                this.ColorUsuario = value;
            }
        }

        public BitmapImage iconoUsuario
        {
            get
            {
                return this.IconoUsuario;
            }
            set
            {
                this.IconoUsuario = value;
            }
        }


        public String direccionFTP
        {
            get
            {
                return this.DireccionFTP;
            }
            set
            {
                this.DireccionFTP = value;
            }
        }




        public bool IsValidEmail(string strIn)
        {
            if (String.IsNullOrEmpty(strIn))
                return true;

            // Return true if strIn is in valid e-mail format. 
            try
            {
                return Regex.IsMatch(strIn,
                      @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                      @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                      RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }
        public bool IsValidPhoneNumber(string strIn)
        {
            Regex Val = new Regex(@"^[+-]?\d+(\.\d+)?$");

            if (String.IsNullOrEmpty(strIn))
                return true;
            if (Val.IsMatch(strIn))
            {
                return true;
            }
            else
                return false;
        }

        public bool IsValidDNI(string strIn)
        {
            Regex Val = new Regex(@"^((\d{8}[A-Z]))$");
            if (Val.IsMatch(strIn))
            {
                return true;
            }
            else
                return false;

        }





    }
}
