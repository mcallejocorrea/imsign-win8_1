﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization;



namespace imSign_W8.Entidades
{
    
    public class Punto
    {

        //localización del punto en forma de coordenadas X,Y
        public double xPos { get; set; }
        public double yPos { get; set; }

        //velocidad (en forma de coordenadas X,Y pero respecto del movimiento realizado (en iOS se llama velocityInView y va asociado al pam, al movimiento de trazado)
        public double xVel { get; set; }
        public double yVel { get; set; }

        //variable para almacenar el módulo de la velocidad
        public double velocidad { get; set; }

        //aceleración --> se calcula en base a la posición y el tiempo del punto actual respecto del punto anterior en sus dos componentes, X e Y
        //                     (Vx1 - Vx0)
        //    AceleraciónX =  ------------
        //                     (T1 - T0)
        //
        //                     (Vy1 - Vy0)
        //    AceleraciónY =  ------------
        //                     (T1 - T0)

        public double xAceleracion { get; set; }
        public double yAceleracion { get; set; }

        


        //tiempo (timestamp del instante en que se dibuja el punto)
        public DateTime time { get; set; }

       
    }

    
}
